// JavaScript source code
// validate course save form on keyup and submit
$(document).ready(function() {
    // Draggable Cards
    dragula([document.getElementById('card-drag-area')]).on('drop', function(el) {
         var order_array  = [];
         $(".curriculum-card").each(function() {
            var content_id   = $(this).attr('data-content-id');
            if(order_array.indexOf(content_id) == -1){
               order_array.push(content_id);
            }
         });
         //calling ajax request to save the curriculum order
         $.ajax({
            type: 'POST',
            data:{
              _token : $('meta[name="csrf-token"]').attr('content'),
              course_id:this_course_id,
              order_array:JSON.stringify(order_array)
            },
            dataType:'JSON',
            url: site_url +'/save-curriculum-order',
            success: function(data){
              toastr.success(data.message, 'Success!', { "timeOut": 0 });
            },
            error: function(error){
                var errors      = error.responseJSON;
                var errorsHtml  = '';
                $.each(errors.errors,function (k,v) {
                    errorsHtml += '<li>'+ v + '</li>';
                });
                toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
            }
         });
    });
    //remove this tile from curriculum click function
    $('.remove-from-curriculum').click(function(){
        var this_curriculum_id = $(this).attr('data-curriculum-id');
        $.ajax({
            type: 'POST',
            data:{
              _token : $('meta[name="csrf-token"]').attr('content'),
              course_id:this_course_id,
              this_curriculum_id:this_curriculum_id
            },
            dataType:'JSON',
            url: site_url +'/delete-curriculum',
            success: function(data){
              toastr.success(data.message, 'Success!', { "timeOut": 0 });
              location.reload();
            },
            error: function(error){
                var errors      = error.responseJSON;
                var errorsHtml  = '';
                $.each(errors.errors,function (k,v) {
                    errorsHtml += '<li>'+ v + '</li>';
                });
                toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
            }
         });
    });
    //validation rules for course users select
    $("#course_user_form").validate({
        ignore: [], 
        rules: {
				  course_select_users: "required"
			  }
    });
    //validation rules for course basics
    $("#course_data_form").validate({
			rules: {
				course_name: "required",
				course_desc: {
					required: true,
					minlength: 10
				},
				course_date: "required",
				course_fee: "required",
        course_duration: "required",
        hid_schedule_file: {
            required:true,
            extension: "pdf"
        },
        hid_image_file: {
            required:true,
            extension: "png|jpe?g|gif"
        }
			},
			messages: {
				course_name: "Course Name is required",
				course_desc: {
					required: "Course Description is required",
					minlength: "Description must consist of at least 10 characters"
				},
				course_date: "Course Start Date is required",
				course_fee: "Course Fees is required",
				course_duration: "Course Duration is required",
        course_schedule:{
          required: "Course Schedule PDF is required",
					extension: "Please select a valid PDF file"
        },
        course_image:{
          required: "Course Image is required",
					extension: "Please select a valid PNG/JPG/GIF file"
        }
			}
		});
     //schedule file input onchange function
    $(document).on('change','#course_schedule',function(){
        if($(this)[0].files.length > 0){
            $('#hid_schedule_file').val($(this)[0].files[0].name);
            $("#schedule-label").html($(this)[0].files[0].name);
        }
    });
    //image file input onchange function
    $(document).on('change','#course_image',function(){
      if($(this)[0].files.length > 0){
          $('#hid_image_file').val($(this)[0].files[0].name);
          $("#image-label").html($(this)[0].files[0].name);
      }
    });
    // Basic Select2 select
    $("#course_select_users").select2({
      // the following code is used to disable x-scrollbar when click in select input and
      // take 100% width in responsive also
      placeholder: "Click here to select a user",
      dropdownAutoWidth: true,
      width: '100%'
    });
    // Basic Select2 select
    $("#add_video_to_course").select2({
      // the following code is used to disable x-scrollbar when click in select input and
      // take 100% width in responsive also
      placeholder: "Click here to select a Video",
      dropdownAutoWidth: true,
      width: '100%'
    });
    // Basic Select2 select
    $("#add_test_to_course").select2({
      // the following code is used to disable x-scrollbar when click in select input and
      // take 100% width in responsive also
      placeholder: "Click here to select a Test",
      dropdownAutoWidth: true,
      width: '100%'
    });
    // Basic Select2 select
    $("#add_class_to_course").select2({
      // the following code is used to disable x-scrollbar when click in select input and
      // take 100% width in responsive also
      placeholder: "Click here to select a Class",
      dropdownAutoWidth: true,
      width: '100%'
    });
    $('.select2-search__field').attr('placeholder','Click here');
    $('.select2-search__field').css('width','100%');
    //select all checkbox onclick function
    $("#select_all_check").click(function(){
        if($("#select_all_check").is(':checked') ){
            console.log('checked');
            $("#course_select_users > option").attr("selected","selected");
            $("#course_select_users").trigger("change");
        }else{
            console.log('unchecked');
            $("#course_select_users > option").removeAttr("selected");
             $("#course_select_users").trigger("change");
         }
    });
    //add content click button function
    $(document).on('click','#add_curriculum_content',function(){
        $("#add_content_modal").modal('show');
    });
});
