/*=========================================================================================
    File Name: roles-list-view.js
    Description: List View
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function() {
  "use strict"
  // init list view datatable
  var dataListView = $(".roles-list-view").DataTable({
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: { selectRow: true }
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 5,
    buttons: [
      {
        text: "<i class='feather icon-plus'></i> Create New Role",
        action: function() {
          $(this).removeClass("btn-secondary");
          location.href = site_url+'/role';
        },
        className: "btn-outline-primary"
      }
    ],
    initComplete: function(settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  });

  dataListView.on('draw.dt', function(){
    setTimeout(function(){
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });

  // init thumb view datatable
  var dataThumbView = $(".data-thumb-view").DataTable({
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: { selectRow: true }
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 4,
    buttons: [
      {
        text: "<i class='feather icon-plus'></i> Create A Test",
        action: function() {
          $(this).removeClass("btn-secondary");
          location.href = site_url+'/role';
        },
        className: "btn-outline-primary"
      }
    ],
    initComplete: function(settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  })

  dataThumbView.on('draw.dt', function(){
    setTimeout(function(){
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });

  // To append actions dropdown before add new button
  var actionDropdown = $(".actions-dropodown")
  actionDropdown.insertBefore($(".top .actions .dt-buttons"))


  // Scrollbar
  if ($(".data-items").length > 0) {
    new PerfectScrollbar(".data-items", { wheelPropagation: false })
  }

  // Close sidebar
  $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
    $(".add-new-data").removeClass("show")
    $(".overlay-bg").removeClass("show")
  })

  // On Edit
  $('.action-edit').on("click",function(e){
      e.stopPropagation();
      var role_id   = $(this).attr('data-value');
      location.href = site_url+'/role/'+role_id;
  });
  // On Delete
  $('.action-delete').on("click", function(e){
    var role_id = $(this).attr('data-value');
    e.stopPropagation();
    Swal.fire({
      title: 'Are you sure?',
      text: "This will disable user permissions associated with the role",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-danger ml-1',
      buttonsStyling: false,
    }).then(function (result) {
      if (result.value) {
        $.ajax({
          type: 'POST',
          dataType:'json',
          data:{
              _token : $('meta[name="csrf-token"]').attr('content'),
              role_id:role_id
          },
          url: 'delete-role',
          success: function(data){
            toastr.success(data.message, 'Success!', { "timeOut": 0 });
            setTimeout(function(){
              location.reload();
            },500);
          },
          error: function(error){
              var errors      = error.responseJSON;
              var errorsHtml  = '';
              $.each(errors.errors,function (k,v) {
                  errorsHtml += '<li>'+ v + '</li>';
              });
              toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
          }
        });
      }
      else{
        location.reload();
      }
    })
  });

  // dropzone init
  Dropzone.options.dataListUpload = {
    complete: function(files) {
      var _this = this
      // checks files in class dropzone and remove that files
      $(".hide-data-sidebar, .cancel-data-btn, .actions .dt-buttons").on(
        "click",
        function() {
          $(".dropzone")[0].dropzone.files.forEach(function(file) {
            file.previewElement.remove()
          })
          $(".dropzone").removeClass("dz-started")
        }
      )
    }
  }
  Dropzone.options.dataListUpload.complete()

  // mac chrome checkbox fix
  if (navigator.userAgent.indexOf("Mac OS X") != -1) {
    $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
  }

  //save video data ajax call
  function ajaxRequestVideoSave($form,event){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        type: 'POST',
        data:new FormData(jQuery('#video_data_form')[0]),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        url: 'save-video-data',
        success: function(data){
          $('#loader_btn').css('display','none');
          toastr.success(data.message, 'Success!', { "timeOut": 0 });
          $(".add-new-data").removeClass('show').addClass('hide');
          $(".overlay-bg").removeClass('show').addClass('hide');
          setTimeout(function(){
            location.reload();
          },1000);
        },
        error: function(error){
            var errors      = error.responseJSON;
            var errorsHtml  = '';
            $.each(errors.errors,function (k,v) {
                errorsHtml += '<li>'+ v + '</li>';
            });
            toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
            $('#loader_btn').css('display','none');
            $('#save_video_btn').removeAttr('disabled');
        }
      });
  }
  
});
