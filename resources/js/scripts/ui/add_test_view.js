/*=========================================================================================
    File Name: add_test_view.js
    Description: Add Test Contents
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
  //eneble select2 globally
  function enableSelect2(){
      // Basic Select2 select
      $("#select_questions").select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        placeholder: "Click here to select a question",
        maximumSelectionLength: max_questions,
        dropdownAutoWidth: true,
        width: '100%'
      });
      $('.select2-search__field').attr('placeholder','Click here');
      $('.select2-search__field').css('width','100%');
  }
  enableSelect2();
