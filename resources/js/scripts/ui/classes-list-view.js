/*=========================================================================================
    File Name: video-list-view.js
    Description: List View
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function() {
  "use strict"
  // init list view datatable
  var dataListView = $(".data-list-view").DataTable({
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: { selectRow: true }
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[5, 10, 15, 20], [5, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 5,
    buttons: [
      {
        text: "<i class='feather icon-plus'></i> Add Class",
        action: function() {
          $(this).removeClass("btn-secondary");
          location.href = 'class';
        },
        className: "btn-outline-primary"
      }
    ],
    initComplete: function(settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  });

  dataListView.on('draw.dt', function(){
    setTimeout(function(){
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });

  // init thumb view datatable
  var dataThumbView = $(".data-thumb-view").DataTable({
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: { selectRow: true }
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 4,
    buttons: [
      {
        text: "<i class='feather icon-plus'></i> Add Class",
        action: function() {
          $(this).removeClass("btn-secondary");
          location.href = 'class';
        },
        className: "btn-outline-primary"
      }
    ],
    initComplete: function(settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  })

  dataThumbView.on('draw.dt', function(){
    setTimeout(function(){
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });

  // To append actions dropdown before add new button
  var actionDropdown = $(".actions-dropodown")
  actionDropdown.insertBefore($(".top .actions .dt-buttons"))


  // Scrollbar
  if ($(".data-items").length > 0) {
    new PerfectScrollbar(".data-items", { wheelPropagation: false })
  }

  // Close sidebar
  $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
    $(".add-new-data").removeClass("show")
    $(".overlay-bg").removeClass("show")
  })

  // On Edit
  $('.action-edit').on("click",function(e){
      e.stopPropagation();
      var class_id   = $(this).attr('data-value');
      location.href = 'class/'+class_id;
  });
  //on view click button
  $('.action-view').on("click",function(e){
      e.stopPropagation();
      var classValueString = $(this).attr('data-value');
      var classValues      = parseClassData(classValueString);
      $("#view_class_name").val(atob(classValues[1]));
      $("#view_class_desc").val(atob(classValues[2]));
      $("#view_class_datetime").val(atob(classValues[3]));
      $("#view_class_duration").val(atob(classValues[4]));
      $("#view_class_join_url").val(atob(classValues[5]));
      $("#view_class_join_pwd").val(atob(classValues[6]));
      $("#view_class_modal").modal('show');
  });
  //parse class data
  function parseClassData(data){
      return data.split(",");
  }
  // On Delete
  $('.action-delete').on("click", function(e){
    var class_id = $(this).attr('data-value');
    e.stopPropagation();
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-danger ml-1',
      buttonsStyling: false,
    }).then(function (result) {
      if (result.value) {
        $.ajax({
          type: 'POST',
          dataType:'json',
          data:{
              _token : $('meta[name="csrf-token"]').attr('content'),
              class_id:class_id
          },
          url: 'delete-class',
          success: function(data){
            toastr.success(data.message, 'Success!', { "timeOut": 0 });
            setTimeout(function(){
              location.reload();
            },500);
          },
          error: function(error){
              var errors      = error.responseJSON;
              var errorsHtml  = '';
              $.each(errors.errors,function (k,v) {
                  errorsHtml += '<li>'+ v + '</li>';
              });
              toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
          }
        });
      }
      else{
        location.reload();
      }
    })
  });

  // dropzone init
  Dropzone.options.dataListUpload = {
    complete: function(files) {
      var _this = this
      // checks files in class dropzone and remove that files
      $(".hide-data-sidebar, .cancel-data-btn, .actions .dt-buttons").on(
        "click",
        function() {
          $(".dropzone")[0].dropzone.files.forEach(function(file) {
            file.previewElement.remove()
          })
          $(".dropzone").removeClass("dz-started")
        }
      )
    }
  }
  Dropzone.options.dataListUpload.complete()

  // mac chrome checkbox fix
  if (navigator.userAgent.indexOf("Mac OS X") != -1) {
    $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
  }

  //save video data ajax call
  function ajaxRequestVideoSave($form,event){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        type: 'POST',
        data:new FormData(jQuery('#video_data_form')[0]),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        url: 'save-video-data',
        success: function(data){
          $('#loader_btn').css('display','none');
          toastr.success(data.message, 'Success!', { "timeOut": 0 });
          $(".add-new-data").removeClass('show').addClass('hide');
          $(".overlay-bg").removeClass('show').addClass('hide');
          setTimeout(function(){
            location.reload();
          },1000);
        },
        error: function(error){
            var errors      = error.responseJSON;
            var errorsHtml  = '';
            $.each(errors.errors,function (k,v) {
                errorsHtml += '<li>'+ v + '</li>';
            });
            toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
            $('#loader_btn').css('display','none');
            $('#save_video_btn').removeAttr('disabled');
        }
      });
  }
  //validation for checking file inputs
  function checkFilesValidation(){
      var selected_image_file    = $('#hid_image_file').val();
      if(!selected_image_file){
        $("#image_form_group").removeClass('error').addClass('error');
        $("#error_image_ul").remove();
        $("#image_controls").append('<div class="help-block" id="error_image_ul"><ul role="alert"><li>Video Image is required</li></ul></div>');
        return false;
      }
      if(selected_image_file){
          $("#image_form_group").removeClass('error');
          $("#error_image_ul").remove();
      }
  }
  //image file input onchange function
  $(document).on('change','#video_image',function(){
    if($(this)[0].files.length > 0){
        $('#hid_image_file').val($(this)[0].files[0].name);
        $("#image-label").html($(this)[0].files[0].name);
    }
    checkFilesValidation();
});
  //enable form validation
  function enableFormValidation(edit){
      $("#video_data_form").find($("input,select,textarea")).not("[type=submit]").jqBootstrapValidation({
        preventSubmit: true,
          submitSuccess: function($form, event) {
            event.preventDefault();
            checkFilesValidation();
            $('#loader_btn').css('display','block');
            $('#save_video_btn').attr('disabled',true);
            ajaxRequestVideoSave($form,event);
          },
          filter: function() {
              return $(this).is(":visible");
          }
      });
  }
  var flagPlayer = 0;
  var provider   = 'youtube';
  //show video preview on video list page
  $('.action-preview').on("click", function(e){
      var videoURL = $(this).attr('data-value');
      var videoObj = parseVideo(videoURL);
      if (videoObj.type == 'youtube') {
          provider = 'youtube';
      }else if (videoObj.type == 'vimeo') {
          provider = 'vimeo';
      }
      e.stopPropagation();
      $("#video_preview_modal").modal('show');
      // video player  define
      if(flagPlayer == 0){
          const player = new Plyr('#player');
          // Expose
          window.player = player;
          flagPlayer = 1;
      }
      //set player data dynamically
      player.source = {
        type: 'video',
        sources: [
          {
            src: videoURL,
            provider: provider,
          },
        ],
      };
  });
  //event calls when a modal is hidden
  $('#video_preview_modal').on('hidden.bs.modal', function () {
      window.player.pause();
  });
  //parsing video urls
  function parseVideo (url) {
    // - Supported YouTube URL formats:
    //   - http://www.youtube.com/watch?v=My2FRPA3Gf8
    //   - http://youtu.be/My2FRPA3Gf8
    //   - https://youtube.googleapis.com/v/My2FRPA3Gf8
    // - Supported Vimeo URL formats:
    //   - http://vimeo.com/25451551
    //   - http://player.vimeo.com/video/25451551
    // - Also supports relative URLs:
    //   - //player.vimeo.com/video/25451551

    url.match(/(http:|https:|)\/\/(player.|www.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com))\/(video\/|embed\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/);

    if (RegExp.$3.indexOf('youtu') > -1) {
        var type = 'youtube';
    } else if (RegExp.$3.indexOf('vimeo') > -1) {
        var type = 'vimeo';
    }

    return {
        type: type,
        id: RegExp.$6
    };
  }
  //add video to multiple courses
  $('.add-to-courses').on("click", function(e){
      e.stopPropagation();
      var video_id = $(this).attr('data-id');
      getVideoCourses(video_id);
      $('#add_course_id_btn').attr('data-id',video_id);
      $('#course_select_modal').modal('show');
  });
  //ajax call to get already added courses for a video
  function getVideoCourses(video_id){
    $.ajax({
      type: 'POST',
      data:{
        _token : $('meta[name="csrf-token"]').attr('content'),
        video_id:video_id
      },
      dataType:'JSON',
      url: 'get-video-courses',
      success: function(data){
          var option_html = '';
          var selected    = '';
          if(data && data.status == 200){
            $.each(data.courses, function(key,value) {
                if(data.added_course_id.indexOf(value['id']) > -1){
                    selected = 'selected';
                }
                else{
                    selected = '';
                }
                option_html+= '<option '+selected+' value='+value['id']+'>'+value['course_name']+'</option>';
            });
            //append select options
            $("#select_courses").html(option_html);
            enableSelect2();
          }
      },
      error: function(error){
          
      }
    });
  }
  //eneble select2 globally
  function enableSelect2(){
      // Basic Select2 select
      $("#select_courses").select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        placeholder: "Click here to select a course",
        dropdownAutoWidth: true,
        width: '100%'
      });
      $('.select2-search__field').attr('placeholder','Click here');
      $('.select2-search__field').css('width','100%');
  }
  //add video to courses save btn click
  $('#add_course_id_btn').on("click", function(e){
      $('#loader_btn').css('display','block');
      $('#add_course_id_btn').attr('disabled',true);
      var selected_course_ids = $("#select_courses").val();
      var video_id            = $(this).attr('data-id');
      $.ajax({
        type: 'POST',
        data:{
          _token : $('meta[name="csrf-token"]').attr('content'),
          video_id:video_id,
          sel_course_id:selected_course_ids
        },
        dataType:'JSON',
        url: 'save-course-video',
        success: function(data){
          $('#loader_btn').css('display','none');
          $("#course_select_modal").modal('hide');
          toastr.success(data.message, 'Success!', { "timeOut": 0 });
          $('#add_course_id_btn').removeAttr('disabled');
        },
        error: function(error){
            var errors      = error.responseJSON;
            var errorsHtml  = '';
            $.each(errors.errors,function (k,v) {
                errorsHtml += '<li>'+ v + '</li>';
            });
            toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
            $('#loader_btn').css('display','none');
            $('#add_course_id_btn').removeAttr('disabled');
        }
      });
  });
});
