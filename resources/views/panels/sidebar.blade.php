@php
$configData = Helper::applClasses();
@endphp
<div
  class="main-menu menu-fixed {{($configData['theme'] === 'light') ? "menu-light" : "menu-dark"}} menu-accordion menu-shadow"
  data-scroll-to-active="true">
  <div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
      <li class="nav-item mr-auto">
        <a class="navbar-brand" href="dashboard-analytics">
          <div class="brand-logo"></div>
          <h2 class="brand-text mb-0">LMS</h2>
        </a>
      </li>
      <li class="nav-item nav-toggle">
        <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
          <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
          <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block primary collapse-toggle-icon"
            data-ticon="icon-disc">
          </i>
        </a>
      </li>
    </ul>
  </div>
  <div class="shadow-bottom"></div>
  <div class="main-menu-content">
      <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">        
          <li class="nav-item  {{ (request()->is('dashboard')) ? 'active' : '' }}">
          <a href="{{asset('dashboard')}}">
          <i class="feather icon-home"></i>
          <span class="menu-title" data-i18n="">Dashboard</span>
          </a>
          </li>
          @canany(['list courses', 'list videos', 'list classes', 'list tests'])
              <li class="navigation-header">
                  <span>MASTER DATA</span>
              </li>
              @can('list courses')
                <li class="nav-item {{ (request()->is('courses')) ? 'active' : '' }}">
                  <a href="{{asset('courses')}}">
                  <i class="feather icon-layers"></i>
                  <span class="menu-title" data-i18n="nav.Courses">Courses</span>
                  </a>
                </li>
              @endcan
              @can('list videos')
                <li class="nav-item {{ (request()->is('videos')) ? 'active' : '' }}">
                  <a href="{{asset('videos')}}">
                  <i class="feather icon-video"></i>
                  <span class="menu-title" data-i18n="nav.Videos">Videos</span>
                  </a>
                </li>
              @endcan
              @can('list classes')
                <li class="nav-item {{ (request()->is('classes')) ? 'active' : '' }}">
                  <a href="{{asset('classes')}}">
                  <i class="feather icon-layers"></i>
                  <span class="menu-title" data-i18n="nav.Classes">Class</span>
                  </a>
                </li>
              @endcan
              @can('list tests')
                <li class="nav-item {{ (request()->is('tests')) ? 'active' : '' }}">
                  <a href="{{asset('tests')}}">
                  <i class="feather icon-file-text"></i>
                  <span class="menu-title" data-i18n="nav.Tests">Tests</span>
                  </a>
                </li>
              @endcan
          @endcanany
          @canany(['list roles', 'list admin users', 'list users'])
              <li class="navigation-header">
                <span>USER MANAGEMENT</span>
              </li>
              @can('list roles')            
                <li class="nav-item {{ (request()->is('roles')) ? 'active' : '' }}">
                  <a href="{{asset('roles')}}">
                  <i class="feather icon-user"></i>
                  <span class="menu-title" data-i18n="nav.roles">Roles</span>
                  </a>
                </li>
              @endcan
              @can('list admin users')
                <li class="nav-item {{ (request()->is('admin-users')) ? 'active' : '' }}">
                <a href="{{asset('admin-users')}}">
                <i class="feather icon-users"></i>
                <span class="menu-title" data-i18n="nav.AdminUsers">Admin Users</span>
                </a>
                </li>
              @endcan
              @can('list users')
                <li class="nav-item {{ (request()->is('users')) ? 'active' : '' }}">
                  <a href="{{asset('users')}}">
                  <i class="feather icon-users"></i>
                  <span class="menu-title" data-i18n="nav.Users">LMS Users</span>
                  </a>
                </li>
              @endcan
          @endcanany
          @canany(['list mcq', 'list desc'])
              <li class="navigation-header">
                  <span>QUESTION MANAGEMENT</span>
              </li>
              @can('list mcq')           
              <li class="nav-item {{ (request()->is('mcq-ques')) ? 'active' : '' }}">
                <a href="{{asset('mcq-ques')}}">
                <i class="feather icon-grid"></i>
                <span class="menu-title" data-i18n="nav.mcq">MCQ Questions</span>
                </a>
              </li>
              @endcan
              @can('list desc')
                <li class="nav-item {{ (request()->is('desc-ques')) ? 'active' : '' }}">
                  <a href="{{asset('desc-ques')}}">
                  <i class="feather icon-file-text"></i>
                  <span class="menu-title" data-i18n="nav.desc">Descriptive Questions</span>
                  </a>
                </li>
              @endcan
          @endcanany
      </ul>
  </div>
</div>
<!-- END: Main Menu-->
