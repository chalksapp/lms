@extends('frontend.pre_auth_layouts.master')

@push('css')
<style>
    #fe_login_form .error {
        width: 100%;
        margin-top: .25rem;
        font-size: 80%;
        color: #dc3545;
        font-weight: bold;
    }
</style>
@endpush
@section('content')
<!-- ================================
       START LOGIN AREA
================================= -->
<section class="login-area section--padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mx-auto">
                <div class="card-box-shared">
                    <div class="card-box-shared-title text-center">
                        <h3 class="widget-title font-size-25">Login to Your Account!</h3>
                    </div>
                    <div class="card-box-shared-body">
                        <div class="contact-form-action">
                            <form id="fe_login_form" action="{{ route('lms_user.login') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Email</label>
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="email" placeholder="Email" value="{{ old('email') }}">
                                                <span class="la la-envelope input-icon"></span>
                                                <span class="invalid-feedback" id="fe_login_email"
                                                  @if ($errors->has('email'))
                                                      style="display: inline;"
                                                @endif
                                                >
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Password<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="password" name="password" placeholder="Password">
                                                <span class="la la-lock input-icon"></span>
                                                <span class="invalid-feedback" id="fe_login_password"
                                                @if ($errors->has('password'))
                                                     style="display: inline;"
                                                @endif
                                                >
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="custom-checkbox d-flex justify-content-between">
                                                <input type="checkbox" id="chb1">
                                                <label for="chb1">Remember Me</label>
                                                <a href="recover.html" class="primary-color-2"> Forgot my password?</a>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12 ">
                                        <div class="btn-box">
                                            <button class="theme-btn" type="submit">login account</button>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                </div><!-- end row -->
                            </form>
                        </div><!-- end contact-form -->
                    </div>
                </div>
            </div><!-- end col-lg-7 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end login-area -->
<!-- ================================
       START LOGIN AREA
================================= -->
@endsection
@push('scripts')
<script>
    $("#fe_login_form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            }
        },
        // Specify validation error messages
        messages: {
            password: {
                required: "Please enter a password.",
                minlength: "Your password must be at least 8 characters long."
            },
            email: {
                required: "Please enter an email address.",
                email: "You have entered an invalid email address."
            },
        },
        // errorElement : 'strong',
        // errorLabelContainer: '.invalid-feedback',
        // errorClass: 'invalid-feedback',
        errorElement: 'span',
        submitHandler: function(form) {
            form.submit();
        }
    });
</script>
@endpush