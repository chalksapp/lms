@extends('frontend.pre_auth_layouts.master')
@push('css')
    <style>
        #fe_registration_form .error {
            width: 100%;
            margin-top: .25rem;
            font-size: 80%;
            color: #dc3545;
            font-weight: bold;
        }
    </style>
@endpush
@section('content')

<!-- ================================
       START SIGN UP AREA
================================= -->
<section class="sign-up section--padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mx-auto">
                <div class="card-box-shared">
                    <div class="card-box-shared-title text-center">
                        <h3 class="widget-title font-size-25">Create an Account and <br> Start Learning!</h3>
                    </div>
                    <div class="card-box-shared-body">
                        <div class="contact-form-action">
                            <form id="fe_registration_form" action="{{ route('lms_user.register') }}" method="post">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-lg-12 ">
                                        <div class="input-box">
                                            <label class="label-text">Name</label>
                                            <div class="form-group">
                                                <input class="form-control" type="text" disabled placeholder="Name" value="{{ $user->name ?? '' }}">
                                                <span class="la la-user input-icon"></span>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Email Address</label>
                                            <div class="form-group">
                                                <input class="form-control" type="text" disabled placeholder="Email address" value="{{ $user->email ?? '' }}">
                                                <input type="hidden" name="email" value="{{ $user->email ?? '' }}">
                                                <span class="la la-envelope input-icon"></span>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Phone<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="phone" placeholder="Phone" value="{{ old('phone') ?? '' }}">
                                                <span class="la la-phone input-icon"></span>
                                                <span class="invalid-feedback" id="fe_registration_phone"
                                                      @if ($errors->has('phone'))
                                                      style="display: inline;"
                                                @endif
                                                >
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Password<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="password" id="password" name="password" placeholder="Password">
                                                <span class="la la-lock input-icon"></span>
                                                <span class="invalid-feedback" id="fe_registration_password"
                                                      @if ($errors->has('password'))
                                                      style="display: inline;"
                                                @endif
                                                >
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12">
                                        <div class="input-box">
                                            <label class="label-text">Confirm Password<span class="primary-color-2 ml-1">*</span></label>
                                            <div class="form-group">
                                                <input class="form-control" type="password" name="password_confirmation" placeholder="Confirm password">
                                                <span class="la la-lock input-icon"></span>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <div class="custom-checkbox">
                                                <input type="checkbox" name="privacy_policy" id="privacy_policy">
                                                <label id="label_for_privacy_policy" for="privacy_policy">By signing up, you agree to our <a href="#">Terms of Use</a> and
                                                    <a href="#">Privacy Policy</a>.
                                                </label><br />
                                                <span id="privacy_policy-error" class="error"></span>
                                            </div>

                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12 ">
                                        <div class="btn-box">
                                            <button class="theme-btn" type="submit">register account</button>
                                        </div>
                                    </div><!-- end col-md-12 -->
                                    <div class="col-lg-12">
                                        <p class="mt-4">Already have an account? <a href="{{ route('lms_user.login_page') }}" class="primary-color-2">Log in</a></p>
                                    </div><!-- end col-md-12 -->
                                </div><!-- end row -->
                            </form>
                        </div><!-- end contact-form -->
                    </div>
                </div>
            </div><!-- end col-md-7 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end sign-up -->
<!-- ================================
       START SIGN UP AREA
================================= -->
@endsection
@push('scripts')
    <script>
        $.validator.addMethod("phone_validation", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            var phone_pattern = /([0-9]{10})|(\([0-9]{3}\)\s+[0-9]{3}\-[0-9]{4})/;
            return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(phone_pattern) && phone_number.length < 15;
        }, "Please specify a valid phone number");

        $("#fe_registration_form").validate({
            rules: {
                phone: {
                    required: true,
                    phone_validation: true,
                },
                password: {
                    required: true,
                    minlength: 8
                },
                password_confirmation: {
                    equalTo : "#password",
                    minlength: 8
                }
            },
            // Specify validation error messages
            messages: {
                password: {
                    required: "Please enter a password.",
                    minlength: "Your password must be at least 8 characters long."
                },
                phone: {
                    required: "Please enter a phone number.",
                    phone_validation: "You have entered an invalid phone number."
                },
                password_confirmation: {
                    equalTo: "Passwords do not match.",
                    minlength: "Your password must be at least 8 characters long."
                }
            },
            // errorElement : 'strong',
            // errorLabelContainer: '.invalid-feedback',
            // errorClass: 'invalid-feedback',
            errorElement: 'span',
            submitHandler: function(form) {
                if (!$('#privacy_policy').is(':checked')) {
                    $('#privacy_policy-error').html('Please agree to our terms and conditions to complete registration.');
                    $('#privacy_policy-error').show();
                    return false;
                }
                form.submit();
            }
        });
    </script>
@endpush