<!-- start scroll top -->
<div id="scroll-top">
    <i class="fa fa-angle-up" title="Go top"></i>
</div>
<!-- end scroll top -->

<!-- template js files -->
<script src="{{ asset('frontend_assets/js/jquery-3.4.1.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/popper.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/magnific-popup.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/isotope.js') }}"></script>
<script src="{{ asset('frontend_assets/js/waypoint.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/fancybox.js') }}"></script>
<script src="{{ asset('frontend_assets/js/wow.js') }}"></script>
<script src="{{ asset('frontend_assets/js/smooth-scrolling.js') }}"></script>
<script src="{{ asset('frontend_assets/js/date-time-picker.js') }}"></script>
<script src="{{ asset('frontend_assets/js/emojionearea.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/jquery.filer.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/tooltipster.bundle.min.js') }}"></script>
<script src="{{ asset('frontend_assets/js/main.js') }}"></script>
<script src="{{ asset('frontend_assets/js/jquery.validate.min.js') }}"></script>