<div class="dashboard-sidebar">
    <div class="dashboard-nav-trigger">
        <div class="dashboard-nav-trigger-btn">
            <i class="la la-bars"></i> Dashboard Nav
        </div>
    </div>
    <div class="dashboard-nav-container">
        <div class="humburger-menu">
            <div class="humburger-menu-lines side-menu-close"></div><!-- end humburger-menu-lines -->
        </div><!-- end humburger-menu -->
        <div class="side-menu-wrap">
            <ul class="side-menu-ul">
                <li class="sidenav__item page-active"><a href="dashboard.html"><i class="la la-dashboard"></i> Dashboard</a></li>
                <!--li class="sidenav__item"><a href="dashboard-profile.html"><i class="la la-user"></i>My Profile</a></li>
                <li class="sidenav__item"><a href="dashboard-courses.html"><i class="la la-file-video-o"></i>My Courses</a></li>
                <li class="sidenav__item"><a href="dashboard-quiz.html"><i class="la la-bolt"></i>Quiz Attempts</a></li>
                <li class="sidenav__item"><a href="dashboard-bookmark.html"><i class="la la-bookmark"></i>Bookmarks</a></li>
                <li class="sidenav__item"><a href="dashboard-enrolled-courses.html"><i class="la la-graduation-cap"></i>Enrolled Courses</a></li>
                <li class="sidenav__item"><a href="dashboard-message.html"><i class="la la-bell"></i>Message <span class="badge badge-info radius-rounded p-1 ml-1">2</span></a></li>
                <li class="sidenav__item"><a href="dashboard-reviews.html"><i class="la la-star"></i>Reviews</a></li>
                <li class="sidenav__item"><a href="dashboard-earnings.html"><i class="la la-dollar"></i>Earnings</a></li>
                <li class="sidenav__item"><a href="dashboard-withdraw.html"><i class="la la-money"></i>Withdraw</a></li>
                <li class="sidenav__item"><a href="dashboard-purchase-history.html"><i class="la la-shopping-cart"></i>Purchase History</a></li>
                <li class="sidenav__item"><a href="dashboard-submit-course.html"><i class="la la-plus-circle"></i>Submit Course</a></li>
                <li class="sidenav__item"><a href="dashboard-settings.html"><i class="la la-cog"></i>Settings</a></li-->
                <li class="sidenav__item"><a href="{{ route('lms_user.logout') }}"><i class="la la-power-off"></i> Logout</a></li>
                <!--li class="sidenav__item"><a href="javascript:void(0)" data-toggle="modal" data-target=".account-delete-modal" ><i class="la la-trash"></i> Delete Account</a></li-->
            </ul>
        </div><!-- end side-menu-wrap -->
    </div>
</div><!-- end dashboard-sidebar -->