<div class="user-action-wrap">
    <div class="notification-item user-action-item">
        <div class="dropdown">
            <button class="notification-btn dot-status online-status dropdown-toggle" type="button" id="userDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img src="{{ asset('frontend_assets/images/team7.jpg') }}" alt="John-doe">
            </button>
            <div class="dropdown-menu" aria-labelledby="userDropdownMenu">
                <div class="mess-dropdown">
                    <div class="mess__title d-flex align-items-center">
                        <div class="image">
                            <a href="#">
                                <img src="{{ asset('frontend_assets/images/team7.jpg') }}" alt="John Doe">
                            </a>
                        </div>
                        <div class="content">
                            <h4 class="widget-title font-size-16">
                                <a href="#" class="text-white">
                                    {{ auth()->user()->name }}
                                </a>
                            </h4>
                            <span class="email">{{ auth()->user()->email }}</span>
                        </div>
                    </div><!-- end mess__title -->
                    <div class="mess__body">
                        <ul class="list-items">
                            <li class="mb-0">
                                <a href="{{ route('lms_user.logout') }}" class="d-block">
                                    <i class="la la-power-off"></i> Logout
                                </a>
                            </li>
                        </ul>
                    </div><!-- end mess__body -->
                </div><!-- end mess-dropdown -->
            </div><!-- end dropdown-menu -->
        </div><!-- end dropdown -->
    </div>
</div>