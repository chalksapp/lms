<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="author" content="TechyDevs">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ env('APP_NAME') }}</title>

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Mukta:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" sizes="16x16" href="{{ asset('frontend_assets/images/favicon.png') }}">

    @stack('style')
    @include('frontend.layouts-dashboard.style')
</head>
<body>
    @include('frontend.layouts-dashboard.header')
    <section class="dashboard-area">
        @include('frontend.layouts-dashboard.left_nav')
        @yield('content')
    </section>
    @include('frontend.layouts-dashboard.footer')
    @include('frontend.layouts-dashboard.script')
    @stack('script')
</body>
</html>