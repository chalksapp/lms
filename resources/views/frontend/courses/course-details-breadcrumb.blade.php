<!-- ================================
    START BREADCRUMB AREA
================================= -->
<section class="breadcrumb-area breadcrumb-detail-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content breadcrumb-detail-content">
                    <div class="section-heading">
                        <span class="badge-label">{{ $courseData->course_level }}</span>
                        <h2 class="section__title mt-1">{{ $courseData->name }}</h2>
                        <h4 class="widget-title mt-2">Course Duration: {{ $courseData->course_duration }} Hrs {{ $courseData->course_period }}</h4>
                    </div>
                    <ul class="breadcrumb__list mt-2">
                        <li>Created by <a href="teacher-detail.html">Mark Hardson</a></li>
                        <li>
                            <i class="la la-star"></i>
                            <i class="la la-star"></i>
                            <i class="la la-star"></i>
                            <i class="la la-star"></i>
                            <i class="la la-star-half-o"></i>
                            4.5 (173,750 ratings)
                        </li>
                        <li>{{ count($courseData->users) }} Students enrolled</li>
                        <li><i class="la la-globe"></i> English</li>
                        <li>Last updated on {{ $courseData->updated_at }}</li>
                    </ul>
                </div><!-- end breadcrumb-content -->
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end breadcrumb-area -->
<!-- ================================
    END BREADCRUMB AREA
================================= -->