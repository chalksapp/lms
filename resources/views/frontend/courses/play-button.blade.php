<a href="javascript:void(0);" class="card__img">
    @if($course->course_image != '')
        <img src="{{ asset("course_files/$course->id/$course->course_image") }}" alt="">
    @else
        <img src="{{ asset('frontend_assets/images/img9.jpg') }}" alt="">
    @endif
    <div class="play-button">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="-307.4 338.8 91.8 91.8" style=" enable-background:new -307.4 338.8 91.8 91.8;" xml:space="preserve">
                                                                <style type="text/css">
                                                                    .st0{opacity:0.6;fill:#000000;border-radius: 100px;enable-background:new;}
                                                                    .st1{fill:#FFFFFF;}
                                                                </style>
            <g>
                <circle class="st0" cx="-261.5" cy="384.7" r="45.9"/><path class="st1" d="M-272.9,363.2l35.8,20.7c0.7,0.4,0.7,1.3,0,1.7l-35.8,20.7c-0.7,0.4-1.5-0.1-1.5-0.9V364C-274.4,363.3-273.5,362.8-272.9,363.2z"/>
            </g>
                                                            </svg>
    </div>
</a>