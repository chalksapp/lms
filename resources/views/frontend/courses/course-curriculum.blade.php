<div class="curriculum-wrap margin-bottom-60px">
    <div class="curriculum-header d-flex align-items-center justify-content-between">
        <div class="curriculum-header-left">
            <h3 class="widget-title">Curriculum</h3>
        </div>
        <div class="curriculum-header-right">
            <span class="curriculum-total__text"><strong>Total Lectures:</strong> {{ array_count_values(array_column($courseData->curriculum, 'type'))['video'] ?? 0 }}</span>
            <span class="curriculum-total__text"><strong>Total Quiz:</strong> {{ array_count_values(array_column($courseData->curriculum, 'type'))['test'] ?? 0 }}</span>

        </div>
    </div><!-- end curriculum-header -->
    <div class="curriculum-content">
        <div class="accordion accordion-shared" id="accordionExample">
            @foreach($courseData->curriculum as $curriculum)
                @if($curriculum->type == 'video')
                    @include('frontend.courses.curriculum-video')
                @endif
                @if($curriculum->type == 'test')
                    @include('frontend.courses.curriculum-test')
                @endif
            @endforeach
        </div><!-- end accordion -->
    </div><!-- end curriculum-content -->
</div><!-- end curriculum-wrap -->