<div class="card">
    <div class="card-header" id="headingOne">
        <h2 class="mb-0">
            <button class="btn btn-link collapsed d-flex align-items-center justify-content-between" type="button" data-toggle="collapse" data-target="#collapse{{ $curriculum->id }}" aria-expanded="false" aria-controls="collapse{{ $curriculum->id }}">
                <i class="fa fa-angle-up"></i>
                <i class="fa fa-angle-down"></i>
                {{ $curriculum->content->test_title ?? ''}}
                <span>1 Quiz</span>
            </button>
        </h2>
    </div><!-- end card-header -->
    <div id="collapse{{ $curriculum->id }}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
        <div class="card-body">
            <ul class="list-items">
                <li>
                    <a href="javascript:void(0)" class="primary-color-2 d-flex align-items-center justify-content-between" data-toggle="modal" data-target=".preview-modal-form">
                                        <span><i class="fa fa-bolt mr-2"></i>{{ $curriculum->content->test_desc ?? '' }}
                        @if($curriculum->content->test_status == 1)
                            <span class="badge-label">Preview</span></span>
                        @else
                            <span class="badge-label badge-secondary">Locked</span>
                        @endif
                        <span class="course-duration">03:07</span>
                    </a>
                </li>

                {{--<li>
                    <a href="javascript:void(0)" class="d-flex align-items-center justify-content-between">
                        <span><i class="fa fa-file mr-2"></i>Download All Course Materials and FAQ</span>
                        <span class="course-duration">2:02</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)" class="d-flex align-items-center justify-content-between">
                        <span><i class="fa fa-bolt mr-2"></i>Data Entry Techniques in Excel</span>
                        <span class="course-duration">1 question</span>
                    </a>
                </li>--}}
            </ul>
        </div><!-- end card-body -->
    </div><!-- end collapse -->
</div>