<div class="instructor-wrap padding-top-50px padding-bottom-45px">
    <h3 class="widget-title">About the instructor</h3>
    <div class="instructor-content margin-top-30px d-flex">
        <div class="instructor-img">
            <a href="#" class="instructor__avatar">
                <img src="{{ asset('frontend_assets/images/team7.jpg') }}" alt="">
            </a>
            <ul class="list-items">
                <li><span class="la la-star"></span> 4.6 Instructor Rating</li>
                <li><span class="la la-user"></span> 45,786 Students</li>
                <li><span class="la la-comment-o"></span> 2,533 Reviews</li>
                <li><span class="la la-play-circle-o"></span> 24 Courses</li>
                <li><span class="la la-eye"></span><a href="teacher-detail.html"> View all Courses</a></li>
            </ul>
        </div><!-- end instructor-img -->
        <div class="instructor-details">
            <div class="instructor-titles">
                <h3 class="widget-title"><a href="teacher-detail.html">Mark Hardson</a></h3>
                <p class="instructor__subtitle">Joined 4 years ago</p>
                <p class="instructor__meta">Digital marketer and writer. Lover of details.</p>
            </div><!-- end instructor-titles -->
            <div class="instructor-desc">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                <div class="collapse" id="show-more-content">
                    <p> <strong>01: Finance</strong> - It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p> <strong>01: Data science</strong> - It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p> <strong>01: Entrepreneurship</strong> - It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p> <strong>01: Blockchain for Business</strong> - It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p>By choosing 365 Careers, you make sure you will learn from proven experts, who have a passion for teaching, and can take you from beginner to pro in the shortest possible amount of time.  </p>
                    <p>If you want to become a financial analyst, a finance manager, an FP&A analyst, an investment banker, a business executive, an entrepreneur, a business intelligence analyst, a data analyst, or a data scientist, <strong>Mark Hardson's courses are the perfect course to start.</strong> </p>
                </div>
                <div class="btn-box pt-2 d-inline-block">
                    <a class="collapsed link-collapsed" data-toggle="collapse" href="#show-more-content" role="button" aria-expanded="false" aria-controls="show-more-content">
                        <span class="link-collapse-read-more">Read more</span>
                        <span class="link-collapse-active">Read less</span>
                        <div class="ml-1">
                            <i class="la la-plus"></i>
                            <i class="la la-minus"></i>
                        </div>
                    </a>
                </div>
            </div><!-- end instructor-desc -->
        </div><!-- end instructor-details -->
    </div><!-- end instructor-content -->
</div><!-- end instructor-wrap -->