<div class="lecture-overview-item">
    <div class="question-overview-filter-wrap my-course-filter-wrap d-flex align-items-center">
        <div class="my-course-search-content">
            <div class="question-overview-filter-item">
                <span class="badge font-size-14 font-weight-semi-bold">Search</span>
                <div class="contact-form-action mt-2">
                    <form onsubmit="return false">
                        <div class="input-box">
                            <div class="form-group mb-0">
                                <input class="form-control" type="text" id="search_course" placeholder="Search courses">
                                <span id="search_course_btn" class="la la-search search-icon"></span>
                            </div>
                        </div><!-- end input-box -->
                    </form>
                </div><!-- end contact-form-action -->
            </div><!-- end question-overview-filter-item -->
        </div><!-- end my-course-search-content -->
    </div>
</div><!-- end lecture-overview-item -->