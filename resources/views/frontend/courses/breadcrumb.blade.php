<!-- ================================
    START BREADCRUMB AREA
================================= -->
<section class="breadcrumb-area my-courses-bread">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-content my-courses-bread-content">
                    <div class="section-heading">
                        <h2 class="section__title">Courses</h2>
                    </div>
                </div><!-- end breadcrumb-content -->
                <div class="my-courses-tab">
                    <div class="section-tab section-tab-2">
                        <ul class="nav nav-tabs" role="tablist" id="review">
                            <li role="presentation">
                                <a href="#all-courses" role="tab" data-toggle="tab" class="active" aria-selected="true">
                                    Enrolled Courses
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#enrolled-courses" role="tab" data-toggle="tab" aria-selected="false">
                                    All courses
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#wishlist" role="tab" data-toggle="tab" aria-selected="false">
                                    Wishlist
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#archived" role="tab" data-toggle="tab" aria-selected="false">
                                    Archived
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- end col-lg-12 -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end breadcrumb-area -->
<!-- ================================
    END BREADCRUMB AREA
================================= -->