<div class="review-wrap">
    <h3 class="widget-title">Student feedback</h3>
    <div class="review-content margin-top-40px margin-bottom-50px d-flex">
        <div class="review-rating-summary">
            <div class="review-rating-summary-inner d-flex align-items-end">
                <div class="stats-average__count">
                    <span class="stats-average__count-count">4.6</span>
                </div><!-- end stats-average__count -->
                <div class="stats-average__rating d-flex">
                    <ul class="review-stars d-flex">
                        <li><span class="la la-star"></span></li>
                        <li><span class="la la-star"></span></li>
                        <li><span class="la la-star"></span></li>
                        <li><span class="la la-star"></span></li>
                        <li><span class="la la-star-o"></span></li>
                    </ul>
                    <span class="star-rating-wrap">
                                            <span class="star__rating">(2,533)</span>
                                        </span>
                </div><!-- end stats-average__rating -->
            </div><!-- end review-rating-summary-inner -->
            <div class="course-rating-text">
                <p class="course-rating-text__text">Course Rating</p>
            </div><!-- end course-rating-text -->
        </div><!-- end review-rating-summary -->
        <div class="review-rating-widget">
            <div class="review-rating-rate">
                <ul>
                    <li class="review-rating-rate__items">
                        <div class="review-rating-inner__item">
                            <div class="review-rating-rate__item-text">5 stars</div>
                            <div class="review-rating-rate__item-fill">
                                <span class="review-rating-rate__item-fill__fill rating-fill-width1"></span>
                            </div>
                            <div class="review-rating-rate__item-percent-text">77 %</div>
                        </div>
                    </li>
                    <li class="review-rating-rate__items">
                        <div class="review-rating-inner__item">
                            <div class="review-rating-rate__item-text">4 stars</div>
                            <div class="review-rating-rate__item-fill">
                                <span class="review-rating-rate__item-fill__fill rating-fill-width2"></span>
                            </div>
                            <div class="review-rating-rate__item-percent-text">54 %</div>
                        </div>
                    </li>
                    <li class="review-rating-rate__items">
                        <div class="review-rating-inner__item">
                            <div class="review-rating-rate__item-text">3 stars</div>
                            <div class="review-rating-rate__item-fill">
                                <span class="review-rating-rate__item-fill__fill rating-fill-width3"></span>
                            </div>
                            <div class="review-rating-rate__item-percent-text">14 %</div>
                        </div>
                    </li>
                    <li class="review-rating-rate__items">
                        <div class="review-rating-inner__item">
                            <div class="review-rating-rate__item-text">2 stars</div>
                            <div class="review-rating-rate__item-fill">
                                <span class="review-rating-rate__item-fill__fill rating-fill-width4"></span>
                            </div>
                            <div class="review-rating-rate__item-percent-text">5 %</div>
                        </div>
                    </li>
                    <li class="review-rating-rate__items">
                        <div class="review-rating-inner__item">
                            <div class="review-rating-rate__item-text">1 stars</div>
                            <div class="review-rating-rate__item-fill">
                                <span class="review-rating-rate__item-fill__fill rating-fill-width5"></span>
                            </div>
                            <div class="review-rating-rate__item-percent-text">2 %</div>
                        </div>
                    </li>
                </ul>
            </div><!-- end review-rating-rate -->
        </div><!-- end review-rating-widget -->
    </div><!-- end review-content -->
    <div class="section-block"></div>
    <div class="comments-wrapper margin-top-50px">
        <h3 class="widget-title"> Reviews</h3>
        <ul class="comments-list padding-top-30px">
            <li>
                <div class="comment">
                    <div class="comment-avatar">
                        <img class="avatar__img" alt="" src="{{ asset('frontend_assets/images/team7.jpg') }}">
                    </div>
                    <div class="comment-body">
                        <div class="meta-data">
                            <h3 class="comment__author">adam smith</h3>
                            <p class="comment__date">17 Dec, 2018 - 4:00 pm</p>
                            <ul class="review-stars review-stars1">
                                <li><span class="la la-star"></span></li>
                                <li><span class="la la-star"></span></li>
                                <li><span class="la la-star"></span></li>
                                <li><span class="la la-star"></span></li>
                                <li><span class="la la-star"></span></li>
                            </ul>
                        </div>
                        <p class="comment-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation.
                        </p>
                        <div class="comment-reply">
                            <a class="theme-btn" href="#" data-toggle="modal" data-target=".modal-action-form">
                                <span class="la la-mail-reply"></span> Reply
                            </a>
                            <div class="helpful__action d-flex align-items-center">
                                <span class="helpful__action-text">Was this review helpful?</span>
                                <button class="btn">Yes</button>
                                <button class="btn">No</button>
                                <div class="msg-action-dot">
                                    <div class="dropdown">
                                        <a class="action-dot" href="#" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="la la-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu border" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target=".report-modal-form"><i class="la la-flag mr-2"></i>Report abuse</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end comment -->
                <ul class="comments-reply">
                    <li>
                        <div class="comment">
                            <div class="comment-avatar">
                                <img class="avatar__img" alt="" src="{{ asset('frontend_assets/images/team8.jpg') }}">
                            </div>
                            <div class="comment-body">
                                <div class="meta-data">
                                    <h3 class="comment__author">Jhon doe</h3>
                                    <p class="comment__date">17 Dec, 2018 - 4:00 pm</p>
                                    <ul class="review-stars review-stars2">
                                        <li><span class="la la-star"></span></li>
                                        <li><span class="la la-star"></span></li>
                                        <li><span class="la la-star"></span></li>
                                        <li><span class="la la-star"></span></li>
                                        <li><span class="la la-star"></span></li>
                                    </ul>
                                </div>
                                <p class="comment-content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    Ut enim ad minim veniam.
                                </p>
                                <div class="comment-reply">
                                    <a class="theme-btn" href="#" data-toggle="modal" data-target=".modal-action-form">
                                        <span class="la la-mail-reply"></span> Reply
                                    </a>
                                    <div class="helpful__action d-flex align-items-center">
                                        <span class="helpful__action-text">Was this review helpful?</span>
                                        <button class="btn">Yes</button>
                                        <button class="btn">No</button>
                                        <div class="msg-action-dot">
                                            <div class="dropdown">
                                                <a class="action-dot" href="#" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="la la-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu border" aria-labelledby="dropdownMenuLink2">
                                                    <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target=".report-modal-form"><i class="la la-flag mr-2"></i>Report abuse</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul><!-- end comment -->
                <div class="comment">
                    <div class="comment-avatar">
                        <img class="avatar__img" alt="" src="{{ asset('frontend_assets/images/team9.jpg') }}">
                    </div>
                    <div class="comment-body">
                        <div class="meta-data">
                            <h3 class="comment__author">Mike Doe</h3>
                            <p class="comment__date">17 Dec, 2018 - 4:00 pm</p>
                            <ul class="review-stars review-stars3">
                                <li><span class="la la-star"></span></li>
                                <li><span class="la la-star"></span></li>
                                <li><span class="la la-star"></span></li>
                                <li><span class="la la-star"></span></li>
                                <li><span class="la la-star"></span></li>
                            </ul>
                        </div>
                        <p class="comment-content">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation.
                        </p>
                        <div class="comment-reply">
                            <a class="theme-btn" href="#" data-toggle="modal" data-target=".modal-action-form">
                                <span class="la la-mail-reply"></span> Reply
                            </a>
                            <div class="helpful__action d-flex align-items-center">
                                <span class="helpful__action-text">Was this review helpful?</span>
                                <button class="btn">Yes</button>
                                <button class="btn">No</button>
                                <div class="msg-action-dot">
                                    <div class="dropdown">
                                        <a class="action-dot" href="#" id="dropdownMenuLink3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="la la-ellipsis-v"></i>
                                        </a>
                                        <div class="dropdown-menu border" aria-labelledby="dropdownMenuLink3">
                                            <a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target=".report-modal-form"><i class="la la-flag mr-2"></i>Report abuse</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end comment -->
            </li>
        </ul>
        <div class="see-more-review-btn margin-bottom-50px">
            <div class="btn-box text-center">
                <button type="button" class="theme-btn theme-btn-light">load more reviews</button>
            </div><!-- end btn-box -->
        </div>
        <div class="review-form">
            <h3 class="widget-title">Add a Review</h3>
            <div class="rating-wrap mt-4">
                <div class="rating-shared rating-shared-box d-inline-block">
                    <fieldset>
                        <input type="radio" id="star5" name="rating" value="5" /><label for="star5" title="5 Star"></label>
                        <input type="radio" id="star4" name="rating" value="4" /><label for="star4" title="4 Star"></label>
                        <input type="radio" id="star3" name="rating" value="3" /><label for="star3" title="3 Star"></label>
                        <input type="radio" id="star2" name="rating" value="2" /><label for="star2" title="2 Star"></label>
                        <input type="radio" id="star1" name="rating" value="1" /><label for="star1" title="1 Star"></label>
                    </fieldset>
                </div>
            </div>
            <div class="contact-form-action margin-top-35px">
                <form method="post">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="input-box">
                                <label class="label-text">Name<span class="primary-color-2 ml-1">*</span></label>
                                <div class="form-group">
                                    <input class="form-control" type="text" name="name" placeholder="Your name">
                                    <i class="la la-user input-icon"></i>
                                </div>
                            </div>
                        </div><!-- end col-lg-6 -->
                        <div class="col-lg-6">
                            <div class="input-box">
                                <label class="label-text">Email<span class="primary-color-2 ml-1">*</span></label>
                                <div class="form-group">
                                    <input class="form-control" type="email" name="email" placeholder="Email address">
                                    <i class="la la-envelope-o input-icon"></i>
                                </div>
                            </div>
                        </div><!-- end col-lg-12 -->
                        <div class="col-lg-12">
                            <div class="input-box">
                                <label class="label-text">Message<span class="primary-color-2 ml-1">*</span></label>
                                <div class="form-group">
                                    <textarea class="message-control form-control" name="message" placeholder="Write message"></textarea>
                                    <i class="la la-pencil input-icon"></i>
                                </div>
                            </div>
                        </div><!-- end col-lg-12 -->
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="custom-checkbox">
                                    <input type="checkbox" id="chb1">
                                    <label for="chb1">Save my name, and email in this browser for the next time I comment.</label>
                                </div>
                            </div>
                        </div><!-- end col-lg-12 -->
                        <div class="col-lg-12">
                            <div class="btn-box">
                                <button class="theme-btn" type="submit">submit review</button>
                            </div>
                        </div><!-- end col-md-12 -->
                    </div><!-- end row -->
                </form>
            </div><!-- end contact-form-action -->
        </div><!-- end review-form -->
    </div><!-- end comments-wrapper -->
</div><!-- end review-wrap -->