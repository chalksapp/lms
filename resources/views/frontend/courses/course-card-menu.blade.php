<div class="msg-action-dot my-course-action-dot">
    <div class="dropdown">
        <a class="action-dot btn" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="la la-ellipsis-v"></i>
        </a>
        <div class="dropdown-menu">
            <ul>
                <li><p class="dropdown-header pt-0 pb-0 primary-color">Collections</p></li>
                <li>
                    <p class="dropdown-header pt-0 pb-3">You have no collections</p>
                </li>
                <li>
                    <div class="section-block mb-2"></div>
                </li>
                <li>
                    <a class="dropdown-item d-flex align-items-center" href="javascript:void(0)" data-toggle="modal" data-target=".share-modal-form">
                        Share <i class="ml-auto la la-share"></i>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item d-flex align-items-center" href="javascript:void(0)" data-toggle="modal" data-target=".create-collection-modal-form">
                        Create New Collection <i class="ml-auto la la-plus"></i>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item d-flex align-items-center" href="javascript:void(0)">
                        <span class="swapping-btn w-100" data-text-swap="Unfavorite" data-text-original="Favorite">Favorite</span>
                        <i class="ml-auto la la-star"></i>
                    </a>
                </li>
                <li>
                    <a class="dropdown-item d-flex align-items-center" href="javascript:void(0)">
                        <span class="swapping-btn w-100" data-text-swap="Archived" data-text-original="Archive">Archive</span>
                        <i class="ml-auto la la-archive"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>