<div class="my-course-container" id="all-course-content">
    <div class="row">
        @foreach($courses as $course)
            <div class="col-lg-4 column-td-half">
                <div class="card-item">
                    <div class="card-image">
                        @include('frontend.courses.play-button')
                        @include('frontend.courses.course-card-menu')
                    </div><!-- end card-image -->
                    <div class="card-content p-4">
                        <p class="card__label">
                            <span class="card__label-text">{{ $course->course_level }}</span>
                            <a href="#" class="card__collection-icon" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to Wishlist"><span class="la la-heart-o"></span></a>
                        </p>
                        <h3 class="card__title mt-0">
                            <a href="{{ route('lms_user.course_details', base64_encode($course->id)) }}">{{ $course->course_name }}</a>
                        </h3>
                        <p class="card__author">
                            <span> {{ substr($course->course_desc, 0, 40) }}</span>
                        </p>
                        <div class="course-complete-bar-2 mt-2">
                            <div class="progress-item mb-0">
                                <p class="skillbar-title">Complete:</p>
                                <div class="skillbar-box mt-1">
                                    <div class="skillbar" data-percent="0%">
                                        <div class="skillbar-bar skillbar-bar-1"></div>
                                    </div> <!-- End Skill Bar -->
                                </div>
                                <div class="skill-bar-percent">0%</div>
                            </div>
                        </div><!-- end course-complete-bar-2 -->
                        <div class="rating-wrap d-flex mt-3">
                            <a href="{{ route('lms_user.course_details', base64_encode($course->id)) }}" class="btn rating-btn">
                                <i class="la la-eye mr-1"></i>Start course
                            </a>
                        </div><!-- end rating-wrap -->
                    </div><!-- end card-content -->
                </div><!-- end card-item -->
            </div>
        @endforeach
        @if(count($courses) < 1)
            <div class="col-lg-4 column-td-half">
                <h2>No results found.</h2>
            </div>
        @endif
    </div>
</div>
{{ $courses->links() }}