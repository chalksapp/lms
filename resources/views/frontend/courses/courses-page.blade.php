@extends('frontend.layouts.master')
@push('style')
<style>
    .my-course-search-content {
        width: 100% !important;
        padding-left: 0px !important;
    }
</style>
@endpush
@section('content')
    @include('frontend.courses.breadcrumb')
    <!-- ================================
       START MY COURSES
    ================================= -->
    <section class="my-courses-area padding-top-30px padding-bottom-90px">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="my-course-content-wrap">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active show" id="all-courses">
                                <div class="my-course-content-body">
                                    @include('frontend.courses.filter')
                                    <div id="all-course-content"></div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="enrolled-courses">
                                <div class="my-course-content-body">
                                    @include('frontend.courses.filter')
                                    <div id="enrolled-course-content"></div>
                                </div>
                            </div><!-- end tab-pane -->
                            <div role="tabpanel" class="tab-pane fade" id="wishlist">
                                <div class="my-wishlist-wrap">
                                    <div class="my-wishlist-info d-flex align-items-center justify-content-between">
                                        <h3 class="widget-title">My wishlists</h3>
                                        <div class="lecture-overview-item m-0">
                                            <div class="my-course-search-content">
                                                <div class="question-overview-filter-item">
                                                    <div class="contact-form-action">
                                                        <form method="post">
                                                            <div class="input-box">
                                                                <div class="form-group mb-0">
                                                                    <input class="form-control" type="text" name="search" placeholder="Search courses">
                                                                    <span class="la la-search search-icon"></span>
                                                                </div>
                                                            </div><!-- end input-box -->
                                                        </form>
                                                    </div><!-- end contact-form-action -->
                                                </div><!-- end question-overview-filter-item -->
                                            </div><!-- end my-course-search-content -->
                                        </div>
                                    </div>
                                    <div class="my-wishlist-card-body padding-top-35px">
                                        <div class="row">
                                            <div class="col-lg-4 column-td-half">
                                                <div class="card-item">
                                                    <div class="card-image">
                                                        <a href="course-details.html" class="card__img"><img src="{{ asset('frontend_assets/images/img9.jpg') }}" alt=""></a>
                                                    </div><!-- end card-image -->
                                                    <div class="card-content">
                                                        <p class="card__label">
                                                            <span class="card__label-text">all levels</span>
                                                            <a href="#" class="card__collection-icon primary-color-2" data-toggle="tooltip" data-placement="top" title="Remove form wishlist"><span class="la la-heart"></span></a>
                                                        </p>
                                                        <h3 class="card__title">
                                                            <a href="course-details.html">User Experience Design - Adobe XD UI UX Design</a>
                                                        </h3>
                                                        <p class="card__author">
                                                            <a href="teacher-detail.html">kamran paul</a>
                                                        </p>
                                                        <div class="rating-wrap d-flex mt-2 mb-3">
                                                            <ul class="review-stars">
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star-o"></span></li>
                                                            </ul>
                                                            <span class="star-rating-wrap">
                                                            <span class="star__rating">4.4</span>
                                                            <span class="star__count">(20)</span>
                                                        </span>
                                                        </div><!-- end rating-wrap -->
                                                        <div class="card-action">
                                                            <ul class="card-duration d-flex justify-content-between align-items-center">
                                                                <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-play-circle"></i> 45 Classes
                                                                </span>
                                                                </li>
                                                                <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-clock-o"></i> 3 hours 20 min
                                                                </span>
                                                                </li>
                                                            </ul>
                                                        </div><!-- end card-action -->
                                                        <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                                            <span class="card__price">Free</span>
                                                            <a href="#" class="text-btn">Get Enrolled</a>
                                                        </div><!-- end card-price-wrap -->
                                                    </div><!-- end card-content -->
                                                </div><!-- end card-item -->
                                            </div><!-- end col-lg-4 -->
                                            <div class="col-lg-4 column-td-half">
                                                <div class="card-item">
                                                    <div class="card-image">
                                                        <a href="course-details.html" class="card__img"><img src="{{ asset('frontend_assets/images/img12.jpg') }}" alt=""></a>
                                                        <div class="card-badge">
                                                            <span class="badge-label">bestseller</span>
                                                        </div>
                                                    </div><!-- end card-image -->
                                                    <div class="card-content">
                                                        <p class="card__label">
                                                            <span class="card__label-text">all levels</span>
                                                            <a href="#" class="card__collection-icon primary-color-2" data-toggle="tooltip" data-placement="top" title="Remove form wishlist"><span class="la la-heart"></span></a>
                                                        </p>
                                                        <h3 class="card__title">
                                                            <a href="course-details.html">The Complete Digital finance Marketing Course</a>
                                                        </h3>
                                                        <p class="card__author">
                                                            <a href="teacher-detail.html">jose purtila</a>
                                                        </p>
                                                        <div class="rating-wrap d-flex mt-2 mb-3">
                                                            <ul class="review-stars">
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star-o"></span></li>
                                                            </ul>
                                                            <span class="star-rating-wrap">
                                                            <span class="star__rating">4.2</span>
                                                          <span class="star__count">(30)</span>
                                                        </span>
                                                        </div><!-- end rating-wrap -->
                                                        <div class="card-action">
                                                            <ul class="card-duration d-flex justify-content-between align-items-center">
                                                                <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-play-circle"></i> 45 Classes
                                                                </span>
                                                                </li>
                                                                <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-clock-o"></i> 3 hours 20 min
                                                                </span>
                                                                </li>
                                                            </ul>
                                                        </div><!-- end card-action -->
                                                        <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                                            <span class="card__price"><span class="before-price">$189.00</span> $119.00</span>
                                                            <a href="#" class="text-btn">Add to cart</a>
                                                        </div><!-- end card-price-wrap -->
                                                    </div><!-- end card-content -->
                                                </div><!-- end card-item -->
                                            </div><!-- end col-lg-4 -->
                                            <div class="col-lg-4 column-td-half">
                                                <div class="card-item">
                                                    <div class="card-image">
                                                        <a href="course-details.html" class="card__img"><img src="images/img13.jpg" alt=""></a>
                                                    </div><!-- end card-image -->
                                                    <div class="card-content">
                                                        <p class="card__label">
                                                            <span class="card__label-text">all levels</span>
                                                            <a href="#" class="card__collection-icon primary-color-2" data-toggle="tooltip" data-placement="top" title="Remove form wishlist"><span class="la la-heart"></span></a>
                                                        </p>
                                                        <h3 class="card__title">
                                                            <a href="course-details.html">Complete Python Bootcamp: Go from zero to hero</a>
                                                        </h3>
                                                        <p class="card__author">
                                                            <a href="teacher-detail.html">noelle travesy</a>
                                                        </p>
                                                        <div class="rating-wrap d-flex mt-2 mb-3">
                                                            <ul class="review-stars">
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star"></span></li>
                                                                <li><span class="la la-star-o"></span></li>
                                                            </ul>
                                                            <span class="star-rating-wrap">
                                                            <span class="star__rating">4.5</span>
                                                            <span class="star__count">(40)</span>
                                                        </span>
                                                        </div><!-- end rating-wrap -->
                                                        <div class="card-action">
                                                            <ul class="card-duration d-flex justify-content-between align-items-center">
                                                                <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-play-circle"></i> 45 Classes
                                                                </span>
                                                                </li>
                                                                <li>
                                                                <span class="meta__date">
                                                                    <i class="la la-clock-o"></i> 3 hours 20 min
                                                                </span>
                                                                </li>
                                                            </ul>
                                                        </div><!-- end card-action -->
                                                        <div class="card-price-wrap d-flex justify-content-between align-items-center">
                                                            <span class="card__price">$68.00</span>
                                                            <a href="#" class="text-btn">Add to cart</a>
                                                        </div><!-- end card-price-wrap -->
                                                    </div><!-- end card-content -->
                                                </div><!-- end card-item -->
                                            </div><!-- end col-lg-4 -->
                                        </div><!-- end row -->
                                    </div>
                                </div><!-- end my-wishlist-wrap -->
                            </div><!-- end tab-pane -->
                            <div role="tabpanel" class="tab-pane fade" id="archived">
                                <div class="my-archived-wrap">
                                    <div class="my-wishlist-info">
                                        <h3 class="widget-title">My archives</h3>
                                    </div>
                                    <div class="my-archived-card-body padding-top-35px">
                                        <div class="row">
                                            <div class="col-lg-4 column-td-half">
                                                <div class="card-item">
                                                    <div class="card-image">
                                                        <a href="lesson-details.html" class="card__img">
                                                            <img src="images/img8.jpg" alt="">
                                                            <div class="play-button">
                                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="-307.4 338.8 91.8 91.8" style=" enable-background:new -307.4 338.8 91.8 91.8;" xml:space="preserve">
                                                                    <style type="text/css">
                                                                        .st0{opacity:0.6;fill:#000000;border-radius: 100px;enable-background:new;}
                                                                        .st1{fill:#FFFFFF;}
                                                                    </style>
                                                                    <g>
                                                                        <circle class="st0" cx="-261.5" cy="384.7" r="45.9"/><path class="st1" d="M-272.9,363.2l35.8,20.7c0.7,0.4,0.7,1.3,0,1.7l-35.8,20.7c-0.7,0.4-1.5-0.1-1.5-0.9V364C-274.4,363.3-273.5,362.8-272.9,363.2z"/>
                                                                    </g>
                                                            </svg>
                                                            </div>
                                                        </a>
                                                        <div class="msg-action-dot my-course-action-dot">
                                                            <div class="dropdown">
                                                                <a class="action-dot btn" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="la la-ellipsis-v"></i>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)">
                                                                        Unarchive <i class="la la-archive"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- end card-image -->
                                                    <div class="card-content p-4">
                                                        <h3 class="card__title mt-0">
                                                            <a href="lesson-details.html">The Complete Full-Stack JavaScript Course!</a>
                                                        </h3>
                                                        <p class="card__author">
                                                            <a href="teacher-detail.html">Joseph Delgadillo</a>
                                                            <span>, Internet Marketer & Business Innovator</span>
                                                        </p>
                                                        <div class="course-complete-bar-2 mt-2">
                                                            <div class="progress-item mb-0">
                                                                <p class="skillbar-title">Complete:</p>
                                                                <div class="skillbar-box mt-1">
                                                                    <div class="skillbar" data-percent="70%">
                                                                        <div class="skillbar-bar skillbar-bar-1"></div>
                                                                    </div> <!-- End Skill Bar -->
                                                                </div>
                                                                <div class="skill-bar-percent">70%</div>
                                                            </div>
                                                        </div><!-- end course-complete-bar-2 -->
                                                        <div class="rating-wrap d-flex mt-3">
                                                            <a href="javascript:void(0)" data-toggle="modal" data-target=".rating-modal-form" class="btn rating-btn">
                                                                <i class="la la-star mr-1"></i>Leave a Rating
                                                            </a>
                                                        </div><!-- end rating-wrap -->
                                                    </div><!-- end card-content -->
                                                </div><!-- end card-item -->
                                            </div><!-- end col-lg-4 -->
                                            <div class="col-lg-4 column-td-half">
                                                <div class="card-item">
                                                    <div class="card-image">
                                                        <a href="lesson-details.html" class="card__img">
                                                            <img src="images/img9.jpg" alt="">
                                                            <div class="play-button">
                                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="-307.4 338.8 91.8 91.8" style=" enable-background:new -307.4 338.8 91.8 91.8;" xml:space="preserve">
                                                                    <style type="text/css">
                                                                        .st0{opacity:0.6;fill:#000000;border-radius: 100px;enable-background:new;}
                                                                        .st1{fill:#FFFFFF;}
                                                                    </style>
                                                                    <g>
                                                                        <circle class="st0" cx="-261.5" cy="384.7" r="45.9"/><path class="st1" d="M-272.9,363.2l35.8,20.7c0.7,0.4,0.7,1.3,0,1.7l-35.8,20.7c-0.7,0.4-1.5-0.1-1.5-0.9V364C-274.4,363.3-273.5,362.8-272.9,363.2z"/>
                                                                    </g>
                                                            </svg>
                                                            </div>
                                                        </a>
                                                        <div class="msg-action-dot my-course-action-dot">
                                                            <div class="dropdown">
                                                                <a class="action-dot btn" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="la la-ellipsis-v"></i>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)">
                                                                        Unarchive <i class="la la-archive"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- end card-image -->
                                                    <div class="card-content p-4">
                                                        <h3 class="card__title mt-0">
                                                            <a href="lesson-details.html">Microsoft SQL Server 2019 for Everyone</a>
                                                        </h3>
                                                        <p class="card__author">
                                                            <a href="teacher-detail.html">Hussain Rubaye</a>
                                                            <span>, Software Engineer and Developer</span>
                                                        </p>
                                                        <div class="course-complete-bar-2 mt-2">
                                                            <div class="progress-item mb-0">
                                                                <p class="skillbar-title">Complete:</p>
                                                                <div class="skillbar-box mt-1">
                                                                    <div class="skillbar" data-percent="0%">
                                                                        <div class="skillbar-bar skillbar-bar-1"></div>
                                                                    </div> <!-- End Skill Bar -->
                                                                </div>
                                                                <div class="skill-bar-percent">0%</div>
                                                            </div>
                                                        </div><!-- end course-complete-bar-2 -->
                                                        <div class="rating-wrap d-flex mt-3">
                                                            <a href="lesson-details.html" class="btn rating-btn">
                                                                <i class="la la-eye mr-1"></i>Start course
                                                            </a>
                                                        </div><!-- end rating-wrap -->
                                                    </div><!-- end card-content -->
                                                </div><!-- end card-item -->
                                            </div><!-- end col-lg-4 -->
                                            <div class="col-lg-4 column-td-half">
                                                <div class="card-item">
                                                    <div class="card-image">
                                                        <a href="lesson-details.html" class="card__img">
                                                            <img src="images/img10.jpg" alt="">
                                                            <div class="play-button">
                                                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="-307.4 338.8 91.8 91.8" style=" enable-background:new -307.4 338.8 91.8 91.8;" xml:space="preserve">
                                                                    <style type="text/css">
                                                                        .st0{opacity:0.6;fill:#000000;border-radius: 100px;enable-background:new;}
                                                                        .st1{fill:#FFFFFF;}
                                                                    </style>
                                                                    <g>
                                                                        <circle class="st0" cx="-261.5" cy="384.7" r="45.9"/><path class="st1" d="M-272.9,363.2l35.8,20.7c0.7,0.4,0.7,1.3,0,1.7l-35.8,20.7c-0.7,0.4-1.5-0.1-1.5-0.9V364C-274.4,363.3-273.5,362.8-272.9,363.2z"/>
                                                                    </g>
                                                            </svg>
                                                            </div>
                                                        </a>
                                                        <div class="msg-action-dot my-course-action-dot">
                                                            <div class="dropdown">
                                                                <a class="action-dot btn" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <i class="la la-ellipsis-v"></i>
                                                                </a>
                                                                <div class="dropdown-menu">
                                                                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)">
                                                                        Unarchive <i class="la la-archive"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div><!-- end card-image -->
                                                    <div class="card-content p-4">
                                                        <h3 class="card__title mt-0">
                                                            <a href="lesson-details.html">WordPress for Beginners – Master WordPress</a>
                                                        </h3>
                                                        <p class="card__author">
                                                            <a href="teacher-detail.html">alex smith</a>
                                                            <span>, Developer, Team Lead, Software Consultant, Loves Technology</span>
                                                        </p>
                                                        <div class="course-complete-bar-2 mt-2">
                                                            <div class="progress-item mb-0">
                                                                <p class="skillbar-title">Complete:</p>
                                                                <div class="skillbar-box mt-1">
                                                                    <div class="skillbar" data-percent="0%">
                                                                        <div class="skillbar-bar skillbar-bar-1"></div>
                                                                    </div> <!-- End Skill Bar -->
                                                                </div>
                                                                <div class="skill-bar-percent">0%</div>
                                                            </div>
                                                        </div><!-- end course-complete-bar-2 -->
                                                        <div class="rating-wrap d-flex mt-3">
                                                            <a href="lesson-details.html" class="btn rating-btn">
                                                                <i class="la la-eye mr-1"></i>Start course
                                                            </a>
                                                        </div><!-- end rating-wrap -->
                                                    </div><!-- end card-content -->
                                                </div><!-- end card-item -->
                                            </div><!-- end col-lg-4 -->
                                        </div><!-- end row -->
                                    </div>
                                </div><!-- end my-archived-wrap -->
                            </div><!-- end tab-pane -->
                        </div>
                    </div>
                </div><!-- end col-lg-12 -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end my-courses-area -->
    <!-- ================================
           START MY COURSES
    ================================= -->
@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function () {
        $.ajax({
            url: '{{ route('lms_user.get_all_courses') }}',
            type: 'GET',
            success: function (data) {
                $('#all-course-content').html(data);
            },
            error: function (error) {
                alert(error)
            }
        });
    });
    $(function() {
        $('body').on('click', '.pagination a', function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            var search_term = $.trim($('#search_course').val())
            getData(url + '&search_term=' + search_term);
            // window.history.pushState("", "", url);
        });

        $('#search_course_btn').on('click', function(e) {
            e.preventDefault();
            getAllCourses()
        });

        function getAllCourses() {
            var search_term = $.trim($('#search_course').val())
            var url = '{{ route('lms_user.get_all_courses') }}' + '?search_term=' + search_term
            getData(url);
        }

        function getData(url) {
            $.ajax({
                url : url
            }).done(function (data) {
                $('#all-course-content').html(data);
            }).fail(function () {
                alert('Articles could not be loaded.');
            });
        }
    });
</script>
@endpush