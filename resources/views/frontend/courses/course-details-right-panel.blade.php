<div class="col-lg-4">
    <div class="sidebar-component">
        <div class="sidebar">
            <div class="sidebar-widget sidebar-preview">
                <div class="sidebar-preview-titles">
                    <h3 class="widget-title">Preview this course</h3>
                    <span class="section-divider"></span>
                </div>
                <div class="preview-video-and-details">
                    <div class="preview-course-video">
                        <a href="javascript:void(0)" data-toggle="modal" data-target=".preview-modal-form">
                            @if($courseData->course_image != '')
                            <img src="{{ asset("course_files/$courseData->id/$courseData->course_image") }}" alt="course-img">
                            @else
                            <img src="{{ asset('frontend_assets/images/img9.jpg') }}" alt="course-img">
                            @endif

                            <div class="play-button">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="-307.4 338.8 91.8 91.8" style=" enable-background:new -307.4 338.8 91.8 91.8;" xml:space="preserve">
                                                   <style type="text/css">
                                                       .st0{opacity:0.6;fill:#000000;border-radius: 100px;enable-background:new;}
                                                       .st1{fill:#FFFFFF;}
                                                   </style>
                                    <g>
                                        <circle class="st0" cx="-261.5" cy="384.7" r="45.9"/><path class="st1" d="M-272.9,363.2l35.8,20.7c0.7,0.4,0.7,1.3,0,1.7l-35.8,20.7c-0.7,0.4-1.5-0.1-1.5-0.9V364C-274.4,363.3-273.5,362.8-272.9,363.2z"/>
                                    </g>
                                            </svg>
                            </div>
                        </a>
                    </div>
                    <div class="preview-course-content">
                        <p class="preview-course__price d-flex align-items-center">
                            <span class="price-current">{{ config('frontend.CURRENCY') . $courseData->course_fees }}</span>
                            {{--<span class="price-before">$104.99</span>
                            <span class="price-discount">24% off</span>--}}
                        </p>
                        <p class="preview-price-discount__text">
                            <span class="discount-left__text-text">4 days</span> left at this price!
                        </p>
                        <div class="buy-course-btn mb-3 text-center">
                            <a href="#" class="theme-btn w-100 mb-3">go to course</a>
                        </div>
                        <div class="preview-course-incentives">
                            <p class="preview-course-incentives__text mb-4">
                                <i class="la la-thumbs-up"></i> 30-Day Money-Back Guarantee
                            </p>
                            <h3 class="widget-title font-size-18">This course includes</h3>
                            <ul class="list-items pb-3">
                                <li><i class="la la-play-circle-o"></i>{{ $courseData->course_duration }} hours duration</li>
                                <li><i class="la la-file"></i>{{ count($courseData->curriculum) }} articles</li>
                                <li><i class="la la-file-text"></i>12 downloadable resources</li>
                                <li><i class="la la-key"></i>Full lifetime access</li>
                                <li><i class="la la-television"></i>Access on mobile and TV</li>
                                <li><i class="la la-certificate"></i>Certificate of Completion</li>
                            </ul>
                            <div class="section-block"></div>
                            <div class="video-content-btn d-flex align-items-center justify-content-between pb-3 pt-3">
                                <button class="btn">
                                    <i class="la la-heart-o mr-1 bookmark-icon"></i>
                                    <span class="swapping-btn" data-text-swap="Wishlisted" data-text-original="Wishlist">Wishlist</span>
                                </button>
                                <button class="btn" data-toggle="modal" data-target=".share-modal-form">
                                    <i class="la la-share mr-1"></i>
                                    <span>Share</span>
                                </button>
                            </div>
                        </div><!-- end preview-course-incentives -->
                    </div><!-- end preview-course-content -->
                </div><!-- end preview-video-and-details -->
            </div><!-- end sidebar-widget -->
            <div class="sidebar-widget sidebar-feature">
                <h3 class="widget-title">Course Features</h3>
                <span class="section-divider"></span>
                <ul class="list-items">
                    <li>
                        <span><i class="la la-clock-o"></i>Duration</span>
                        <span>{{ $courseData->course_duration }} hours</span>
                    </li>
                    {{--<li>
                        <span><i class="la la-play-circle-o"></i>Lectures</span>
                        <span>17</span>
                    </li>
                    <li>
                        <span><i class="la la-file-text"></i>Resources</span>
                        <span>12</span>
                    </li>--}}
                    <li>
                        <span><i class="la la-puzzle-piece"></i>Quizzes</span>
                        <span>{{ array_count_values(array_column($courseData->curriculum, 'type'))['test'] ?? 0 }}</span>
                    </li>
                    <li>
                        <span><i class="la la-eye"></i>Preview Lessons</span>
                        <span>{{ array_count_values(array_column($courseData->curriculum, 'type'))['video'] ?? 0 }}</span>
                    </li>
                    <li>
                        <span> <i class="la la-language"></i>Language</span>
                        <span>English</span>
                    </li>
                    <li>
                        <span><i class="la la-level-up"></i>Skill level</span>
                        <span>{{ $courseData->course_level }}</span>
                    </li>
                    <li>
                        <span>  <i class="la la-users"></i>Students</span>
                        <span>{{ count($courseData->users) }}</span>
                    </li>
                    <li>
                        <span><i class="la la-certificate"></i>Certificate</span>
                        <span>Yes</span>
                    </li>
                </ul>
            </div><!-- end sidebar-widget -->
        </div><!-- end sidebar -->
    </div><!-- end sidebar-component -->
</div><!-- end col-lg-4 -->