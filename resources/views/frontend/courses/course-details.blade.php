@extends('frontend.layouts.master')
@section('content')
    @include('frontend.courses.course-details-breadcrumb')
    <!--======================================
        START COURSE DETAIL
======================================-->
    <section class="course-detail margin-bottom-110px">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="course-detail-content-wrap margin-top-100px">
                        <div class="description-wrap margin-bottom-40px">
                            <h3 class="widget-title">Description</h3>
                            <p class="mb-2 mt-3">{{ $courseData->description }}</p>
                        </div><!-- end description-wrap -->
                        {{--<div class="requirement-wrap margin-bottom-30px">
                            <h3 class="widget-title">Who this course is for:</h3>
                            <ul class="list-items mt-3 mb-3">
                                <li>Anyone who wants to become a digital marketer, promote a business</li>
                                <li>Anyone who is tired of their day job or wants a career change</li>
                                <li>People who want to earn an additional income from freelancing</li>
                                <li>Digital marketers who want some more tricks</li>
                            </ul>
                        </div>--}}
                        @if(count($courseData->curriculum))
                        @include('frontend.courses.course-curriculum')
                        @endif
                        <div class="section-block"></div>
                        @include('frontend.courses.about-instructor')
                        @include('frontend.courses.reviews')
                    </div><!-- end course-detail-content-wrap -->
                </div><!-- end col-lg-8 -->
                @include('frontend.courses.course-details-right-panel')
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end course-detail -->
    <!--======================================
            END COURSE DETAIL
    ======================================-->
@endsection