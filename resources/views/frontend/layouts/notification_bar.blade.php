<div class="notification-wrap d-flex align-items-center">
    <div class="notification-item mr-3">
        <div class="dropdown">
            <button class="notification-btn dropdown-toggle" type="button" id="notificationDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="la la-bell"></i>
                <span class="quantity">5</span>
            </button>
            <div class="dropdown-menu" aria-labelledby="notificationDropdownMenu">
                <div class="mess-dropdown">
                    <div class="mess__title">
                        <h4 class="widget-title">Notifications</h4>
                    </div><!-- end mess__title -->
                    <div class="mess__body">
                        <a href="dashboard.html" class="d-block">
                            <div class="mess__item">
                                <div class="icon-element bg-color-1 text-white">
                                    <i class="la la-bolt"></i>
                                </div>
                                <div class="content">
                                    <span class="time">1 hour ago</span>
                                    <p class="text">Your Resume Updated!</p>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                        <a href="dashboard.html" class="d-block">
                            <div class="mess__item">
                                <div class="icon-element bg-color-2 text-white">
                                    <i class="la la-lock"></i>
                                </div>
                                <div class="content">
                                    <span class="time">November 12, 2019</span>
                                    <p class="text">You changed password</p>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                        <a href="dashboard.html" class="d-block">
                            <div class="mess__item">
                                <div class="icon-element bg-color-3 text-white">
                                    <i class="la la-check-circle"></i>
                                </div>
                                <div class="content">
                                    <span class="time">October 6, 2019</span>
                                    <p class="text">You applied for a job <span class="color-text">Front-end Developer</span></p>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                        <a href="dashboard.html" class="d-block">
                            <div class="mess__item">
                                <div class="icon-element bg-color-4 text-white">
                                    <i class="la la-user"></i>
                                </div>
                                <div class="content">
                                    <span class="time">Jun 12, 2019</span>
                                    <p class="text">Your account has been created successfully</p>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                        <a href="dashboard.html" class="d-block">
                            <div class="mess__item">
                                <div class="icon-element bg-color-5 text-white">
                                    <i class="la la-download"></i>
                                </div>
                                <div class="content">
                                    <span class="time">May 12, 2019</span>
                                    <p class="text">Someone downloaded resume</p>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                    </div><!-- end mess__body -->
                    <div class="btn-box p-2 text-center">
                        <a href="dashboard.html">Show All Notifications</a>
                    </div><!-- end btn-box -->
                </div><!-- end mess-dropdown -->
            </div><!-- end dropdown-menu -->
        </div><!-- end dropdown -->
    </div>
    <div class="notification-item mr-3">
        <div class="dropdown">
            <button class="notification-btn dropdown-toggle" type="button" id="messageDropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="la la-envelope"></i>
                <span class="quantity">5</span>
            </button>
            <div class="dropdown-menu" aria-labelledby="messageDropdownMenu">
                <div class="mess-dropdown">
                    <div class="mess__title">
                        <h4 class="widget-title">Messages</h4>
                    </div><!-- end mess__title -->
                    <div class="mess__body">
                        <a href="dashboard-message.html" class="d-block">
                            <div class="mess__item">
                                <div class="avatar dot-status">
                                    <img src="{{ asset('frontend_assets/images/team7.jpg') }}" alt="Team img">
                                </div>
                                <div class="content">
                                    <h4 class="widget-title">Michelle Moreno</h4>
                                    <p class="text">Thanks for reaching out. I'm quite busy right now on many</p>
                                    <span class="time">5 min ago</span>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                        <a href="dashboard-message.html" class="d-block">
                            <div class="mess__item">
                                <div class="avatar dot-status online-status">
                                    <img src="{{ asset('frontend_assets/images/team8.jpg') }}" alt="Team img">
                                </div>
                                <div class="content">
                                    <h4 class="widget-title">Alex Smith</h4>
                                    <p class="text">Thanks for reaching out. I'm quite busy right now on many</p>
                                    <span class="time">2 days ago</span>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                        <a href="dashboard-message.html" class="d-block">
                            <div class="mess__item">
                                <div class="avatar dot-status">
                                    <img src="{{ asset('frontend_assets/images/team9.jpg') }}" alt="Team img">
                                </div>
                                <div class="content">
                                    <h4 class="widget-title">Michelle Moreno</h4>
                                    <p class="text">Thanks for reaching out. I'm quite busy right now on many</p>
                                    <span class="time">5 min ago</span>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                        <a href="dashboard-message.html" class="d-block">
                            <div class="mess__item">
                                <div class="avatar dot-status online-status">
                                    <img src="{{ asset('frontend_assets/images/team7.jpg') }}" alt="Team img">
                                </div>
                                <div class="content">
                                    <h4 class="widget-title">Alex Smith</h4>
                                    <p class="text">Thanks for reaching out. I'm quite busy right now on many</p>
                                    <span class="time">2 days ago</span>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                        <a href="dashboard-message.html" class="d-block">
                            <div class="mess__item">
                                <div class="avatar dot-status">
                                    <img src="{{ asset('frontend_assets/images/team8.jpg') }}" alt="Team img">
                                </div>
                                <div class="content">
                                    <h4 class="widget-title">Alex Smith</h4>
                                    <p class="text">Thanks for reaching out. I'm quite busy right now on many</p>
                                    <span class="time">2 days ago</span>
                                </div>
                            </div><!-- end mess__item -->
                        </a>
                    </div><!-- end mess__body -->
                    <div class="btn-box p-2 text-center">
                        <a href="dashboard-message.html">Show All Message</a>
                    </div><!-- end btn-box -->
                </div><!-- end mess-dropdown -->
            </div><!-- end dropdown-menu -->
        </div><!-- end dropdown -->
    </div>
</div>