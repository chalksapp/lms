<!-- inject:css -->
<link rel="stylesheet" href="{{ asset('frontend_assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/line-awesome.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/owl.carousel.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/owl.theme.default.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/fancybox.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/jquery.filer.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/tooltipster.bundle.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/jqvmap.css') }}">
<link rel="stylesheet" href="{{ asset('frontend_assets/css/style.css') }}">
<!-- end inject -->