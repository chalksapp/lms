<div class="page-navigation-wrap mt-4 text-center">
    <div class="page-navigation-inner d-inline-block">
        <div class="page-navigation">
            <a href="{{ $courses->previousPageUrl() }}" class="page-go page-prev paginate_btn">
                <i class="la la-arrow-left"></i>
            </a>
            <ul class="page-navigation-nav">
                {{--                <li><a href="#" class="page-go-link">1</a></li>--}}
                <li class="active"><a href="{{ $courses->currentPage() }}" class="page-go-link">{{ $courses->currentPage() }}</a></li>
                {{--                <li><a href="#" class="page-go-link">3</a></li>--}}
                {{--                <li><a href="#" class="page-go-link">4</a></li>--}}

            </ul>
            <a href="{{ $courses->nextPageUrl() }}" class="page-go page-next paginate_btn">
                <i class="la la-arrow-right"></i>
            </a>
        </div>
    </div>
    <p class="font-size-14 mt-3">Showing {{ $courses->count() }} of {{ $courses->total() }} courses</p>
</div>