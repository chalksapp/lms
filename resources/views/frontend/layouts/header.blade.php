<!--======================================
        START HEADER AREA
    ======================================-->
<header class="header-menu-area">
    <div class="header-menu-content dashboard-menu-content my-course-menu-content">
        <div class="container-fluid">
            <div class="main-menu-content">
                <div class="row align-items-center">
                    <div class="col-lg-2">
                        <div class="logo-box">
                            <a href="{{ route('lms_user.dashboard') }}" class="logo"><img src="{{ asset('frontend_assets/images/logo.png') }}" alt="logo"></a>
                            <div class="side-menu-open">
                                <i class="la la-user"></i>
                            </div>
                        </div>
                    </div><!-- end col-lg-2 -->
                    <div class="col-lg-10">
                        <div class="menu-wrapper">
                            <div class="contact-form-action">
                                <form method="post">
                                    <div class="input-box">
                                        <div class="form-group">
                                            <input class="form-control" type="text" name="search" placeholder="Search for anything">
                                            <span class="la la-search search-icon"></span>
                                        </div>
                                    </div><!-- end input-box -->
                                </form>
                            </div><!-- end contact-form-action -->
                            <div class="logo-right-button d-flex align-items-center">
                                <div class="shop-cart course-cart h-auto">
                                    <ul>
                                        <li>
                                            <a href="{{ route('lms_user.dashboard') }}" class="shop-cart-btn font-size-15 text-uppercase">Dashboard</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="shop-cart course-cart h-auto">
                                    <ul>
                                        <li>
                                            <a href="{{ route('lms_user.common-tests') }}" class="shop-cart-btn font-size-15 text-uppercase">Tests</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="shop-cart course-cart h-auto">
                                    <ul>
                                        <li>
                                            <a href="{{ route('lms_user.courses') }}" class="shop-cart-btn font-size-15 text-uppercase">Courses</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="header-action-button d-flex align-items-center">
                                    @include('frontend.layouts.notification_bar')
                                    @include('frontend.layouts.user_action')
                                </div>
                            </div><!-- end logo-right-button -->
                        </div><!-- end menu-wrapper -->
                    </div><!-- end col-lg-10 -->
                </div><!-- end row -->
            </div>
        </div><!-- end container-fluid -->
    </div><!-- end header-menu-content -->
</header><!-- end header-menu-area -->
<!--======================================
        END HEADER AREA
======================================-->