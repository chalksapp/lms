@extends('layouts/contentLayoutMaster')

@section('title', 'Add MCQ Page')

@section('vendor-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-user.css')) }}">
        
@endsection

@section('content')
<!-- users edit start -->
<section class="users-edit">
  <div class="card">
    <div class="card-content">
      <div class="card-body">
        <ul class="nav nav-tabs mb-3" role="tablist">
          <li class="nav-item">
            <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account"
              aria-controls="account" role="tab" aria-selected="true">
              <i class="feather icon-file-text mr-25"></i><span class="d-none d-sm-block">Multiple Choice Question</span>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link d-flex align-items-center" id="information-tab" data-toggle="tab" href="#information"
              aria-controls="information" role="tab" aria-selected="false">
              <i class="feather icon-info mr-25"></i><span class="d-none d-sm-block">Information</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link d-flex align-items-center" id="social-tab" data-toggle="tab" href="#social"
              aria-controls="social" role="tab" aria-selected="false">
              <i class="feather icon-share-2 mr-25"></i><span class="d-none d-sm-block">Social</span>
            </a>
          </li> -->
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
            <!-- users edit media object start -->
            <!--<div class="media mb-2">
              <a class="mr-2 my-25" href="#">
                <img src="{{ asset('images/portrait/small/avatar-s-12.jpg') }}" alt="users avatar"
                  class="users-avatar-shadow rounded" height="64" width="64">
              </a>
              <div class="media-body mt-50">
                <h4 class="media-heading">Angelo Sashington</h4>
                <div class="col-12 d-flex mt-1 px-0">
                  <a href="#" class="btn btn-primary d-none d-sm-block mr-75">Change</a>
                  <a href="#" class="btn btn-primary d-block d-sm-none mr-75"><i
                      class="feather icon-edit-1"></i></a>
                  <a href="#" class="btn btn-outline-danger d-none d-sm-block">Remove</a>
                  <a href="#" class="btn btn-outline-danger d-block d-sm-none"><i class="feather icon-trash-2"></i></a>
                </div>
              </div>
            </div> -->
            <!-- users edit media object ends -->
            <!-- users edit account form start -->
            <form novalidate method="POST" action="{{ route('create-mcq')}}">
              @csrf
              <div class="row">
                <div class="col-12 col-sm-12">
                  <!-- <div class="form-group">
                    <div class="controls">
                      <label>Username</label>
                      <input type="text" class="form-control" placeholder="Username" value="adoptionism744" required
                        data-validation-required-message="This username field is required">
                    </div>
                  </div> -->
                  <input type="hidden" name="hid_mcq_id" id="hid_mcq_id" value="{{$get_mcq?$get_mcq['id']:''}}">
                  <div class="form-group">
                    <div class="controls">
                      <label>Question Title</label>
                      <input type="text" class="form-control" id="mcq_title" name="mcq_title" placeholder="Enter a Title for easy filter" required
                        data-validation-required-message="This title field is required" value="{{$get_mcq?$get_mcq['mcq_ques_title']:''}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Question</label>
                      <textarea class="form-control" id="mcq_ques" name="mcq_ques" rows="3" placeholder="MCQ Question" required="" data-validation-required-message="Question is required" aria-invalid="false">{{$get_mcq?$get_mcq['mcq_ques_text']:''}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Option 1</label>
                      <input type="text" class="form-control" id="mcq_option1" name="mcq_option1" placeholder="Enter Option 1" required
                        data-validation-required-message="Option 1 is required" value="{{$get_mcq?$get_mcq['mcq_option1']:''}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Option 2</label>
                      <input type="text" class="form-control" id="mcq_option2" name="mcq_option2" placeholder="Enter Option 2" required
                        data-validation-required-message="Option 2 is required" value="{{$get_mcq?$get_mcq['mcq_option2']:''}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Option 3</label>
                      <input type="text" class="form-control" id="mcq_option3" name="mcq_option3" placeholder="Enter Option 3" required
                        data-validation-required-message="Option 3 is required" value="{{$get_mcq?$get_mcq['mcq_option3']:''}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Option 4</label>
                      <input type="text" class="form-control" id="mcq_option4" name="mcq_option4" placeholder="Enter Option 4" required
                        data-validation-required-message="Option 4 is required" value="{{$get_mcq?$get_mcq['mcq_option4']:''}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Correct Answer</label>
                      <select class="custom-select form-control" name="mcq_correct" id="mcq_correct">
                        <option value="1" @if($get_mcq) @if($get_mcq['mcq_correct_ans'] == '1') selected @endif @endif>Option 1</option>
                        <option value="2" @if($get_mcq) @if($get_mcq['mcq_correct_ans'] == '2') selected @endif @endif>Option 2</option>
                        <option value="3" @if($get_mcq) @if($get_mcq['mcq_correct_ans'] == '3') selected @endif @endif>Option 3</option>
                        <option value="4" @if($get_mcq) @if($get_mcq['mcq_correct_ans'] == '4') selected @endif @endif>Option 4</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Full Marks for this Question</label>
                      <input type="number" class="form-control" id="mcq_full_marks" name="mcq_full_marks" placeholder="Enter Full Marks" required
                        data-validation-required-message="Full Marks is required" value="{{$get_mcq?$get_mcq['mcq_full_marks']:''}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Negative Marks for this Question</label>
                      <input type="number" class="form-control" id="mcq_negative_marks" name="mcq_negative_marks" placeholder="Enter Negative Marks for this Question" required
                        data-validation-required-message="Negative Marks is required" value="{{$get_mcq?$get_mcq['mcq_negative_marks']:''}}">
                    </div>
                  </div>
                </div>
                <!-- <div class="col-12 col-sm-6">

                  <div class="form-group">
                    <label>Status</label>
                    <select class="form-control">
                      <option>Active</option>
                      <option>Blocked</option>
                      <option>deactivated</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Role</label>
                    <select class="form-control">
                      <option>Admin</option>
                      <option>User</option>
                      <option>Staff</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Company</label>
                    <input type="text" class="form-control" value="WinDon Technologies Pvt Ltd"
                      placeholder="Company name">
                  </div>
                </div>
                <div class="col-12">
                  <div class="table-responsive border rounded px-1 ">
                    <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2"><i
                        class="feather icon-lock mr-50 "></i>Permission</h6>
                    <table class="table table-borderless">
                      <thead>
                        <tr>
                          <th>Module</th>
                          <th>Read</th>
                          <th>Write</th>
                          <th>Create</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Users</td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox1"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox1"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox2"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox2"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox3"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox3"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox4"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox4"></label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Articles</td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox5"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox5"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox6"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox6"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox7"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox7"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox8"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox8"></label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Staff</td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox9"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox9"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox10"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox10"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox11"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox11"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox12"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox12"></label>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div> -->
                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                  <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    Changes</button>
                  <button type="reset" class="btn btn-outline-warning">Reset</button>
                </div>
              </div>
            </form>
            <!-- users edit account form ends -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- users edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
  <script>
    //eneble select2 globally
  function enableSelect2(){
      // Basic Select2 select
      $("#select_questions").select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        placeholder: "Click here to select a question",
        dropdownAutoWidth: true,
        width: '100%'
      });
      $('.select2-search__field').attr('placeholder','Click here');
      $('.select2-search__field').css('width','100%');
  }
  enableSelect2();
  </script>
@endsection

