
@extends('layouts/contentLayoutMaster')

@section('title', 'Videos')

@section('vendor-style')
        {{-- vendor files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/extensions/toastr.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/plyr.css')) }}">
@endsection

@section('content')
{{-- Data list view starts --}}
<section id="data-list-view" class="data-list-view-header">
    <!-- <div class="action-btns d-none">
      <div class="btn-dropdown mr-1 mb-1">
        <div class="btn-group dropdown actions-dropodown">
          <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#"><i class="feather icon-trash"></i>Delete</a>
            <a class="dropdown-item" href="#"><i class="feather icon-save"></i>Another Action</a>
          </div>
        </div>
      </div>
    </div> -->
    {{-- DataTable starts --}}
    <div class="table-responsive">
      <table class="table data-list-view">
        <thead>
          <tr>
            <th></th>
            <th>TITLE</th>
            <th>DESCRIPTION</th>
            <th>VIDEO URL</th>
            <th>ADD TO A COURSE</th>
            <th>STATUS</th>
            @canany(['edit videos', 'delete videos', 'view videos'])
              <th>ACTION</th>
            @endcanany
          </tr>
        </thead>
        <tbody>
          @foreach ($videos as $video)
            @if($video["video_status"] === 1)
              <?php $color = "success" ?>
            @elseif($course["video_status"] === 0)
              <?php $color = "danger" ?>
            @endif
            <?php
              $arr = array('Inactive', 'Active', 'info', 'warning', 'danger');
            ?>
            <tr>
              <td></td>
              <td class="product-name">{{ $video["video_title"] }}</td>
              <td class="product-name" data-original-title="Video Description" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{ $video['video_desc'] }}">{{ Str::limit($video["video_desc"],20) }}</td>
              <td class="product-action"><span class="action-preview" data-id="{{$video['id']}}" data-value="{{$video['video_url']}}"><a href="javascript:void(0)">{{$video['video_url']}}</a></i></span></td>
              <td class="product-name"><button type="button" class="btn btn-primary btn-sm add-to-courses" data-id="{{$video['id']}}">Add to a Course</button></td>
              <td>
                <div class="chip chip-{{$color}}">
                  <div class="chip-body">
                    <div class="chip-text">{{ $arr[$video["video_status"]]}}</div>
                  </div>
                </div>
              </td>
              @canany(['edit videos', 'delete videos', 'view videos'])
                <td class="product-action">
                  @can('view videos')
                    <span class="action-view" data-value="{{$video['id']}},{{base64_encode($video['video_title'])}},{{base64_encode($video['video_desc'])}},{{base64_encode($video['video_url'])}},{{base64_encode($video['video_image'])}}"><i class="feather icon-eye"></i></span>
                  @endcan
                  @can('edit videos')
                      <span class="action-edit" data-value="{{$video['id']}},{{base64_encode($video['video_title'])}},{{base64_encode($video['video_desc'])}},{{base64_encode($video['video_url'])}},{{base64_encode($video['video_image'])}}"><i class="feather icon-edit"></i></span>
                  @endcan
                  @can('delete videos')
                      <span class="action-delete" data-value="{{$video['id']}}"><i class="feather icon-trash"></i></span>
                  @endcan
                </td>
              @endcanany
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    {{-- DataTable ends --}}

    {{-- add new sidebar starts --}}
    <div class="add-new-data-sidebar">
      <div class="overlay-bg"></div>
      <div class="add-new-data">
        <form class="form-horizontal" id="video_data_form" enctype="multipart/form-data" novalidate>
          @csrf
          <input type="hidden" id="hid_video_id" name="hid_video_id">
          <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
            <div>
              <h4 class="text-uppercase">Video Data</h4>
            </div>
            <div class="hide-data-sidebar">
              <i class="feather icon-x"></i>
            </div>
          </div>
          <div class="data-items pb-3">
            <div class="data-fields px-2 mt-1">
              <div class="row">
                <div class="col-sm-12 form-group data-field-col">
                  <label for="data-name">Title</label>
                  <div class="controls">
                    <input type="text" class="form-control" name="video_title" id="video_title" placeholder="Video Title" required
                        data-validation-required-message="Video Title is required">
                  </div>
                </div>
                <div class="col-sm-12 form-group data-field-col">
                  <label for="data-category"> Description </label>
                  <div class="controls">
                    <textarea class="form-control" id="video_desc" name="video_desc" rows="3" placeholder="Video Description" required
                        data-validation-required-message="Video Description is required"></textarea>
                  </div>
                </div>
                <div class="col-12 form-group data-field-col">
                    <label for="first-name-column">URL</label>
                    <div class="controls">
                      <input type="url" id="video_url" class="form-control" placeholder="Youtube/Vimeo Video URL" name="video_url" required
                        data-validation-required-message="Video URL is required">
                    </div>
                </div>
                <div class="col-12">
                    <fieldset class="form-group data-field-col" id="image_form_group">
                        <label for="basicInputFile">Image (JPG,PNG,JPEG)</label>
                        <div class="custom-file">
                            <div class="controls" id="image_controls">
                              <input type="file" accept="image/*" class="custom-file-input" name="video_image" id="video_image">
                              <input type="hidden" id="hid_image_file">
                              <label class="custom-file-label" for="image" id="image-label">
                                  Choose file
                              </label>
                        </div>
                        </div>
                    </fieldset>
                </div>
              </div>
            </div>
          </div>
          <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
            <div class="add-data-btn">
              <button type="submit" class="btn btn-primary" id="save_video_btn">
                  <span class="spinner-border spinner-border-sm" style="display:none" role="status" aria-hidden="true" id="loader_btn"></span>
                  Save
              </button>
            </div>
            <div class="cancel-data-btn">
              <button type="reset" class="btn btn-outline-danger">Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    {{-- add new sidebar ends --}}

    {{-- Modal --}}
      <div class="modal fade" id="view_video_modal" tabindex="-1" role="dialog"
              aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Video Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <label>Video Title: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_video_title" disabled>
                    </div>
                    <label>Video Description: </label>
                    <div class="form-group">
                      <textarea id="view_video_desc" disabled class="form-control"></textarea>
                    </div>
                    <label>Video URL: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_video_url" disabled>
                    </div>
                    <label>Video Image: </label>
                    <div class="form-group">
                      <img id="view_video_image" width="250" height="100"/>
                    </div>
                  </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Video --}}
      <div class="modal fade" id="video_preview_modal" tabindex="-1" role="dialog"
              aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Preview Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" id="iframe_body">
                      <video controls crossorigin playsinline id="player">
                      </video>
                  </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    {{-- Modal Select Course --}}
      <div class="modal fade" id="course_select_modal" tabindex="-1" role="dialog"
              aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Add Video to Courses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <label>Select a Course: </label>
                    <div class="form-group">
                        <select class="select2 form-control" id="select_courses" data-placeholder="Click here to select a course" multiple="multiple">
                        </select>
                      </div>
                  </div>
            <div class="modal-footer">
              <div class="add-data-btn">
                <button type="button" class="btn btn-primary" id="add_course_id_btn">
                    <span class="spinner-border spinner-border-sm" style="display:none" role="status" aria-hidden="true" id="loader_btn"></span>
                    Add
                </button>
              </div>
              <div class="cancel-data-btn">
                <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancel</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </section>
  {{-- Data list view end --}}
@endsection
@section('vendor-script')
{{-- vendor js files --}}
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/media/plyr.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/ui/video-list-view.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/popover/popover.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/extensions/media-plyr.js')) }}"></script>
@endsection
