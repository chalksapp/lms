@extends('layouts/fullLayoutMaster')

@section('title', 'Login Page')

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection
@section('content')
<section class="row flexbox-container">
  <div class="col-xl-8 col-11 d-flex justify-content-center">
      <div class="card bg-authentication rounded-0 mb-0">
          <div class="row m-0">
              <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                  <img src="{{ asset('images/pages/login.png') }}" alt="branding logo">
              </div>
              <div class="col-lg-6 col-12 p-0">
                  <div class="card rounded-0 mb-0 px-2">
                      <div class="card-header pb-1">
                          <div class="card-title">
                              <h4 class="mb-0">LMS - Super Admin Login</h4>
                          </div>
                      </div>
                      <p class="px-2">Welcome back, please login to your account.</p>
                      <div class="card-content">
                          <div class="card-body pt-1">
                              <form action="user-login" method="POST">
                                {{ csrf_field() }}
                                  @if (\Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul role="alert">
                                            <li>{!! \Session::get('error') !!}</li>
                                        </ul>
                                    </div>
                                  @endif
                                  @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul role="alert">
                                            @if($errors->first('email'))<li>{{ $errors->first('email') }}</li>@endif
                                            @if($errors->first('password'))<li>{{ $errors->first('password') }}</li>@endif
                                        </ul>
                                    </div>
                                  @endif
                                  <label for="user-name">Email</label>
                                  <fieldset class="form-label-group form-group position-relative has-icon-left">
                                      <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                                      <div class="form-control-position">
                                          <i class="feather icon-user"></i>
                                      </div>
                                  </fieldset>
                                  <label for="user-password">Password</label>
                                  <fieldset class="form-label-group position-relative has-icon-left">
                                      <input type="password" class="form-control" name="password" id="user-password" placeholder="Password" minlength="6" required>
                                      <div class="form-control-position">
                                          <i class="feather icon-lock"></i>
                                      </div>
                                  </fieldset>
                                  <div class="form-group d-flex justify-content-between align-items-center">
                                      <div class="text-left">
                                      </div>
                                  </div>
                                  <button type="submit" class="btn btn-primary float-right btn-inline">Login</button>
                              </form>
                          </div>
                      </div>
                      <div class="login-footer">
                        
                        
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
</section>
@endsection
