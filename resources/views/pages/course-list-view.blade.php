
@extends('layouts/contentLayoutMaster')

@section('title', 'Courses')

@section('vendor-style')
        {{-- vendor files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/extensions/toastr.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('content')
{{-- Data list view starts --}}
<section id="data-list-view" class="data-list-view-header">
    <!-- <div class="action-btns d-none">
      <div class="btn-dropdown mr-1 mb-1">
        <div class="btn-group dropdown actions-dropodown">
          <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#"><i class="feather icon-trash"></i>Delete</a>
            <a class="dropdown-item" href="#"><i class="feather icon-save"></i>Another Action</a>
          </div>
        </div>
      </div>
    </div> -->

    {{-- DataTable starts --}}
    <div class="table-responsive">
      <table class="table data-list-view">
        <thead>
          <tr>
            <th></th>
            <th>NAME</th>
            <th>DESCRIPTION</th>
            <th>SCHEDULE</th>
            <th>FEES</th>
            <th>DURATION</th>
            <th>STATUS</th>
            @canany(['edit courses', 'delete courses', 'view courses'])
                <th>ACTION</th>
            @endcanany
          </tr>
        </thead>
        <tbody>
          @foreach ($courses as $course)
            @if($course["course_status"] === 1)
              <?php $color = "success" ?>
            @elseif($course["order_status"] === 0)
              <?php $color = "danger" ?>
            @endif
            <?php
              $arr = array('Inactive', 'Active', 'info', 'warning', 'danger');
            ?>
            <tr>
              <td></td>
              <td class="product-name"><a class="course_name_click" data-id="{{$course['id']}}" href="javascript:void(0)">{{ $course["course_name"] }}</a></td>
              <td class="product-name" data-original-title="Course Description" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{ $course['course_desc'] }}">{{ Str::limit($course["course_desc"],20) }}</td>
              <td class="product-name"><a target="_blank" href="{{URL::to('/')}}/course_files/{{$course['id']}}/{{ $course['course_schedule'] }}">{{ $course["course_schedule"] }}</a></td>
              <td class="product-price">{{ $course["course_fees"] }}</td>
              <td class="product-category">{{ gmdate("H:i:s", $course["course_duration"]) }}</td>
              <td>
                <div class="chip chip-{{$color}}">
                  <div class="chip-body">
                    <div class="chip-text">{{ $arr[$course["course_status"]]}}</div>
                  </div>
                </div>
              </td>
              @canany(['edit courses', 'delete courses', 'view courses'])
                  <td class="product-action">
                      @can('view courses')
                          <span class="action-view" data-value="{{$course['id']}},{{base64_encode($course['course_name'])}},{{base64_encode($course['course_desc'])}},{{$course['course_start_date']}},{{base64_encode($course['course_fees'])}},{{$course['course_level']}},{{base64_encode($course['course_duration'])}},{{$course['course_period']}},{{base64_encode($course['course_schedule'])}},{{base64_encode($course['course_image'])}}"><i class="feather icon-eye"></i></span>
                      @endcan
                      @can('edit courses')
                          <span class="action-edit" data-value="{{$course['id']}},{{base64_encode($course['course_name'])}},{{base64_encode($course['course_desc'])}},{{$course['course_start_date']}},{{base64_encode($course['course_fees'])}},{{$course['course_level']}},{{base64_encode($course['course_duration'])}},{{$course['course_period']}},{{base64_encode($course['course_schedule'])}},{{base64_encode($course['course_image'])}}"><i class="feather icon-edit"></i></span>
                      @endcan
                      @can('delete courses')
                          <span class="action-delete" data-value="{{$course['id']}}"><i class="feather icon-trash"></i></span>
                      @endcan
                  </td>
              @endcanany
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    {{-- DataTable ends --}}

    {{-- add new sidebar starts --}}
    <div class="add-new-data-sidebar">
      <div class="overlay-bg"></div>
      <div class="add-new-data">
        <form class="form-horizontal" id="course_data_form" enctype="multipart/form-data" novalidate>
          @csrf
          <input type="hidden" id="hid_course_id" name="hid_course_id">
          <div class="div mt-2 px-2 d-flex new-data-title justify-content-between">
            <div>
              <h4 class="text-uppercase">Course Data</h4>
            </div>
            <div class="hide-data-sidebar">
              <i class="feather icon-x"></i>
            </div>
          </div>
          <div class="data-items pb-3">
            <div class="data-fields px-2 mt-1">
              <div class="row">
                <div class="col-sm-12 form-group data-field-col">
                  <label for="data-name">Name</label>
                  <div class="controls">
                    <input type="text" class="form-control" name="course_name" id="course_name" placeholder="Course Name" required
                        data-validation-required-message="Course Name is required">
                  </div>
                </div>
                <div class="col-sm-12 form-group data-field-col">
                  <label for="data-category"> Description </label>
                  <div class="controls">
                    <textarea class="form-control" id="course_desc" name="course_desc" rows="3" placeholder="Course Description" required
                        data-validation-required-message="Course Description is required"></textarea>
                  </div>
                </div>
                <div class="col-sm-12 form-group data-field-col">
                  <label for="data-status">Starting Date</label>
                  <div class="controls">
                    <input type='text' class="form-control pickadate" name="course_date" id="course_date" placeholder="Course Start Date" required
                        data-validation-required-message="Course Start Date is required"/>
                  </div>
                </div>
                <div class="col-sm-12 form-group data-field-col">
                  <label for="data-price">Fee (Rs)</label>
                  <div class="controls">
                    <input type="number" class="form-control" name="course_fee" id="course_fee" placeholder="Course Fees" required
                        data-validation-required-message="Course Fees is required">
                  </div>
                </div>
                <div class="col-sm-12 form-group data-field-col">
                  <label for="data-popularity">Level</label>
                  <div class="controls">
                      <select class="custom-select form-control" name="course_level" id="course_level">
                        <option value="Beginner">Beginner</option>
                        <option value="Intermediate">Intermediate</option>
                        <option value="Advanced">Advanced</option>
                      </select>
                  </div>
                </div>
                <div class="col-md-6 col-12 form-group data-field-col">
                    <label for="first-name-column">Duration (Minutes)</label>
                    <div class="controls">
                      <input type="text" id="course_duration" class="form-control" placeholder="Course Duration" name="course_duration" required
                        data-validation-required-message="Course Duration is required">
                    </div>
                </div>
                <div class="col-md-6 col-12 form-group data-field-col">
                    <label for="first-name-column">Period</label>
                    <div class="controls">
                      <select class="custom-select form-control" id="course_period" name="course_period" >
                          <option value="Weekly">Weekly</option>
                          <option value="Monthly">Monthly</option>
                      </select>
                    </div>
                </div>
                <div class="col-12">
                    <fieldset class="form-group data-field-col" id="schedule_form_group">
                        <label for="basicInputFile">Schedule (PDF)</label>
                        <div class="controls" id="schedule_controls">
                          <div class="custom-file">
                              <input type="file" accept=".pdf" class="custom-file-input" id="course_schedule" name="course_schedule">
                              <input type="hidden" id="hid_schedule_file">
                              <label class="custom-file-label" for="schedule" id="schedule-label">
                                  Choose file
                              </label>
                          </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-12">
                    <fieldset class="form-group data-field-col" id="image_form_group">
                        <label for="basicInputFile">Course Tile Image (JPG,PNG,JPEG)</label>
                        <div class="custom-file">
                            <div class="controls" id="image_controls">
                              <input type="file" accept="image/*" class="custom-file-input" name="course_image" id="course_image">
                              <input type="hidden" id="hid_image_file">
                              <label class="custom-file-label" for="image" id="image-label">
                                  Choose file
                              </label>
                        </div>
                        </div>
                    </fieldset>
                </div>
              </div>
            </div>
          </div>
          <div class="add-data-footer d-flex justify-content-around px-3 mt-2">
            <div class="add-data-btn">
              <button type="submit" class="btn btn-primary" id="save_course_btn">
                  <span class="spinner-border spinner-border-sm" style="display:none" role="status" aria-hidden="true" id="loader_btn"></span>
                  Save
              </button>
            </div>
            <div class="cancel-data-btn">
              <button type="reset" class="btn btn-outline-danger">Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    {{-- add new sidebar ends --}}

    {{-- Modal --}}
      <div class="modal fade" id="view_course_modal" tabindex="-1" role="dialog"
              aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">Course Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <label>Course Name: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_course_name" disabled>
                    </div>
                    <label>Course Description: </label>
                    <div class="form-group">
                      <textarea id="view_course_desc" disabled class="form-control"></textarea>
                    </div>
                    <label>Course Starting Date: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_course_date" disabled>
                    </div>
                    <label>Course Fees: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_course_fees" disabled>
                    </div>
                    <label>Course Level: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_course_level" disabled>
                    </div>
                    <label>Course Duration: (Minutes)</label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_course_duration" disabled>
                    </div>
                    <label>Course Period: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_course_period" disabled>
                    </div>
                    <label>Course Schedule: </label>
                    <div class="form-group">
                      <a target="_blank" id="view_course_schedule"></a>
                    </div>
                    <label>Course Image: </label>
                    <div class="form-group">
                      <img id="view_course_image" width="250" height="100"/>
                    </div>
                  </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          </form>
        </div>
      </div>
    </div>


  </section>
  {{-- Data list view end --}}
@endsection
@section('vendor-script')
{{-- vendor js files --}}
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.time.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/pickers/pickadate/legacy.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/polyfill.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/ui/course-list-view.js')) }}"></script>
        <script src="{{ asset(mix('js/scripts/popover/popover.js')) }}"></script>
@endsection
