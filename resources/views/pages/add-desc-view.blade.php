@extends('layouts/contentLayoutMaster')

@section('title', 'Descriptive Question')

@section('vendor-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-user.css')) }}">
        
@endsection

@section('content')
<!-- users edit start -->
<section class="users-edit">
  <div class="card">
    <div class="card-content">
      <div class="card-body">
        <ul class="nav nav-tabs mb-3" role="tablist">
          <li class="nav-item">
            <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account"
              aria-controls="account" role="tab" aria-selected="true">
              <i class="feather icon-file-text mr-25"></i><span class="d-none d-sm-block">Descriptive Question</span>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link d-flex align-items-center" id="information-tab" data-toggle="tab" href="#information"
              aria-controls="information" role="tab" aria-selected="false">
              <i class="feather icon-info mr-25"></i><span class="d-none d-sm-block">Information</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link d-flex align-items-center" id="social-tab" data-toggle="tab" href="#social"
              aria-controls="social" role="tab" aria-selected="false">
              <i class="feather icon-share-2 mr-25"></i><span class="d-none d-sm-block">Social</span>
            </a>
          </li> -->
        </ul>
        <div class="tab-content">
          <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
            <!-- users edit media object start -->
            <!--<div class="media mb-2">
              <a class="mr-2 my-25" href="#">
                <img src="{{ asset('images/portrait/small/avatar-s-12.jpg') }}" alt="users avatar"
                  class="users-avatar-shadow rounded" height="64" width="64">
              </a>
              <div class="media-body mt-50">
                <h4 class="media-heading">Angelo Sashington</h4>
                <div class="col-12 d-flex mt-1 px-0">
                  <a href="#" class="btn btn-primary d-none d-sm-block mr-75">Change</a>
                  <a href="#" class="btn btn-primary d-block d-sm-none mr-75"><i
                      class="feather icon-edit-1"></i></a>
                  <a href="#" class="btn btn-outline-danger d-none d-sm-block">Remove</a>
                  <a href="#" class="btn btn-outline-danger d-block d-sm-none"><i class="feather icon-trash-2"></i></a>
                </div>
              </div>
            </div> -->
            <!-- users edit media object ends -->
            <!-- users edit account form start -->
            <form novalidate method="POST" action="{{ route('create-desc')}}">
              @csrf
              <div class="row">
                <div class="col-12 col-sm-12">
                  <!-- <div class="form-group">
                    <div class="controls">
                      <label>Username</label>
                      <input type="text" class="form-control" placeholder="Username" value="adoptionism744" required
                        data-validation-required-message="This username field is required">
                    </div>
                  </div> -->
                  <input type="hidden" name="hid_desc_id" id="hid_desc_id" value="{{$get_desc?$get_desc['id']:''}}">
                  <div class="form-group">
                    <div class="controls">
                      <label>Question Title</label>
                      <input type="text" class="form-control" id="desc_title" name="desc_title" placeholder="Enter a Title for easy filter" required
                        data-validation-required-message="This title field is required" value="{{$get_desc?$get_desc['desc_ques_title']:''}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Question</label>
                      <textarea class="form-control" id="desc_ques" name="desc_ques" rows="3" placeholder="Decsriptive Question" required="" data-validation-required-message="Question is required" aria-invalid="false">{{$get_desc?$get_desc['desc_ques_text']:''}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Answer</label>
                      <textarea class="form-control" id="desc_ans" name="desc_ans" rows="3" placeholder="Answer for the above question" required="" data-validation-required-message="Answer is required" aria-invalid="false">{{$get_desc?$get_desc['desc_answer_text']:''}}</textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Full Marks for this Question</label>
                      <input type="number" class="form-control" id="desc_full_marks" name="desc_full_marks" placeholder="Enter Full Marks" required
                        data-validation-required-message="Full Marks is required" value="{{$get_desc?$get_desc['desc_full_marks']:''}}">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="controls">
                      <label>Negative Marks for this Question</label>
                      <input type="number" class="form-control" id="desc_negative_marks" name="desc_negative_marks" placeholder="Enter Negative Marks for this Question" required
                        data-validation-required-message="Negative Marks is required" value="{{$get_desc?$get_desc['desc_negative_marks']:''}}">
                    </div>
                  </div>
                </div>
                <!-- <div class="col-12 col-sm-6">

                  <div class="form-group">
                    <label>Status</label>
                    <select class="form-control">
                      <option>Active</option>
                      <option>Blocked</option>
                      <option>deactivated</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Role</label>
                    <select class="form-control">
                      <option>Admin</option>
                      <option>User</option>
                      <option>Staff</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Company</label>
                    <input type="text" class="form-control" value="WinDon Technologies Pvt Ltd"
                      placeholder="Company name">
                  </div>
                </div>
                <div class="col-12">
                  <div class="table-responsive border rounded px-1 ">
                    <h6 class="border-bottom py-1 mx-1 mb-0 font-medium-2"><i
                        class="feather icon-lock mr-50 "></i>Permission</h6>
                    <table class="table table-borderless">
                      <thead>
                        <tr>
                          <th>Module</th>
                          <th>Read</th>
                          <th>Write</th>
                          <th>Create</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Users</td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox1"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox1"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox2"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox2"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox3"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox3"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox4"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox4"></label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Articles</td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox5"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox5"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox6"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox6"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox7"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox7"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox8"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox8"></label>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Staff</td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox9"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox9"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox10"
                                class="custom-control-input" checked>
                              <label class="custom-control-label" for="users-checkbox10"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox11"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox11"></label>
                            </div>
                          </td>
                          <td>
                            <div class="custom-control custom-checkbox"><input type="checkbox" id="users-checkbox12"
                                class="custom-control-input"><label class="custom-control-label"
                                for="users-checkbox12"></label>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div> -->
                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                  <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    Changes</button>
                  <button type="reset" class="btn btn-outline-warning">Reset</button>
                </div>
              </div>
            </form>
            <!-- users edit account form ends -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- users edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pages/app-user.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
@endsection

