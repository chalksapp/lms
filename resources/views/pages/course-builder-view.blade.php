@extends('layouts/contentLayoutMaster')

@section('title', 'Course Builder Page')

@section('vendor-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/pickadate/pickadate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/dragula.min.css')) }}">
@endsection

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-ecommerce-shop.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-user.css')) }}">
        <style>
          .custom-image-deck{height:150px;}
        </style>
@endsection
@section('content')
<!-- users edit start -->
<section class="users-edit">
  <div class="card">
    <div class="card-content">
      <div class="card-body">
        <ul class="nav nav-tabs mb-3" role="tablist">
          <li class="nav-item">
            <a class="nav-link d-flex align-items-center @if(!request()->route('tab')) active @endif" id="account-tab" data-toggle="tab" href="#account"
              aria-controls="account" role="tab" aria-selected="true">
              <i class="feather icon-user mr-25"></i><span class="d-none d-sm-block">Course Basics</span>
            </a>
          </li>
          @if($this_course && $this_course['id']!= '')
            <li class="nav-item">
              <a class="nav-link d-flex align-items-center @if(request()->route('tab') == 2) active @endif" id="information-tab" data-toggle="tab" href="#information"
                aria-controls="information" role="tab" aria-selected="false">
                <i class="feather icon-info mr-25"></i><span class="d-none d-sm-block">Course Curriculum</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link d-flex align-items-center @if(request()->route('tab') == 3) active @endif" id="social-tab" data-toggle="tab" href="#social"
                aria-controls="social" role="tab" aria-selected="false">
                <i class="feather icon-users mr-25"></i><span class="d-none d-sm-block">Course Users</span>
              </a>
            </li>
          @endif
        </ul>
        <div class="tab-content">
          <div class="tab-pane @if(!request()->route('tab')) active @endif" id="account" aria-labelledby="account-tab" role="tabpanel">
            <!-- users edit account form start -->
            <form class="form-horizontal" method="POST" id="course_data_form" enctype="multipart/form-data" action="{{route('save-course-data')}}">
              @csrf
              <input type="hidden" id="hid_course_id" name="hid_course_id" value="{{$this_course?$this_course['id']:''}}">
              <div class="data-items pb-3">
                <div class="data-fields px-2 mt-1">
                  <div class="row">
                    <div class="col-sm-12 form-group data-field-col">
                      <label for="data-name">Name</label>
                      <div class="controls">
                        <input type="text" class="form-control" name="course_name" id="course_name" placeholder="Course Name"
                            data-validation-required-message="Course Name is required" value="{{$this_course?$this_course['course_name']:''}}" required>
                      </div>
                    </div>
                    <div class="col-sm-12 form-group data-field-col">
                      <label for="data-category"> Description </label>
                      <div class="controls">
                        <textarea required class="form-control" id="course_desc" name="course_desc" rows="3" placeholder="Course Description"
                            data-validation-required-message="Course Description is required">{{$this_course?$this_course['course_desc']:''}}</textarea>
                      </div>
                    </div>
                    <div class="col-sm-12 form-group data-field-col">
                      <label for="data-status">Starting Date</label>
                      <div class="controls">
                        <input type='text' required class="form-control pickadate" name="course_date" id="course_date" placeholder="Course Start Date"
                            data-validation-required-message="Course Start Date is required" value="{{$this_course?$this_course['course_start_date']:''}}"/>
                      </div>
                    </div>
                    <div class="col-sm-12 form-group data-field-col">
                      <label for="data-price">Fee (Rs)</label>
                      <div class="controls">
                        <input type="number" class="form-control" required name="course_fee" id="course_fee" placeholder="Course Fees"
                            data-validation-required-message="Course Fees is required" value="{{$this_course?$this_course['course_fees']:''}}">
                      </div>
                    </div>
                    <div class="col-sm-12 form-group data-field-col">
                      <label for="data-popularity">Level</label>
                      <div class="controls">
                          <select class="custom-select form-control" name="course_level" id="course_level">
                            <option value="Beginner" @if($this_course) @if($this_course['course_level'] == 'Beginner') selected @endif @endif>Beginner</option>
                            <option value="Intermediate" @if($this_course) @if($this_course['course_level'] == 'Intermediate') selected @endif @endif>Intermediate</option>
                            <option value="Advanced" @if($this_course) @if($this_course['course_level'] == 'Advanced') selected @endif @endif>Advanced</option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-6 col-12 form-group data-field-col">
                        <label for="first-name-column">Duration (Minutes)</label>
                        <div class="controls">
                          <input type="text" id="course_duration" required class="form-control" placeholder="Course Duration" name="course_duration"
                            data-validation-required-message="Course Duration is required" value="{{$this_course?$this_course['course_duration']:''}}">
                        </div>
                    </div>
                    <div class="col-md-6 col-12 form-group data-field-col">
                        <label for="first-name-column">Period</label>
                        <div class="controls">
                          <select class="custom-select form-control" id="course_period" name="course_period" >
                              <option value="Weekly" @if($this_course) @if($this_course['course_period'] == 'Weekly') selected @endif @endif>Weekly</option>
                              <option value="Monthly" @if($this_course) @if($this_course['course_period'] == 'Monthly') selected @endif @endif>Monthly</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <fieldset class="form-group data-field-col" id="schedule_form_group">
                            <label for="basicInputFile">Schedule (PDF)</label>
                            <div class="controls" id="schedule_controls">
                              <div class="custom-file">
                                  <input type="file" accept="application/pdf" class="custom-file-input" id="course_schedule" name="course_schedule">
                                  <input type="hidden" id="hid_schedule_file" value="{{$this_course?$this_course['course_schedule']:''}}">
                                  <label class="custom-file-label" for="schedule" id="schedule-label">
                                      {{$this_course?$this_course['course_schedule']:'Choose a PDF file'}}
                                  </label>
                              </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-12">
                        <fieldset class="form-group data-field-col" id="image_form_group">
                            <label for="basicInputFile">Course Tile Image (JPG,PNG,JPEG)</label>
                            <div class="custom-file">
                                <div class="controls" id="image_controls">
                                  <input type="file" accept="image/*"  class="custom-file-input" name="course_image" id="course_image">
                                  <input type="hidden" id="hid_image_file" value="{{$this_course?$this_course['course_image']:''}}">
                                  <label class="custom-file-label" for="image" id="image-label">
                                      {{$this_course?$this_course['course_image']:'Choose a JPG/PNG/GIF file'}}
                                  </label>
                            </div>
                            </div>
                        </fieldset>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                  <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                    Changes</button>
                  <button type="reset" class="btn btn-outline-warning">Reset</button>
              </div>
            </form>
            <!-- users edit account form ends -->
          </div>
          <div class="tab-pane @if(request()->route('tab') == 2) active @endif" id="information" aria-labelledby="information-tab" role="tabpanel">
            <button type="button" class="btn btn-outline-primary mr-1 mb-1" id="add_curriculum_content"><i class="feather icon-plus"></i> Add Content</button>
            @if(!empty($this_course->curriculum[0]))
                <!-- Wishlist Starts -->
                <section>
                  <div class="row">
                    <div class="col-12">
                      <div class="card-deck-wrapper">
                        <div class="card-deck" id="card-drag-area">
                           @foreach($this_course->curriculum as $curriculum)
                              <div class="card curriculum-card" data-content-id="{{$curriculum->content_id}}_{{$curriculum->content_type}}">
                                <div class="card-content">
                                  @if(!empty($curriculum->course_content->video_image))
                                      <img src="{{$asset_video_path}}{{$curriculum->content_id}}/{{$curriculum->course_content->video_image}}" class="card-img-top img-fluid img-thumbnail custom-image-deck" alt="img-placeholder">
                                  @elseif(!empty($curriculum->course_content->test_image))
                                      <img src="{{$asset_test_path}}{{$curriculum->content_id}}/{{$curriculum->course_content->test_image}}" class="card-img-top img-fluid img-thumbnail custom-image-deck" alt="img-placeholder">
                                  @elseif(!$curriculum->course_content->video_image && !$curriculum->course_content->test_image)
                                      <img src="{{asset('images/banner/upgrade.png')}}" class="card-img-top img-fluid img-thumbnail custom-image-deck" alt="img-placeholder">
                                  @endif
                                  <div class="card-body">
                                      @if($curriculum->content_type == 1)
                                        <h4 class="card-title">{{$curriculum->course_content->video_title}}</h4>
                                      @elseif($curriculum->content_type == 2)
                                        <h4 class="card-title">{{$curriculum->course_content->test_title}}</h4>
                                      @elseif($curriculum->content_type == 3)
                                        <h4 class="card-title">{{$curriculum->course_content->class_name}}</h4>
                                      @endif
                                    @if($curriculum->content_type == 1)
                                      <p class="card-text"><small class="text-muted">Video Content</small></p>
                                    @elseif($curriculum->content_type == 2)
                                      <p class="card-text"><small class="text-muted">Test Content</small></p>
                                    @elseif($curriculum->content_type == 3)
                                      <p class="card-text"><small class="text-muted">Class Content</small></p>
                                    @endif
                                    <button class="btn btn-danger mt-1 waves-effect waves-light remove-from-curriculum" data-curriculum-id="{{$curriculum->content_id}}_{{$curriculum->content_type}}"><i class="feather icon-trash align-middle"></i> Remove</button>
                                  </div>
                                </div>
                              </div>
                              @if($loop->iteration % 3 ==  0)
                                </div>
                              </div>
                             </div>
                                 <div class="col-12">
                                  <div class="card-deck-wrapper">
                                    <div class="card-deck" id="card-drag-area">
                              @endif
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            <!-- Wishlist Ends -->
              @else
                  {{-- Basic Alerts start --}}
                      <section id="basic-alerts">
                        <div class="row">
                          <div class="col-xl-12 col-lg-12">
                            <div class="card">
                              <div class="card-header">
                                <h4 class="card-title">No Contents Added For This Course</h4>
                              </div>
                              <div class="card-content">
                                <div class="card-body">
                                  <p>
                                    Contents can be added from here by clicking the Add Content Button. You can re arrange the order of Added Contents by simole drag and drop.
                                  </p>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                {{-- Basic Alerts end --}}
             @endif
          </div>
          <div class="tab-pane @if(request()->route('tab') == 3) active @endif" id="social" aria-labelledby="social-tab" role="tabpanel">
            <!-- add users to course starts -->
            <form novalidate method="POST" action="{{ route('save-course-users')}}" name="course_user_form" id="course_user_form">
                @csrf
                <input name="hidden_course_id" type="hidden" id="hidden_course_id" value="{{$this_course?$this_course['id']:''}}">
                <div class="form-group">
                  <div class="controls">
                      <fieldset>
                        <label>
                          <input type="checkbox" value="" id="select_all_check">
                          Select All Users
                        </label>
                      </fieldset>
                  </div>
                </div>
                <div class="form-group">
                  <div class="controls">
                      <label>Select Users and Save: </label>
                      <select class="select2 form-control" name="course_select_users[]" id="course_select_users" data-placeholder="Click here to select a user" multiple="multiple">
                          @if(!empty($all_users))
                            @foreach($all_users as $user)
                              <option value="{{$user['id']}}"
                                  @if(in_array($user["id"], $course_user_array))
                                      selected="selected"
                                  @endif
                              >{{$user['name']}} ({{$user['email']}})
                              </option>
                            @endforeach
                          @endif
                      </select>
                      <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                          <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                            Changes</button>
                          <button type="reset" class="btn btn-outline-warning">Reset</button>
                      </div>
                  </div>
                </div>
            </form>
            <!-- add users to course ends -->
          </div>
        </div>
      </div>
    </div>
  </div>
   {{-- Add Curriculum Modal --}}
      <div class="modal fade text-left" id="add_content_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel33" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="myModalLabel33">ADD COURSE CONTENT</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" action="{{ route('save-course-content')}}" name="course_content_form" id="course_content_form">
              @csrf
              <input type="hidden" name="hidden_curriculum_course_id" value="{{$this_course?$this_course['id']:''}}">
              <div class="modal-body">
                <div class="form-group">
                  <div class="controls">
                    <label>Select Videos </label>
                    <select class="select2 form-control" name="add_video_to_course[]" id="add_video_to_course" data-placeholder="Click here to select a Video" multiple="multiple">
                        @if(!empty($all_videos))
                          @foreach($all_videos as $video)
                            <option value="{{$video['id']}}"
                                @if(in_array($video["id"], $video_selected))
                                    selected="selected"
                                @endif
                            >{{$video['video_title']}}
                            </option>
                          @endforeach
                        @endif
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="controls">
                    <label>Select Tests </label>
                    <select class="select2 form-control" name="add_test_to_course[]" id="add_test_to_course" data-placeholder="Click here to select a Video" multiple="multiple">
                        @if(!empty($all_tests))
                          @foreach($all_tests as $test)
                            <option value="{{$test['id']}}"
                                @if(in_array($test["id"], $test_selected))
                                    selected="selected"
                                @endif
                            >{{$test['test_title']}}
                            </option>
                          @endforeach
                        @endif
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="controls">
                    <label>Select Classes </label>
                    <select class="select2 form-control" name="add_class_to_course[]" id="add_class_to_course" data-placeholder="Click here to select a Video" multiple="multiple">
                        @if(!empty($all_classes))
                          @foreach($all_classes as $class)
                            <option value="{{$class['id']}}"
                                @if(in_array($class["id"], $class_selected))
                                    selected="selected"
                                @endif
                            >{{$class['class_name']}}
                            </option>
                          @endforeach
                        @endif
                    </select>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">CANCEL</button>
                <button type="submit" class="btn btn-primary">ADD TO COURSE</button>
              </div>
            </form>
          </div>
        </div>
      </div>
</section>
<!-- users edit ends -->
@endsection

@section('vendor-script')
  {{-- Vendor js files --}}
  <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/pickers/pickadate/picker.date.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/validation/jquery.validate.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/validation/additional-methods.js')) }}"></script>
  <script src="{{ asset(mix('vendors/js/extensions/dragula.min.js')) }}"></script>
@endsection

@section('page-script')
  {{-- Page js files --}}
  <script src="{{ asset(mix('js/scripts/pickers/dateTime/pick-a-datetime.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/navs/navs.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/pages/app-ecommerce-shop.js')) }}"></script>
  <script src="{{ asset(mix('js/scripts/ui/course-builder.js')) }}"></script>
  <script>
        var this_course_id = "{{$this_course?$this_course['id']:''}}";
  </script>
@endsection

