
@extends('layouts/contentLayoutMaster')

@section('title', 'Users')

@section('vendor-style')
        {{-- vendor files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/extensions/toastr.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection

@section('content')
{{-- Data list view starts --}}
<section id="data-list-view" class="data-list-view-header">
    <!-- <div class="action-btns d-none">
      <div class="btn-dropdown mr-1 mb-1">
        <div class="btn-group dropdown actions-dropodown">
          <button type="button" class="btn btn-white px-1 py-1 dropdown-toggle waves-effect waves-light"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Actions
          </button>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="#"><i class="feather icon-trash"></i>Delete</a>
            <a class="dropdown-item" href="#"><i class="feather icon-save"></i>Another Action</a>
          </div>
        </div>
      </div>
    </div> -->
    {{-- DataTable starts --}}
    <div class="table-responsive">
      <table class="table data-list-view">
        <thead>
          <tr>
            <th></th>
            <th>NAME</th>
            <th>EMAIL</th>
            <th>STATUS</th>
            @canany(['edit user', 'delete user', 'view user'])
                <th>ACTION</th>
            @endcanany
          </tr>
        </thead>
        <tbody>
          @foreach ($users as $user)
            @if($user["user_status"] === 1)
              <?php $color = "success" ?>
            @elseif($user["user_status"] === 0)
              <?php $color = "danger" ?>
            @endif
            <?php
              $arr = array('Inactive', 'Active', 'info', 'warning', 'danger');
            ?>
            <tr>
              <td></td>
              <td class="product-name">{{ $user["name"] }}</td>
              <td class="product-name">{{ $user["email"] }}</td>
              <td>
                <div class="chip chip-{{$color}}">
                  <div class="chip-body">
                    <div class="chip-text">{{ $arr[$user["user_status"]]}}</div>
                  </div>
                </div>
              </td>
              @canany(['edit user', 'delete user', 'view user'])
                <td class="product-action">
                  @can('view user')
                      <span class="action-view" data-value="{{$user['id']}},{{base64_encode($user['name'])}},{{base64_encode($user['email'])}}"><i class="feather icon-eye"></i></span>
                  @endcan
                  @can('edit user')
                      <span class="action-edit" data-value="{{$user['id']}}"><i class="feather icon-edit"></i></span>
                  @endcan
                  @can('delete user')
                      <span class="action-delete" data-value="{{$user['id']}}"><i class="feather icon-trash"></i></span>
                  @endcan
                </td>
              @endcanany
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    {{-- DataTable ends --}}
    {{-- View Users Modal --}}
      <div class="modal fade" id="view_user_modal" tabindex="-1" role="dialog"
              aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalScrollableTitle">User Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <label>User Name: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_user_name" disabled>
                    </div>
                    <label>User Email: </label>
                    <div class="form-group">
                      <input type="text" class="form-control" id="view_user_email" disabled>
                    </div>
                  </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  {{-- Data list view end --}}
@endsection
@section('vendor-script')
{{-- vendor js files --}}
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/ui/users-list-view.js')) }}"></script>
@endsection
