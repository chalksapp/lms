@extends('layouts/contentLayoutMaster')

@section('title', 'Roles')
@section('vendor-style')
        {{-- vendor files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('content')
  {{-- Nav Filled Starts --}}
  <section id="data-list-view" class="data-list-view-header">
      {{-- DataTable starts --}}
        <div class="table-responsive">
          <table class="table roles-list-view data-list-view">
            <thead>
              <tr>
                <th></th>
                <th>NAME</th>
                <th>ACTION</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                    <tr>
                      <td></td>
                      <td class="product-name"><a class="course_name_click" data-id="{{$role['id']}}" href="javascript:void(0)">{{ $role["name"] }}</a></td>
                      <td class="product-action">
                        <span class="action-edit" data-value="{{$role['id']}}"><i class="feather icon-edit"></i></span>
                        <span class="action-delete" data-value="{{$role['id']}}"><i class="feather icon-trash"></i></span>
                      </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      {{-- DataTable ends --}}
  </section>
  {{-- Nav Filled Ends --}}
@endsection
@section('vendor-script')
{{-- vendor js files --}}
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/ui/roles-list-view.js')) }}"></script>
@endsection
