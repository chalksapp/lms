@extends('layouts/contentLayoutMaster')

@section('title', 'Multiple Choice Questions')
@section('vendor-style')
        {{-- vendor files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('content')
  {{-- Nav Filled Starts --}}
  <section id="data-list-view" class="data-list-view-header">
      {{-- DataTable starts --}}
        <div class="table-responsive">
          <table class="table mcq-list-view data-list-view">
            <thead>
              <tr>
                <th></th>
                <th>Question Title</th>
                <th>Question</th>
                <th>Option 1</th>
                <th>Option 2</th>
                <th>Option 3</th>
                <th>Option 4</th>
                <th>Right Answer</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($mcq_questions as $mcq_question)
                    @if($mcq_question["mcq_ques_status"] === 1)
                      <?php $color = "success" ?>
                    @elseif($mcq_question["mcq_ques_status"] === 0)
                      <?php $color = "danger" ?>
                    @endif
                    <?php
                      $arr = array('Inactive', 'Active', 'info', 'warning', 'danger');
                    ?>
                    <tr>
                      <td></td>
                      <td class="product-name"><a class="course_name_click" data-id="{{$mcq_question['id']}}" href="javascript:void(0)">{{ $mcq_question["mcq_ques_title"] }}</a></td>
                      <td class="product-name">{{ $mcq_question["mcq_ques_text"] }}</td>
                      <td class="product-name">{{ $mcq_question["mcq_option1"] }}</td>
                      <td class="product-name">{{ $mcq_question["mcq_option2"] }}</td>
                      <td class="product-name">{{ $mcq_question["mcq_option3"] }}</td>
                      <td class="product-name">{{ $mcq_question["mcq_option4"] }}</td>
                      <td class="product-name">{{ $mcq_question["mcq_correct_ans"] }}</td>
                      <td>
                        <div class="chip chip-{{$color}}">
                          <div class="chip-body">
                            <div class="chip-text">{{ $arr[$mcq_question["mcq_ques_status"]]}}</div>
                          </div>
                        </div>
                      </td>
                      <td class="product-action">
                        <span class="action-view"><i class="feather icon-eye"></i></span>
                        <span class="action-edit" data-value="{{$mcq_question['id']}}"><i class="feather icon-edit"></i></span>
                        <span class="action-delete"><i class="feather icon-trash"></i></span>
                      </td>
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      {{-- DataTable ends --}}
  </section>
  {{-- Nav Filled Ends --}}
@endsection
@section('vendor-script')
{{-- vendor js files --}}
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/ui/mcq-list-view.js')) }}"></script>
@endsection
