@extends('layouts/contentLayoutMaster')

@section('title', 'Course Curriculum')
@section('vendor-style')
        <!-- vendor css files -->
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/dragula.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
@endsection
@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-ecommerce-shop.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/extensions/toastr.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/animate/animate.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('content')
 @if(!empty($this_course->curriculum[0]))
  <div class="alert alert-primary mb-2" role="alert">
    <strong>Hi!</strong> you can drag and rearrange the course contents. users will see contents in the same order
  </div>
    <!-- Wishlist Starts -->
  <section id="card-drag-area" class="grid-view wishlist-items">
      @foreach($this_course->curriculum as $curriculum)
          <div class="card ecommerce-card curriculum-card" data-content-id="{{$curriculum->content_id}}_{{$curriculum->content_type}}">
              <div class="card-content">
                  <a href="app-ecommerce-details">
                      <div class="item-img text-center">
                          @if(!empty($curriculum->course_content->video_image))
                              <img src="{{$asset_video_path}}{{$curriculum->content_id}}/{{$curriculum->course_content->video_image}}" class="img-fluid" alt="img-placeholder">
                          @else
                              <img src="{{$asset_test_path}}{{$curriculum->content_id}}/{{$curriculum->course_content->test_image}}" class="img-fluid" alt="img-placeholder">
                          @endif
                      </div>
                      <div class="card-body">
                          <div class="item-wrapper">
                              @if($curriculum->content_type == 1)
                                <div class="badge badge-success mr-1 mb-1">
                                  <i class="feather icon-video"></i>
                                  <span>Video Content</span>
                                </div>
                              @else
                                <div class="badge badge-success mr-1 mb-1">
                                  <i class="feather icon-file"></i>
                                  <span>Test Content</span>
                                </div>
                              @endif
                          </div>
                          <div class="item-name">
                          <span>{{$curriculum->course_content->video_title?$curriculum->course_content->video_title:$curriculum->course_content->test_title}}</span>
                          </div>
                          <div>
                            <p class="item-description">
                                {{$curriculum->course_content->video_desc?$curriculum->course_content->video_desc:$curriculum->course_content->test_desc}}
                            </p>
                          </div>
                      </div>
                  </a>
                  <div class="item-options text-center">
                      <div class="cart move-cart remove-from-curriculum" data-curriculum-id="{{$curriculum->content_id}}_{{$curriculum->content_type}}">
                      <i class="feather icon-x align-middle"></i> <span class="add-to-cart">Remove from Course</span>
                      </div>
                  </div>
              </div>
          </div>
      @endforeach
  </section>
<!-- Wishlist Ends -->
  @else
      {{-- Basic Alerts start --}}
          <section id="basic-alerts">
            <div class="row">
              <div class="col-xl-12 col-lg-12">
                <div class="card">
                  <div class="card-header">
                    <h4 class="card-title">No Contents Added For This Course</h4>
                  </div>
                  <div class="card-content">
                    <div class="card-body">
                      <p>
                        Contents can be added from video/test page. You can re arrange the order here.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
    {{-- Basic Alerts end --}}
  @endif
@endsection
@section('vendor-script')
        <!-- vendor files -->
        <script src="{{ asset(mix('vendors/js/extensions/dragula.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/pages/app-ecommerce-shop.js')) }}"></script>
    <script src="{{ asset(mix('js/scripts/ui/course-curriculum.js')) }}"></script>
    <script>
        var this_course_id = "{{$this_course_id}}";
    </script>
@endsection
