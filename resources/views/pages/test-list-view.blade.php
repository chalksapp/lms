@extends('layouts/contentLayoutMaster')

@section('title', 'Tests')
@section('vendor-style')
        {{-- vendor files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/extensions/dataTables.checkboxes.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/toastr.css')) }}">
@endsection
@section('page-style')
        {{-- Page css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/pages/data-list-view.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/extensions/sweetalert2.min.css')) }}">
@endsection
@section('content')
  {{-- Nav Filled Starts --}}
  <section id="data-list-view" class="data-list-view-header">
      {{-- DataTable starts --}}
        <div class="table-responsive">
          <table class="table test-list-view data-list-view">
            <thead>
              <tr>
                <th></th>
                <th>NAME</th>
                <th>DESCRIPTION</th>
                <th>STATUS</th>
                @canany(['edit test', 'delete test', 'view test'])
                    <th>ACTION</th>
                @endcanany
              </tr>
            </thead>
            <tbody>
                @foreach ($tests as $test)
                    @if($test["test_status"] === 1)
                      <?php $color = "success" ?>
                    @elseif($test["test_status"] === 2)
                      <?php $color = "warning" ?>
                    @endif
                    <?php
                      $arr = array('Inactive', 'Active', 'Inactive', 'warning', 'danger');
                    ?>
                    <tr>
                      <td></td>
                      <td class="product-name"><a class="course_name_click" data-id="{{$test['id']}}" href="javascript:void(0)">{{ $test["test_title"] }}</a></td>
                      <td class="product-name" data-original-title="Test Description" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="{{ $test['test_desc'] }}">{{ Str::limit($test["test_desc"],20) }}</td>
                      <td>
                        <div class="chip chip-{{$color}}">
                          <div class="chip-body">
                            <div class="chip-text">{{ $arr[$test["test_status"]]}}</div>
                          </div>
                        </div>
                      </td>
                      @canany(['edit test', 'delete test', 'view test'])
                          <td class="product-action">
                            @can('view test')
                                <span class="action-view"><i class="feather icon-eye"></i></span>
                            @endcan
                            @can('edit test')
                                <span class="action-edit" data-value="{{$test['id']}}"><i class="feather icon-edit"></i></span>
                            @endcan
                            @can('delete test')
                                <span class="action-delete" data-value="{{$test['id']}}"><i class="feather icon-trash"></i></span>
                            @endcan
                          </td>
                      @endcanany
                    </tr>
                @endforeach
            </tbody>
          </table>
        </div>
      {{-- DataTable ends --}}
  </section>
  {{-- Nav Filled Ends --}}
@endsection
@section('vendor-script')
{{-- vendor js files --}}
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.bootstrap.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.select.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jqBootstrapValidation.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/toastr.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/sweetalert2.all.min.js')) }}"></script>
@endsection
@section('page-script')
        {{-- Page js files --}}
        <script src="{{ asset(mix('js/scripts/ui/test-list-view.js')) }}"></script>
@endsection
