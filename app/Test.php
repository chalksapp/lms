<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CourseCurriculum;
use App\QuestionToTest;
class Test extends Model
{
    protected $table = 'tests';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'test_title','test_desc','total_questions','test_image'
    ];
    //defining relationship with course curriculum
    public function course_curriculum() {
        return $this->hasMany(CourseCurriculum::class,'content_id','id');
    }
    //defining relationship with questions to test
    public function test_to_questions() {
        return $this->hasMany(QuestionToTest::class, 'test_id', 'id');
    }
}
