<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Course;
use App\Video;
use App\Test;
use App\Classes;
class CourseCurriculum extends Model
{
    protected $table = 'course_curriculum';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'course_id','content_id','content_type','content_order'
    ];
    //defining inverse relationship with courses
    public function course() {
        return $this->belongsTo(Course::class,'course_id', 'id');
    }
    //defining inverse relationship with content
    public function course_content() {
        if($this->content_type == 1){
            return $this->belongsTo(Video::class,'content_id', 'id');
        }
        else if($this->content_type == 2){
            return $this->belongsTo(Test::class,'content_id', 'id');
        }
        else if($this->content_type == 3){
            return $this->belongsTo(Classes::class,'content_id', 'id');
        }
    }

    /**********************************For using in resource***************************************/
    public function video()
    {
        return $this->belongsTo(Video::class,'content_id', 'id');
    }

    public function test()
    {
        return $this->belongsTo(Test::class,'content_id', 'id');
    }
    /**********************************For using in resource***************************************/
}
