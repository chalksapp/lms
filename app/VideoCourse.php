<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoCourse extends Model
{
    protected $table = 'videos_courses';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_id','course_id'
    ];

}
