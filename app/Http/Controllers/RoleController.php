<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use File;
use App\Permission;
use Toastr;
class RoleController extends Controller
{
    // Get all roles
    public function getAllRoles(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"roles",'name'=>"Roles List"], ['name'=>"Roles View"]
        ];
        $roles  = Role::where('id', '!=', 1)->get();
        return view('/pages/roles-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'roles'   => $roles
        ]);
    }
    // Load role from given id
    public function loadThisRole(Request $request,$role_id = null){
        $get_role               = array();
        $assigned_permissions   = array();
        if($role_id == '1'){
            return redirect()->route('load-role');
        }
        if($role_id && $role_id != '1'){
            $get_role  = Role::where('id', '=', $role_id)->first();
            if($get_role->permissions){
                foreach($get_role->permissions as $sel_permission){
                    array_push($assigned_permissions,$sel_permission['id']);
                }
            }
        }
        $all_permissions      = Permission::all();
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"roles",'name'=>"Roles"], ['name'=>$get_role?$get_role['name']:'Create Role']
        ];
        return view('/pages/add-role-view', [
            'breadcrumbs' => $breadcrumbs,
            'role'        => $get_role,
            'permissions' => $all_permissions,
            'assigned_permissions' => $assigned_permissions
        ]);
    }
    //create/edit new Role data
    public function  createUpdateRole(Request $request){
        $added_role_id = '';
        $validator = $request->validate([
            'role_title'      => 'required'
        ]);       
        //if role id is already there, then update data instead insert
        if($request->input('hid_role_id')){
            $this_role_id          = $request->input('hid_role_id');
            $role                  = (new Role)->find($this_role_id);
            $role->name            = $request->input('role_title');
            $role->save();
            $added_role_id         = $request->input('hid_role_id');
            if($request->input('select_permissions')){
                $role->syncPermissions($request->input('select_permissions'));
            }
            $notification = array(
                'message'    => 'Role Updated successfully',
                'alert-type' => 'success'
            );
            $message         = 'Role Updated successfully';
            Toastr::success($message , 'Success', ["positionClass" => "toast-top-right"]);
            return redirect()->route('load-role',['id'=>$added_role_id])->with($notification);
        }
        else{
            $role_array_data = array(
                "name" => $request->input('role_title')
            ); 
            $role_result      = Role::create($role_array_data);
            $role             = Role::findById($role_result->id);
            if($request->input('select_permissions')){
                $role->syncPermissions($request->input('select_permissions'));
            }
            $added_role_id   = $role_result->id;
            $message         = 'Role created successfully';
            Toastr::success($message , 'Success', ["positionClass" => "toast-top-right"]);
            return redirect()->route('load-role',['id'=>$added_role_id]);
        }
        $notification = array(
            'message'    => 'Unexpected error occured! Try again',
            'alert-type' => 'error'
        );
        $message         = 'Unexpected error occured! Try again';
        Toastr::error($message , 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('load-role')->with($notification);
    }
    //to delete a role
    public function deleteRole(Request $request){
        $validator = $request->validate([
            'role_id'     => 'required'
        ]);
        $role    = Role::findById($request->role_id);
        $role->delete();
        $status  = 200;
        $message = 'Role Deleted succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
}
