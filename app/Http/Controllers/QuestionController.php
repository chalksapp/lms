<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use File;
use App\McqQuestion;
use App\DescQuestion;
use Toastr;
class QuestionController extends Controller
{
    // Get all MCQ Questions
    public function getAllMCQ(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"mcq-ques",'name'=>"MCQ Questions"]
        ];
        $mcq_questions   = McqQuestion::all();
        return view('/pages/mcq-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'mcq_questions'  => $mcq_questions
        ]);
    }
    // Load this mcq question from given id
    public function loadThisMCQ(Request $request,$mcq_id = null){
        $get_mcq = array();
        if($mcq_id){
            $get_mcq = McqQuestion::where('id', '=', $mcq_id)->first();
        }
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"mcq-ques",'name'=>"MCQ"], ['name'=>$get_mcq?$get_mcq['mcq_ques_title']:'Create an MCQ Question']
        ];
        return view('/pages/add-mcq-view', [
            'breadcrumbs' => $breadcrumbs,
            'get_mcq'        => $get_mcq
        ]);
    }
    // insert/update mcq question
    public function createNewMCQ(Request $request){
        $validator = $request->validate([
            'mcq_title'    => 'required',
            'mcq_ques'     => 'required',
            'mcq_option1'  => 'required',
            'mcq_option2'  => 'required',
            'mcq_option2'  => 'required',
            'mcq_option3'  => 'required',
            'mcq_option4'  => 'required',
            'mcq_correct'  => 'required',
            'mcq_full_marks'      => 'required',
            'mcq_negative_marks'  => 'required'
        ]);
        if($request->input('hid_test_id')){
            $this_mcq_id             = $request->input('hid_test_id');
            $mcq                     = (new McqQuestion)->find($this_mcq_id);
            $mcq->mcq_ques_title	   = $request->input('mcq_title');
            $mcq->mcq_ques_text      = $request->input('mcq_ques');
            $mcq->mcq_option1        = $request->input('mcq_option1');
            $mcq->mcq_option2        = $request->input('mcq_option2');
            $mcq->mcq_option3        = $request->input('mcq_option3');
            $mcq->mcq_option4        = $request->input('mcq_option4');
            $mcq->mcq_correct_ans    = $request->input('mcq_correct');
            $mcq->mcq_full_marks     = $request->input('mcq_full_marks');
            $mcq->mcq_negative_marks = $request->input('mcq_negative_marks');
            $mcq->save();
            $notification = array(
                'message'    => 'MCQ updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('load-mcq-ques',['id'=>$this_mcq_id])->with($notification);
        }
        else{
            $mcq_array_data = array(
                "mcq_ques_title"  => $request->input('mcq_title'),
                "mcq_ques_text"   => $request->input('mcq_ques'),
                "mcq_option1"     => $request->input('mcq_option1'),
                "mcq_option2"     => $request->input('mcq_option2'),
                "mcq_option3"     => $request->input('mcq_option3'),
                "mcq_option4"     => $request->input('mcq_option4'),
                "mcq_correct_ans" => $request->input('mcq_correct'),
                "mcq_full_marks"  => $request->input('mcq_full_marks'),
                "mcq_negative_marks" => $request->input('mcq_negative_marks')
            );
            $mcq_result     = McqQuestion::create($mcq_array_data);
            $notification = array(
                'message'    => 'MCQ created successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('load-mcq-ques',['id'=>$mcq_result->id])->with($notification);
        }
        $notification = array(
            'message'    => 'Some error oocured',
            'alert-type' => 'error'
        );
        return redirect()->route('mcq-ques')->with($notification);
    }
    // Get all Descriptive Questions
    public function getAllDescQuestions(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"desc-ques",'name'=>"Descriptive Questions"]
        ];
        $desc_questions   = DescQuestion::all();
        return view('/pages/desc-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'desc_questions'  => $desc_questions
        ]);
    }
    // Load this descriptive question from given id
    public function loadThisDesc(Request $request,$desc_id = null){
        $get_desc = array();
        if($desc_id){
            $get_desc = DescQuestion::where('id', '=', $desc_id)->first();
        }
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"desc-ques",'name'=>"Descriptive"], ['name'=>$get_desc?$get_desc['desc_ques_title']:'Create a Descriptive Question']
        ];
        return view('/pages/add-desc-view', [
            'breadcrumbs' => $breadcrumbs,
            'get_desc'    => $get_desc
        ]);
    }
    // insert/update descriptive question
    public function createNewDesc(Request $request){
        $validator = $request->validate([
            'desc_title'    => 'required',
            'desc_ques'     => 'required',
            'desc_ans'  => 'required',
            'desc_full_marks'  => 'required',
            'desc_negative_marks'  => 'required'
        ]);
        if($request->input('hid_desc_id')){
            $this_desc_id              = $request->input('hid_desc_id');
            $desc                      = (new DescQuestion)->find($this_desc_id);
            $desc->desc_ques_title	   = $request->input('desc_title');
            $desc->desc_ques_text      = $request->input('desc_ques');
            $desc->desc_answer_text    = $request->input('desc_ans');
            $desc->desc_full_marks	   = $request->input('desc_full_marks');
            $desc->desc_negative_marks = $request->input('desc_negative_marks');
            $desc->save();
            $notification = array(
                'message'    => 'Decsriptive Question updated successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('load-desc-ques',['id'=>$this_desc_id])->with($notification);
        }
        else{
            $desc_array_data = array(
                "desc_ques_title"     => $request->input('desc_title'),
                "desc_ques_text"      => $request->input('desc_ques'),
                "desc_answer_text"    => $request->input('desc_ans'),
                "desc_full_marks"     => $request->input('desc_full_marks'),
                "desc_negative_marks" => $request->input('desc_negative_marks')
            );
            $desc_result     = DescQuestion::create($desc_array_data);
            $notification    = array(
                'message'    => 'Decsriptive Question created successfully',
                'alert-type' => 'success'
            );
            return redirect()->route('load-desc-ques',['id'=>$desc_result->id])->with($notification);
        }
        $notification = array(
            'message'    => 'Some error oocured',
            'alert-type' => 'error'
        );
        return redirect()->route('desc-ques')->with($notification);
    }
}
