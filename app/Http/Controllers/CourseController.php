<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\CourseCurriculum;
use File;
use Carbon\Carbon;
use Toastr;
use App\CourseUser;
use App\LmsUser;
use App\Video;
use App\Test;
use App\Classes;
use App\Jobs\SendEmailClassUserJob;
class CourseController extends Controller
{
    // Get all courses
    public function getCourses(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"dashboard",'name'=>"Course List"], ['name'=>"Course View"]
        ];
        $courses = Course::all();
        return view('/pages/course-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'courses' => $courses
        ]);
    }
    //load course builder View
    public function courseBuilderView(Request $request, $course_id = null){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"courses",'name'=>"Courses"], ['name'=>"Course Builder"]
        ];
        $pageConfigs = [
            'bodyClass' => 'ecommerce-application',
        ];
        $video_selected   = array();
        $test_selected    = array();
        $class_selected   = array();
        $asset_video_path  = asset(env('UPLOADED_VIDEO_PATH')).'/';
        $asset_test_path   = asset(env('UPLOADED_TEST_PATH')).'/';
        $all_videos        = Video::select('id','video_title','video_url')->where('video_status', 1)->get();
        $all_tests         = Test::select('id','test_title')->where('test_status', 1)->get();
        $all_classes       = Classes::select('id','class_name')->where('class_status', 1)->get();
        $course_user_array = array();
        $all_users         = LmsUser::all('id','name','email');
        if($course_id){
            $added_users   = CourseUser::select('user_id')->where('course_id', $course_id)->get();
            foreach($added_users as $user){
                array_push($course_user_array,$user['user_id']);
            }
            $get_course       = Course::with('curriculum')->where('id', '=', $course_id)->first();
            if(!empty($get_course->curriculum)){
              foreach($get_course->curriculum as $curriculum){
                if($curriculum->content_type == 1){
                  array_push($video_selected,$curriculum->content_id);
                }
                if($curriculum->content_type == 2){
                  array_push($test_selected,$curriculum->content_id);
                }
                if($curriculum->content_type == 3){
                  array_push($class_selected,$curriculum->content_id);
                }
              }
            }
            if($get_course){
                return view('/pages/course-builder-view', [
                    'pageConfigs' => $pageConfigs,
                    'breadcrumbs' => $breadcrumbs,
                    'this_course' => $get_course,
                    'course_user_array' => $course_user_array,
                    'all_users'   => $all_users,
                    'all_videos'  => $all_videos,
                    'all_tests'  => $all_tests,
                    'all_classes'  => $all_classes,
                    'video_selected' => $video_selected,
                    'test_selected' => $test_selected,
                    'class_selected' => $class_selected,
                    'asset_video_path'  => $asset_video_path,
                    'asset_test_path'  => $asset_test_path,
                    'this_course_id'   => $course_id
                ]);
            }
            else{
               return redirect()->route('courses');
            }
        }
        else{
            return view('/pages/course-builder-view', [
                'pageConfigs' => $pageConfigs,
                'breadcrumbs' => $breadcrumbs,
                'this_course' => '',
                'course_user_array' => $course_user_array,
                'all_users'   => $all_users,
                'all_videos'  => $all_videos,
                'video_selected' => $video_selected,
                'test_selected' => $test_selected,
                'class_selected' => $class_selected,
                'all_tests'  => $all_tests,
                'all_classes'  => $all_classes,
                'asset_video_path'  => $asset_video_path,
                'asset_test_path'  => $asset_test_path
            ]);
        }
    }
    //save users to a Course
    public function saveCourseUsers(Request $request){
        $validator = $request->validate([
            'hidden_course_id'    => 'required'
        ]);
        $course_data = array();
        //delete all users for this course before update
        CourseUser::where('course_id',$request->input('hidden_course_id'))->delete();
        $this_course_data  = Course::where('id',$request->input('hidden_course_id'))->get();
        //echo '<pre>'; print_r($this_course_data['course_name']); die;
        $current_course_id = $request->input('hidden_course_id');
        if(!empty($request->input('course_select_users'))){
          foreach($request->input('course_select_users') as $user_value) {
              $data = array('course_id' => $request->input('hidden_course_id'), 'user_id' => $user_value);
              CourseUser::insert($data);
              $get_this_user                 = LmsUser::where('id', '=', $user_value)->first();
              $course_data['user_name']      = $get_this_user['name'];
              $course_data['to_user']        = $get_this_user['email'];
              $course_data['course_name']    = $this_course_data[0]->course_name;
              //process mail queue
              dispatch(new SendEmailClassUserJob($course_data));
          }
        }
        $message = 'Users updated successfully';
        Toastr::success($message , 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('course-builder',[$current_course_id,3]);
    }
    //save all course contents
    public function saveCourseContent(Request $request){
        $validator = $request->validate([
            'hidden_curriculum_course_id'    => 'required'
        ]);
        $current_course_id = $request->input('hidden_curriculum_course_id');
        //delete all course contents before starts
        CourseCurriculum::where('course_id',$request->input('hidden_curriculum_course_id'))->delete();
        //add video contents
        if(!empty($request->input('add_video_to_course'))){
          foreach($request->input('add_video_to_course') as $video_value) {
              $data = array('course_id' => $request->input('hidden_curriculum_course_id'), 'content_id' => $video_value, 'content_type' => 1);
              CourseCurriculum::insert($data);
          }
        }
        //add tests contents
        if(!empty($request->input('add_test_to_course'))){
          foreach($request->input('add_test_to_course') as $test_value) {
              $data = array('course_id' => $request->input('hidden_curriculum_course_id'), 'content_id' => $test_value, 'content_type' => 2);
              CourseCurriculum::insert($data);
          }
        }
        //add class contents
        if(!empty($request->input('add_class_to_course'))){
          foreach($request->input('add_class_to_course') as $class_value) {
              $data = array('course_id' => $request->input('hidden_curriculum_course_id'), 'content_id' => $class_value, 'content_type' => 3);
              CourseCurriculum::insert($data);
          }
        }
        $message = 'Course Contents updated successfully';
        Toastr::success($message , 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('course-builder',[$current_course_id,2]);
    }
    //load course curriculum view pages
    public function courseCurriculumView(Request $request, $course_id = null){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"courses",'name'=>"Courses"], ['name'=>"Course Curriculum"]
        ];
        $pageConfigs = [
            'bodyClass' => 'ecommerce-application',
        ];
        if($course_id){
            $get_course = Course::with('curriculum')->where('id', '=', $course_id)->first();
            if($get_course){
                $asset_video_path = asset(env('UPLOADED_VIDEO_PATH')).'/';
                $asset_test_path  = asset(env('UPLOADED_TEST_PATH')).'/';
                return view('/pages/course-curriculum-view', [
                    'pageConfigs' => $pageConfigs,
                    'breadcrumbs' => $breadcrumbs,
                    'this_course' => $get_course,
                    'asset_video_path'  => $asset_video_path,
                    'asset_test_path'  => $asset_test_path,
                    'this_course_id'   => $course_id
                ]);
            }
            else{
               return redirect()->route('courses');
            }
        }
    }
    //Save curriculum order
    public function saveCurriculumOrder(Request $request){
        $validator = $request->validate([
            'course_id'     => 'required',
            'order_array'   => 'required'
        ]);
        CourseCurriculum::where('course_id',$request->course_id)->delete();
        //looping over curriculum order value
        if(!empty($request['order_array'])){
            $order_array = json_decode($request['order_array'],true);
            $order_counter = 1;
            foreach($order_array as $order_value){
                $order_exploded = explode('_',$order_value);
                $data = array('content_order' => $order_counter,'course_id' => $request['course_id'], 'content_id' => $order_exploded[0],'content_type' => $order_exploded[1]);
                CourseCurriculum::insert($data);
                $order_counter++;
            }
            $status  = 200;
            $message = 'Order Updated succesfully';
            return response()->json(compact('status', 'message'), 200);
        }
    }
    //Delete curriculum from course
    public function deleteCurriculum(Request $request){
        $validator = $request->validate([
            'course_id'     => 'required',
            'this_curriculum_id' => 'required'
        ]);
        $curriculum_exploded = explode('_',$request->this_curriculum_id);
        CourseCurriculum::where('course_id',$request->course_id)->where('content_id',$curriculum_exploded[0])->where('content_type',$curriculum_exploded[1])->delete();
        $status  = 200;
        $message = 'Removed succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
    // Save course data
    public function saveCourseData(Request $request){
        $current_course_id = '';
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"dashboard",'name'=>"Course List"], ['name'=>"Course View"]
        ];
        if($request->input('hid_course_id')){
            $validator = $request->validate([
                'course_name'     => 'required',
                'course_desc'     => 'required|min:10',
                'course_date'     => 'required',
                'course_fee'      => 'required',
                'course_level'    => 'required',
                'course_duration' => 'required',
                'course_period'   => 'required'
            ]);
        }
        else{
            $validator = $request->validate([
                'course_name'     => 'required',
                'course_desc'     => 'required|min:10',
                'course_date'     => 'required',
                'course_fee'      => 'required',
                'course_level'    => 'required',
                'course_duration' => 'required',
                'course_period'   => 'required',
                'course_schedule' => 'required|mimes:pdf',
                'course_image'    => 'required|image',
            ]);
        }
        $course_array_data = array(
            "course_name" => $request->input('course_name'),
            "course_desc" => $request->input('course_desc'),
            "course_start_date" => Carbon::parse($request->input('course_date'))->format('Y-m-d'),
            "course_fees" => $request->input('course_fee'),
            "course_level" => $request->input('course_level'),
            "course_duration" => $request->input('course_duration'),
            "course_period" => $request->input('course_period')
        );
        //if course id is already there, then update data
        if($request->input('hid_course_id')){
            $course_update   = Course::where('id', $request->input('hid_course_id'))->update($course_array_data);
            $course_get_data = Course::select('course_schedule','course_image')->where('id', $request->input('hid_course_id'))->get();
            $added_course_id = $request->input('hid_course_id');
            $pdf_name        = $course_get_data[0]->course_schedule;
            $image_name      = $course_get_data[0]->course_image;
            $current_course_id = $added_course_id;
        } else {
            $course_result     = Course::create($course_array_data);
            $added_course_id   = $course_result->id;
            $current_course_id = $added_course_id;
        }
        $destinationPath = public_path().'/'.env("UPLOADED_COURSE_PATH").$added_course_id.'/';
        if($request->hasFile('course_schedule')) {
           $validator = $request->validate([
                'course_schedule' => 'required|mimes:pdf'
           ]);
           $pdf_file = $request->file('course_schedule');
           //you also need to keep file extension as well
           $pdf_name = $pdf_file->getClientOriginalName();
           //using the array instead of object
           $pdf['filePath'] = $pdf_name;
           if (!File::exists($destinationPath)) {
              // path does not exist
              File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
           $pdf_file->move($destinationPath, $pdf_name);
        }
        if($request->hasFile('course_image')) {
           $validator = $request->validate([
                'course_image'    => 'required|image'
           ]);
           $image_file  = $request->file('course_image');
           //you also need to keep file extension as well
           $image_name  = $image_file->getClientOriginalName();
           //using the array instead of object
           $image['filePath'] = $image_name;
           if (!File::exists($destinationPath)) {
            // path does not exist
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
           $image_file->move($destinationPath, $image_name);
        }
        if($request->hasFile('course_image') || $request->hasFile('course_schedule')) {
           $course = (new Course)->find($added_course_id);
           $course->course_schedule = $pdf_name;
           $course->course_image    = $image_name;
           $course->save();
        }
        $status  = 200;
        if($request->input('hid_course_id')){
            $message = 'Course Updated succesfully';
        }
        else{
            $message = 'Course added succesfully';
        }
        Toastr::success($message , 'Success', ["positionClass" => "toast-top-right"]);
        return redirect()->route('course-builder',[$current_course_id]);
    }
    //to delete course data
    public function deleteCourseData(Request $request){
        $validator = $request->validate([
            'course_id'     => 'required'
        ]);
        Course::where('id',$request->course_id)->delete();
        $status  = 200;
        $message = 'Course Deleted succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
}
