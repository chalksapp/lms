<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Classes;
use App\LmsUser;
use App\ClassUser;
use File;
use DateTime;
use DateTimeZone;
use Carbon\Carbon;
use App\Jobs\SendEmailClassUserJob;
class ClassController extends Controller
{
    // Get all classes
    public function getClasses(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"classes",'name'=>"Classes List"], ['name'=>"Classes View"]
        ];
        $classes = Classes::all();
        return view('/pages/classes-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'classes' => $classes
        ]);
    }
    // load create class view page
    public function addNewClassView(Request $request,$class_id = null){
        $get_class         = array();
        $course_user_array = array();
        $all_users         = LmsUser::all('id','name','email');
        //check if class id is present
        if($class_id){
            $added_users   = ClassUser::select('user_id')->where('class_id', $class_id)->get();
            foreach($added_users as $user){
                array_push($course_user_array,$user['user_id']);
            }
            $get_class                    = Classes::where('id', '=', $class_id)->first();
            $get_class->class_date_time   = Carbon::parse($get_class->class_date_time)->format('Y-m-d\TH:i');
        }
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"classes",'name'=>"Classes"], ['name'=>"Add New Class"]
        ];
        return view('/pages/add-class-view', [
            'breadcrumbs' => $breadcrumbs,
            'class'       => $get_class,
            'course_user_array' => $course_user_array,
            'all_users'   => $all_users
        ]);
    }
    // save class data to database
    public function saveClassData(Request $request){
        $validator = $request->validate([
            'class_name'   => 'required',
            'class_desc'   => 'required',
            'class_datetime'   => 'required',
            'class_duration'   => 'required',
            'class_join_url'   => 'required',
            'class_join_pwd'   => 'required',
            'class_select_users' => 'required'
        ]);
        $class_array_data = array(
            "class_name" => $request->input('class_name'),
            "class_description" => $request->input('class_desc'),
            "class_date_time" => $request->input('class_datetime'),
            "class_duration" => $request->input('class_duration'),
            "class_join_url" => $request->input('class_join_url'),
            "class_join_password" => $request->input('class_join_pwd')
        );
        //check whether this is an update function
        if($request->input('hid_class_id')){
            //delete all users for this class before update
            ClassUser::where('class_id',$request->input('hid_class_id'))->delete();
            $class_update   = Classes::where('id', $request->input('hid_class_id'))->update($class_array_data);
            $notification = array(
                'message' => 'Class updated successfully',
                'alert-type' => 'success'
            );
            //manage - add user to a class
            if($request->input('class_select_users')){
                foreach($request->input('class_select_users') as $user_value) {
                    $data = array('class_id' => $request->input('hid_class_id'), 'user_id' => $user_value);
                    ClassUser::insert($data);
                    $get_this_user                      = LmsUser::where('id', '=', $user_value)->first();
                    $class_array_data['formatted_time'] = $this->gmdate_to_mydate($class_array_data['class_date_time'],1);
                    $class_array_data['user_name']      = $get_this_user['user_name'];
                    $class_array_data['to_user']        = $get_this_user['user_email'];
                    //process mail queue
                    dispatch(new SendEmailClassUserJob($class_array_data));
                }
            }
            return redirect()->route('classes')->with($notification);
        }
        else{
            $class_result   = Classes::create($class_array_data);
            //manage - add user to a class
            if($request->input('class_select_users')){
                foreach($request->input('class_select_users') as $user_value) {
                    $data = array('class_id' => $class_result->id, 'user_id' => $user_value);
                    ClassUser::insert($data);
                    $get_this_user                      = LmsUser::where('id', '=', $user_value)->first();
                    $class_array_data['formatted_time'] = $this->gmdate_to_mydate($class_array_data['class_date_time'],1);
                    $class_array_data['user_name']      = $get_this_user['user_name'];
                    $class_array_data['to_user']        = $get_this_user['user_email'];
                    //process mail queue
                    dispatch(new SendEmailClassUserJob($class_array_data));
                }
            }
            if($class_result){
                $notification = array(
                    'message' => 'Class added successfully',
                    'alert-type' => 'success'
                );
                return redirect()->route('classes')->with($notification);
            }
        }
    }
    /**
     * format date-time baded on local timezone
     *
     * @return datetime
    */
    public function gmdate_to_mydate($gmdate,$type){
        /* $gmdate must be in YYYY-mm-dd H:i:s format*/
        $userTimezone = new DateTimeZone('Asia/Kolkata');
        $gmtTimezone  = new DateTimeZone('GMT');
        $myDateTime   = new DateTime($gmdate, $gmtTimezone);
        $offset       = $userTimezone->getOffset($myDateTime);
        if($type == 1){
            return date("d-m-Y g:iA", strtotime($gmdate)+$offset);
        }
        else{
            return date("Y-m-d H:i:s", strtotime($gmdate)+$offset);
        }
    }
    //to delete a class
    public function deleteClass(Request $request){
        $validator = $request->validate([
            'class_id'     => 'required'
        ]);
        Classes::where('id',$request->class_id)->delete();
        $status  = 200;
        $message = 'Class Deleted succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
}
