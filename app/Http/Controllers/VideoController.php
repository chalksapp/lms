<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Video;
use App\CourseCurriculum;
use App\Course;
use File;
use Carbon\Carbon;
class VideoController extends Controller
{
    // Get all videos
    public function getVideos(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"videos",'name'=>"Videos List"], ['name'=>"Video View"]
        ];
        $videos = Video::all();
        return view('/pages/video-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'videos' => $videos
        ]);
    }

    // Save video data
    public function saveVideoData(Request $request){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"dashboard",'name'=>"Video List"], ['name'=>"Video View"]
        ];
        if($request->input('hid_video_id')){
            $validator = $request->validate([
                'video_title'     => 'required',
                'video_desc'     => 'required|min:10',
                'video_url'     => 'required|url'
            ]);
        }
        else{
            $validator = $request->validate([
                'video_title'     => 'required',
                'video_desc'     => 'required|min:10',
                'video_url'     => 'required|url',
                'video_image'    => 'required|image',
            ]);
        }
        $video_array_data = array(
            "video_title" => $request->input('video_title'),
            "video_desc" => $request->input('video_desc'),
            "video_url" => $request->input('video_url')
        );
        //if video id is already there, then update data
        if($request->input('hid_video_id')){
            $video_update    = Video::where('id', $request->input('hid_video_id'))->update($video_array_data);
            $video_get_data  = Video::select('video_image')->where('id', $request->input('hid_video_id'))->get();
            $added_video_id  = $request->input('hid_video_id');
            $image_name      = $video_get_data[0]->video_image;
        } else {
            $video_result    = Video::create($video_array_data);
            $added_video_id  = $video_result->id;
        }
        $destinationPath = public_path().'/'.env("UPLOADED_VIDEO_PATH").$added_video_id.'/';
        if($request->hasFile('video_image')) {
           $validator = $request->validate([
                'video_image'    => 'required|image'
           ]);
           $image_file  = $request->file('video_image');
           //you also need to keep file extension as well
           $image_name  = $image_file->getClientOriginalName();
           //using the array instead of object
           $image['filePath'] = $image_name;
           if (!File::exists($destinationPath)) {
            // path does not exist
                File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
           $image_file->move($destinationPath, $image_name);
        }
        if($request->hasFile('video_image')) {
           $video = (new Video)->find($added_video_id);
           $video->video_image  = $image_name;
           $video->save();
        }
        $status  = 200;
        if($request->input('hid_video_id')){
            $message = 'Video Updated succesfully';
        }
        else{
            $message = 'Video added succesfully';
        }
        return response()->json(compact('status', 'message'), 200);
    }
    //to delete video data
    public function deleteVideoData(Request $request){
        $validator = $request->validate([
            'video_id'     => 'required'
        ]);
        Video::where('id',$request->video_id)->delete();
        CourseCurriculum::where('content_id',$request->video_id)->where('content_type',1)->delete();
        $status  = 200;
        $message = 'Video Deleted succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
    //to get added courses to video
    public function getVideoCourses(Request $request){
        $validator = $request->validate([
            'video_id'     => 'required'
        ]);
        $courses         = Course::all('id','course_name');
        $added_course_id = array();
        $added_courses   = CourseCurriculum::select('course_id')->where('content_id', $request->video_id)->where('content_type', 1)->get();
        foreach($added_courses as $course){
            array_push($added_course_id,$course['course_id']);
        }
        $status          = 200;
        return response()->json(compact('status', 'added_course_id', 'courses'), 200);
    }
    //to add video to courses
    public function addVideoToCourse(Request $request){
        $validator = $request->validate([
            'video_id'     => 'required'
        ]);
        CourseCurriculum::where('content_id',$request->video_id)->where('content_type',1)->delete();
        if($request->sel_course_id){
            foreach($request->sel_course_id as $course_value) {
                $data = array('content_id' => $request->video_id, 'content_type' => 1, 'course_id' => $course_value);
                CourseCurriculum::insert($data);    
            }
        }
        $status  = 200;
        $message = 'Added succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
}
