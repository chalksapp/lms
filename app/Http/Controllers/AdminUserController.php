<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use File;
use Carbon\Carbon;
use Hash;
class AdminUserController extends Controller
{
    // Get all admin users
    public function getAllUsers(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"admin-users",'name'=>"Admin Users List"], ['name'=>"Admin User View"]
        ];
        $users = User::where('id', '!=', 1)->get();
        return view('/pages/admin-users-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'users' => $users
        ]);
    }
     // load /create user view page
    public function addNewUserView(Request $request,$user_id = null){
        $get_user       = array();
        $assigned_roles = array();
        if($user_id && $user_id != '1'){
            $get_user          = User::with('roles')->where('id', '=', $user_id)->first();
            if($get_user->roles){
                foreach($get_user->roles as $assign_roles){
                    array_push($assigned_roles,$assign_roles['id']);
                }
            }
        }
        $breadcrumbs  = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"users",'name'=>"Users"], ['name'=>"Add New User"]
        ];
        $all_roles = Role::all();
        return view('/pages/add-admin-user-view', [
            'breadcrumbs' => $breadcrumbs,
            'user'        => $get_user,
            'all_roles'   => $all_roles,
            'assigned_roles' => $assigned_roles
        ]);
    }
    // save user data to database
    public function saveUserData(Request $request){
        $validator = $request->validate([
            'user_name'   => 'required',
            'user_email'  => 'required|email'
        ]);
        $user_array_data = array(
            "name" => $request->input('user_name'),
            "email" => $request->input('user_email')
        );
        if($request->input('hid_user_id')){
            $get_user        = User::where('email', '=', $request->input('user_email'))->where('id', '!=', $request->input('hid_user_id'))->first();
            if($get_user){
                $notification = array(
                    'message'    => 'This Email id already added for another user',
                    'alert-type' => 'error'
                );
                return redirect()->route('admin-users')->with($notification);
            }
            else{
                $user_update   = User::where('id', $request->input('hid_user_id'))->update($user_array_data);
                \Mail::to($user_array_data['email'])->send(new \App\Mail\SendMailAdminUser($user_array_data));
                $notification = array(
                    'message' => 'User updated successfully',
                    'alert-type' => 'success'
                );
                if($request->input('select_roles')){
                    $sync_user = User::where('id', $request->input('hid_user_id'))->first();
                    $sync_user->syncRoles($request->input('select_roles'));
                }
                return redirect()->route('admin-users')->with($notification);
            }
        }
        else{
            if (User::where('email', '=', $request->input('user_email'))->count() > 0) {
                $notification = array(
                    'message'    => 'This Email id already exists',
                    'alert-type' => 'error'
                );
                return redirect()->route('admin-users')->with($notification);
            }
            else{
                $user_array_data['password'] = Hash::make('123456ab');
                $user_result   = User::create($user_array_data);
                //send mail to User
                \Mail::to($user_array_data['email'])->send(new \App\Mail\SendMailAdminUser($user_array_data));
                if($user_result){
                    $notification = array(
                        'message' => 'User added successfully',
                        'alert-type' => 'success'
                    );
                    if($request->input('select_roles')){
                        $sync_user = User::where('id', $user_result->id)->first();
                        $sync_user->syncRoles($request->input('select_roles'));
                    }
                    return redirect()->route('admin-users')->with($notification);
                }
            }
        }
    }
    //to delete an admin user
    public function deleteAdminUser(Request $request){
        $validator = $request->validate([
            'admin_id'     => 'required'
        ]);
        $get_user = User::with('roles')->where('id', '=', $request->admin_id)->first();
        User::where('id',$request->admin_id)->delete();
        $get_user->syncRoles([]);
        $status   = 200;
        $message  = 'User Deleted succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
}
