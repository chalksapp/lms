<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\LmsUser;
use Illuminate\Http\Request;

class LmsUserAuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function completeRegistration(Request $request, $encryptedString)
    {
        $userEmail = base64_decode($encryptedString);
        $user = LmsUser::where('email', $userEmail)->first();
        if ($user) {
            return view('frontend.auth.complete_registration')->with([
                'user' => $user
            ]);
        }
        return redirect()->route('lms_user.login_page');
    }

    public function dashboard(Request $request)
    {
        $this->middleware('auth:lms_user');
        return view('frontend.dashboard');
    }
}
