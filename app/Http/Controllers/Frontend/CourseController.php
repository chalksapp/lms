<?php

namespace App\Http\Controllers\Frontend;

use App\Course;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\CourseResource;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        return view('/frontend/courses/courses-page');
    }

    public function getAllCourses(Request $request)
    {
        $user_id = Auth::user()->id;
        $courses = Course::query();
        if ($request->search_term) {
            $courses->where('course_name', 'like', '%' . $request->search_term . '%');
        }
        $courses->with(['users'])
            ->whereHas('users', function($q) use($user_id) {
                $q->where('user_id', $user_id);
            });
        $coursesCollection = $courses->paginate(config('frontend.PAGINATION_LIMIT'));

        if ($request->ajax()) {
            return view('/frontend/courses/all-courses', [
                'courses' => $coursesCollection
            ])->render();
        }
        return view('/frontend/courses/all-courses', [
            'courses' => $coursesCollection
        ])->render();
    }

    public function courseDetailsPage(Request $request, $course)
    {
        $course = Course::findOrFail(base64_decode($course));
        $courseData = new CourseResource($course->load('curriculum', 'users'));
        return view('frontend.courses.course-details', [
            'courseData' => json_decode(json_encode($courseData))
        ]);
    }

    public function showLessons(Request $request, $course)
    {
        return view('/frontend/courses/lessons/index');
    }
}
