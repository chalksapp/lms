<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/fe/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:lms_user')->except('logout');
    }
    //click login user
    public function authenticate(Request $request)
    {
        $validator = $request->validate([
            'email'     => 'required|email',
            'password'  => 'required|min:6'
        ]);
        if (Auth::attempt($validator)) {
            return redirect()->route('lms_user.dashboard');
        }
        else{
            return redirect()->back()->with('error', 'These credentials doesnot match our records');
        }
    }


    // Login
    public function showLoginForm(){
        return view('frontend.auth.login');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect()->route('lms_user.login_page');
    }

    protected function guard(){
        return Auth::guard('lms_user');
    }
}
