<?php

namespace App\Http\Controllers\Frontend;
use App\Test;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        $all_tests     = Test::where('test_category',1)->where('test_status',1)->get();
        if(!empty($all_tests)){
            $test_image_path = asset('test_files');
        }
        return view('frontend.tests.all-tests', [
            'test_image_path' => $test_image_path,
            'testData' => $all_tests
        ]);
    }
}
