<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\LmsUser;
use File;
use Carbon\Carbon;

class UserController extends Controller
{
    // Get all users
    public function getUsers(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"users",'name'=>"Users List"], ['name'=>"User View"]
        ];
        $users = LmsUser::all();
        return view('/pages/users-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'users' => $users
        ]);
    }
    // load create user view page
    public function addNewUserView(Request $request,$user_id = null){
        $get_user = array();
        if($user_id){
            $get_user = LmsUser::where('id', '=', $user_id)->first();
        }
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"users",'name'=>"Users"], ['name'=>"Add New User"]
        ];
        return view('/pages/add-user-view', [
            'breadcrumbs' => $breadcrumbs,
            'user'        => $get_user
        ]);
    }
    // save user data to database
    public function saveUserData(Request $request){
        $validator = $request->validate([
            'user_name'   => 'required',
            'user_email'  => 'required|email'
        ]);
        $user_array_data = array(
            "name" => $request->input('user_name'),
            "email" => $request->input('user_email')
        );
        if($request->input('hid_user_id')){
            $get_user        = LmsUser::where('email', '=', $request->input('user_email'))->where('id', '!=', $request->input('hid_user_id'))->first();
            if($get_user){
                $notification = array(
                    'message'    => 'This Email id already added for another user',
                    'alert-type' => 'error'
                );
                return redirect()->route('users')->with($notification);
            }
            else{
                $user_update   = LmsUser::where('id', $request->input('hid_user_id'))->update($user_array_data);
                \Mail::to($user_array_data['email'])->send(new \App\Mail\AddStudentMail($user_array_data));
                $notification = array(
                    'message' => 'User updated successfully',
                    'alert-type' => 'success'
                );
                return redirect()->route('users')->with($notification);
            }
        }
        else{
            if (LmsUser::where('email', '=', $request->input('user_email'))->count() > 0) {
                $notification = array(
                    'message'    => 'This Email id already exists',
                    'alert-type' => 'error'
                );
                return redirect()->route('users')->with($notification);
            }
            else{
                $user_result   = LmsUser::create($user_array_data);
                //send mail to User
                \Mail::to($user_array_data['email'])->send(new \App\Mail\AddStudentMail($user_array_data));
                if($user_result){
                    $notification = array(
                        'message' => 'User added successfully',
                        'alert-type' => 'success'
                    );
                    return redirect()->route('users')->with($notification);
                }
            }
        }
    }
    //to delete a user
    public function deleteUser(Request $request){
        $validator = $request->validate([
            'user_id'     => 'required'
        ]);
        LmsUser::where('id',$request->user_id)->delete();
        $status  = 200;
        $message = 'User Deleted succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
}
