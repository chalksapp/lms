<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;
use File;
use App\QuestionToTest;
use Toastr;
use App\McqQuestion;
use App\DescQuestion;
class TestController extends Controller
{
    // Get all tests
    public function getAllTests(){
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"tests",'name'=>"Test List"], ['name'=>"Test View"]
        ];
        $tests      = Test::all();
        return view('/pages/test-list-view', [
            'breadcrumbs' => $breadcrumbs,
            'tests'   => $tests
        ]);
    }
    // Load test from given id
    public function loadThisTest(Request $request,$test_id = null){
        $get_test             = array();
        $mcq_selected         = array();
            $desc_selected    = array();
        if($test_id){
            $get_test         = Test::with('test_to_questions')->where('id', '=', $test_id)->first();
            if(!empty($get_test->test_to_questions)){
              foreach($get_test->test_to_questions as $test_ques){
                if($test_ques->question_type == 1){
                  array_push($mcq_selected,$test_ques->question_id);
                }
                if($test_ques->question_type == 2){
                  array_push($desc_selected,$test_ques->question_id);
                }
              }
            }
        }
        $all_mcqs = McqQuestion::all();
        $all_desc = DescQuestion::all();
        $breadcrumbs = [
            ['link'=>"dashboard",'name'=>"Home"],['link'=>"tests",'name'=>"Tests"], ['name'=>$get_test?$get_test['test_title']:'Create Test']
        ];
        return view('/pages/add-test-view', [
            'breadcrumbs' => $breadcrumbs,
            'test'        => $get_test,
            'all_mcqs'    => $all_mcqs,
            'all_desc'    => $all_desc,
            'mcq_selected' => $mcq_selected,
            'desc_selected' => $desc_selected
        ]);
    }
    //create/edit new Test data
    public function  createNewTest(Request $request){
        $added_test_id = '';
        $validator = $request->validate([
            'test_title'      => 'required',
            'test_desc'       => 'required|min:10',
            'test_category'   => 'required',
            'test_status'   => 'required',
            'test_ques_count' => 'required'
        ]);
        $test_array_data = array(
            "test_title" => $request->input('test_title'),
            "test_desc"  => $request->input('test_desc'),
            "test_category" => $request->input('test_category'),
            "test_status"   => $request->input('test_status'),
            "total_questions" => $request->input('test_ques_count')
        );
        //if test id is already there, then update data instead insert
        if($request->input('hid_test_id')){
            $this_test_id          = $request->input('hid_test_id');
            $test                  = (new Test)->find($this_test_id);
            $test->test_title      = $request->input('test_title');
            $test->test_desc       = $request->input('test_desc');
            $test->test_category   = $request->input('test_category');
            $test->test_status     = $request->input('test_status');
            $test->total_questions = $request->input('test_ques_count');
            $test->save();
            $added_test_id         = $request->input('hid_test_id');
            //delete all the questions added for the test at first
            QuestionToTest::where('test_id',$this_test_id)->delete();
        }
        else{
            $test_result     = Test::create($test_array_data);
            $added_test_id   = $test_result->id;
        }
        if($added_test_id) {
            //delete all the questions added for the test at first
            QuestionToTest::where('test_id',$added_test_id)->delete();
            $destinationPath = public_path().'/'.env("UPLOADED_TEST_PATH").$added_test_id.'/';
            if($request->file('test_thumbnail')){
                $image_file      = $request->file('test_thumbnail');
                //you also need to keep file extension as well
                $image_name      = $image_file->getClientOriginalName();
                if (!File::exists($destinationPath)) {
                // path does not exist
                    File::makeDirectory($destinationPath, $mode = 0777, true, true);
                }
                $image_file->move($destinationPath, $image_name);
                $test = (new Test)->find($added_test_id);
                $test->test_image  = $image_name;
                $test->save();
            }
            $notification = array(
                'message'    => 'Test saved successfully',
                'alert-type' => 'success'
            );
            if($request->input('select_questions')){
                $all_questions = $request->input('select_questions');
                $test_id       = $added_test_id?$added_test_id:$request->input('hid_test_id');
                foreach($all_questions as $question){
                    $ques_exploded = explode('_',$question);
                    $data = array('test_id' => $test_id,'question_id' => $ques_exploded[0], 'question_type' => $ques_exploded[1]);
                    QuestionToTest::insert($data);
                }
            }
            $message = 'Test saved successfully';
            Toastr::success($message , 'Success', ["positionClass" => "toast-top-right"]);
            return redirect()->route('load-test',['id'=>$added_test_id])->with($notification);
        }
        $notification = array(
            'message'    => 'Test created successfully',
            'alert-type' => 'error'
        );
        return redirect()->route('load-test')->with($notification);
    }
    //to delete a test
    public function deleteTest(Request $request){
        $validator = $request->validate([
            'test_id'     => 'required'
        ]);
        Test::where('id',$request->test_id)->delete();
        $status  = 200;
        $message = 'Test Deleted succesfully';
        return response()->json(compact('status', 'message'), 200);
    }
}
