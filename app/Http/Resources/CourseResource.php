<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    //to remove outer `data` wrapping in JsonResponse
    public static $wrap = '';

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->course_name,
            'description' => $this->course_desc,
            'course_start_date' => $this->course_start_date,
            'course_fees' => $this->course_fees,
            'course_level' => $this->course_level,
            'course_duration' => $this->course_duration,
            'course_period' => $this->course_period,
            'course_schedule' => $this->course_schedule,
            'course_image' => $this->course_image,
            'course_status' => $this->course_status,
            'curriculum' => $this->whenLoaded('curriculum', CurriculumResourceCollection::make($this->curriculum)),
            'users' => $this->whenLoaded('users', $this->users),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
