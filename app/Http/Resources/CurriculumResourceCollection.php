<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CurriculumResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    //to remove outer `data` wrapping in JsonResponse
    public static $wrap = '';
    public function toArray($request)
    {
        return CurriculumResource::collection($this->collection);
    }
}
