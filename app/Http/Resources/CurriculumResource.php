<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurriculumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->when($this->content_type == 1, function (){
                return 'video';
            }, 'test'),
            'content' => $this->when($this->content_type == 1, function (){
                return $this->video;
            }, $this->test)
        ];
    }
}
