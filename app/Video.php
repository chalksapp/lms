<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_title','video_desc','video_url','video_image'
    ];

    //defining relationship with course curriculum
    public function course_curriculum() {
        return $this->hasMany(CourseCurriculum::class,'content_id','id');
    }
}
