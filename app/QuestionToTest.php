<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Test;
class QuestionToTest extends Model
{
    protected $table = 'questions_to_test';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'test_id','question_id','question_type'
    ];
    //defining relationship with questions to test
    public function questions_to_text() {
        return $this->belongsTo(Test::class,'test_id', 'id');
    }
}
