<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DescQuestion extends Model
{
    protected $table = 'desc_questions';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'desc_ques_title','desc_ques_text','desc_answer_text','desc_full_marks','desc_negative_marks','desc_ques_status'
    ];
}
