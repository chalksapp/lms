<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table = 'classes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "class_name","class_description","class_date_time","class_duration","class_join_url","class_join_password"
    ];
    //defining relationship with course curriculum
    public function course_curriculum() {
        return $this->hasMany(CourseCurriculum::class,'content_id','id');
    }
}
