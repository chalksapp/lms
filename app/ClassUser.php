<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassUser extends Model
{
    protected $table = 'classes_lms_users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'class_id','user_id'
    ];
}
