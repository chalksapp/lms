<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CourseCurriculum;
use Illuminate\Support\Carbon;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id'
    ];
    //defining relationship with course curriculum
    public function curriculum() {
        return $this->hasMany(CourseCurriculum::class, 'course_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(LmsUser::class, 'course_users', 'course_id', 'user_id')
            ->withPivot(['created_at', 'updated_at']);
    }

    public function getUpdatedAtAttribute()
    {
        return Carbon::create($this->attributes['updated_at'])->format('d M, Y');
    }
}
