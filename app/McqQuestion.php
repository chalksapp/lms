<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class McqQuestion extends Model
{
     protected $table = 'mcq_questions';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'mcq_ques_title','mcq_ques_text','mcq_option1','mcq_option2','mcq_option3','mcq_option4','mcq_correct_ans','mcq_full_marks','mcq_negative_marks','mcq_ques_status'
    ];
}
