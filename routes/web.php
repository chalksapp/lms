<?php
use App\Http\Controllers\LanguageController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route Authentication Pages
Route::get('/login', 'AuthenticationController@get_login')->name('login');
Route::post('/user-login', 'Auth\LoginController@authenticate');
Route::get('logout', 'Auth\LoginController@logout');
// Route url
Route::group(['middleware' => ['auth','prevent-back-history']], function () {
    Route::get('/', 'DashboardController@dashboardAnalytics')->name('dashboard-analytics');
    Route::get('/dashboard', 'DashboardController@dashboardAnalytics')->name('dashboard');
    //Courses
    Route::get('/courses', 'CourseController@getCourses')->name('courses');
    //Route::get('/course/{id?}', 'CourseController@courseCurriculumView')->name('course-curriculum');
    Route::get('/course/{id?}/{tab?}', 'CourseController@courseBuilderView')->name('course-builder');
    Route::post('/save-course-data', 'CourseController@saveCourseData')->name('save-course-data');
    Route::post('/delete-course-data', 'CourseController@deleteCourseData')->name('delete-course-data');
    Route::post('/save-course-users', 'CourseController@saveCourseUsers')->name('save-course-users');
    //Videos
    Route::get('/videos', 'VideoController@getVideos')->name('videos');
    Route::post('/save-video-data', 'VideoController@saveVideoData')->name('save-video-data');
    Route::post('/delete-video-data', 'VideoController@deleteVideoData')->name('delete-course-data');
    Route::post('/get-video-courses', 'VideoController@getVideoCourses')->name('get-video-courses');
    Route::post('/save-course-video', 'VideoController@addVideoToCourse')->name('save-course-video');
    //Users
    Route::get('/users', 'UserController@getUsers')->name('users');
    Route::get('/user/{id?}', 'UserController@addNewUserView')->name('user');
    Route::post('/save-user', 'UserController@saveUserData')->name('save-user');
    Route::post('/delete-user', 'UserController@deleteUser')->name('delete-user');
    //Classes
    Route::get('/classes', 'ClassController@getClasses')->name('classes');
    Route::get('/class/{id?}', 'ClassController@addNewClassView')->name('class');
    Route::post('/save-class', 'ClassController@saveClassData')->name('save-class');
    Route::post('/delete-class', 'ClassController@deleteClass')->name('delete-class');
    //Curriculum
    Route::post('/save-curriculum-order', 'CourseController@saveCurriculumOrder')->name('save-curriculum-order');
    Route::post('/delete-curriculum', 'CourseController@deleteCurriculum')->name('delete-curriculum-data');
    Route::post('/save-course-content', 'CourseController@saveCourseContent')->name('save-course-content');
    //Tests
    Route::get('/tests', 'TestController@getAllTests')->name('tests');
    Route::post('/create-test', 'TestController@createNewTest')->name('create-test');
    Route::get('/test/{id?}', 'TestController@loadThisTest')->name('load-test');
    Route::post('/delete-test', 'TestController@deleteTest')->name('delete-test');
    //Roles
    Route::get('/roles', 'RoleController@getAllRoles')->name('roles');
    Route::get('/role/{id?}', 'RoleController@loadThisRole')->name('load-role');
    Route::post('/create-update-role', 'RoleController@createUpdateRole')->name('create-update-role');
    Route::post('/delete-role', 'RoleController@deleteRole')->name('delete-role');
    //Admin Users
    Route::get('/admin-users', 'AdminUserController@getAllUsers')->name('admin-users');
    Route::get('/admin-user/{id?}', 'AdminUserController@addNewUserView')->name('admin-user');
    Route::post('/save-admin-user', 'AdminUserController@saveUserData')->name('save-admin-user');
    Route::post('/delete-admin-user', 'AdminUserController@deleteAdminUser')->name('delete-admin-user');
    //Questions
    Route::get('/mcq-ques', 'QuestionController@getAllMCQ')->name('mcq-ques');
    Route::get('/mcq/{id?}', 'QuestionController@loadThisMCQ')->name('load-mcq-ques');
    Route::post('/create-mcq', 'QuestionController@createNewMCQ')->name('create-mcq');
    Route::get('/desc-ques', 'QuestionController@getAllDescQuestions')->name('desc-ques');
    Route::get('/desc/{id?}', 'QuestionController@loadThisDesc')->name('load-desc-ques');
    Route::post('/create-desc', 'QuestionController@createNewDesc')->name('create-desc');
});   

// Route Dashboards
Route::get('/dashboard-ecommerce', 'DashboardController@dashboardEcommerce');


// Route Apps
Route::get('/app-email', 'EmailAppController@emailApp');
Route::get('/app-chat', 'ChatAppController@chatApp');
Route::get('/app-todo', 'ToDoAppController@todoApp');
Route::get('/app-calender', 'CalenderAppController@calenderApp');
Route::get('/app-ecommerce-shop', 'EcommerceAppController@ecommerce_shop');
Route::get('/app-ecommerce-details', 'EcommerceAppController@ecommerce_details');
Route::get('/app-ecommerce-wishlist', 'EcommerceAppController@ecommerce_wishlist');
Route::get('/app-ecommerce-checkout', 'EcommerceAppController@ecommerce_checkout');
Route::get('/app-file-manager', 'FileManagerController@file_manager');

// Users Pages
Route::get('/app-user-list', 'UserPagesController@user_list');
Route::get('/app-user-view', 'UserPagesController@user_view');
Route::get('/app-user-edit', 'UserPagesController@user_edit');

// Route Data List
Route::resource('/data-list-view','DataListController');
Route::resource('/data-thumb-view', 'DataThumbController');


// Route Content
Route::get('/content-grid', 'ContentController@grid');
Route::get('/content-typography', 'ContentController@typography');
Route::get('/content-text-utilities', 'ContentController@text_utilities');
Route::get('/content-syntax-highlighter', 'ContentController@syntax_highlighter');
Route::get('/content-helper-classes', 'ContentController@helper_classes');

// Route Color
Route::get('/colors', 'ContentController@colors');

// Route Icons
Route::get('/icons-feather', 'IconsController@icons_feather');
Route::get('/icons-font-awesome', 'IconsController@icons_font_awesome');

// Route Cards
Route::get('/card-basic', 'CardsController@card_basic');
Route::get('/card-advance', 'CardsController@card_advance');
Route::get('/card-statistics', 'CardsController@card_statistics');
Route::get('/card-analytics', 'CardsController@card_analytics');
Route::get('/card-actions', 'CardsController@card_actions');

// Route Components
Route::get('/component-alert', 'ComponentsController@alert');
Route::get('/component-buttons', 'ComponentsController@buttons');
Route::get('/component-breadcrumbs', 'ComponentsController@breadcrumbs');
Route::get('/component-carousel', 'ComponentsController@carousel');
Route::get('/component-collapse', 'ComponentsController@collapse');
Route::get('/component-dropdowns', 'ComponentsController@dropdowns');
Route::get('/component-list-group', 'ComponentsController@list_group');
Route::get('/component-modals', 'ComponentsController@modals');
Route::get('/component-pagination', 'ComponentsController@pagination');
Route::get('/component-navs', 'ComponentsController@navs');
Route::get('/component-navbar', 'ComponentsController@navbar');
Route::get('/component-tabs', 'ComponentsController@tabs');
Route::get('/component-pills', 'ComponentsController@pills');
Route::get('/component-tooltips', 'ComponentsController@tooltips');
Route::get('/component-popovers', 'ComponentsController@popovers');
Route::get('/component-badges', 'ComponentsController@badges');
Route::get('/component-pill-badges', 'ComponentsController@pill_badges');
Route::get('/component-progress', 'ComponentsController@progress');
Route::get('/component-media-objects', 'ComponentsController@media_objects');
Route::get('/component-spinner', 'ComponentsController@spinner');
Route::get('/component-toast', 'ComponentsController@toast');

// Route Extra Components
Route::get('/ex-component-avatar', 'ExtraComponentsController@avatar');
Route::get('/ex-component-chips', 'ExtraComponentsController@chips');
Route::get('/ex-component-divider', 'ExtraComponentsController@divider');

// Route Forms
Route::get('/form-select', 'FormsController@select');
Route::get('/form-switch', 'FormsController@switch');
Route::get('/form-checkbox', 'FormsController@checkbox');
Route::get('/form-radio', 'FormsController@radio');
Route::get('/form-input', 'FormsController@input');
Route::get('/form-input-groups', 'FormsController@input_groups');
Route::get('/form-number-input', 'FormsController@number_input');
Route::get('/form-textarea', 'FormsController@textarea');
Route::get('/form-date-time-picker', 'FormsController@date_time_picker');
Route::get('/form-layout', 'FormsController@layouts');
Route::get('/form-wizard', 'FormsController@wizard');
Route::get('/form-validation', 'FormsController@validation');

// Route Tables
Route::get('/table', 'TableController@table');
Route::get('/table-datatable', 'TableController@datatable');
Route::get('/table-ag-grid', 'TableController@ag_grid');

// Route Pages
Route::get('/page-user-profile', 'PagesController@user_profile');
Route::get('/page-faq', 'PagesController@faq');
Route::get('/page-knowledge-base', 'PagesController@knowledge_base');
Route::get('/page-kb-category', 'PagesController@kb_category');
Route::get('/page-kb-question', 'PagesController@kb_question');
Route::get('/page-search', 'PagesController@search');
Route::get('/page-invoice', 'PagesController@invoice');
Route::get('/page-account-settings', 'PagesController@account_settings');
Route::get('/pricing', 'PagesController@pricing');

// Route Authentication Pages
Route::get('/auth-register', 'AuthenticationController@register');
Route::get('/auth-forgot-password', 'AuthenticationController@forgot_password');
Route::get('/auth-reset-password', 'AuthenticationController@reset_password');
Route::get('/auth-lock-screen', 'AuthenticationController@lock_screen');

// Route Miscellaneous Pages
Route::get('/page-coming-soon', 'MiscellaneousController@coming_soon');
Route::get('/error-404', 'MiscellaneousController@error_404');
Route::get('/error-500', 'MiscellaneousController@error_500');
Route::get('/page-not-authorized', 'MiscellaneousController@not_authorized');
Route::get('/page-maintenance', 'MiscellaneousController@maintenance');

// Route Charts & Google Maps
Route::get('/chart-apex', 'ChartsController@apex');
Route::get('/chart-chartjs', 'ChartsController@chartjs');
Route::get('/chart-echarts', 'ChartsController@echarts');
Route::get('/maps-google', 'ChartsController@maps_google');

// Route Extension Components
Route::get('/ext-component-sweet-alerts', 'ExtensionController@sweet_alert');
Route::get('/ext-component-toastr', 'ExtensionController@toastr');
Route::get('/ext-component-noui-slider', 'ExtensionController@noui_slider');
Route::get('/ext-component-file-uploader', 'ExtensionController@file_uploader');
Route::get('/ext-component-quill-editor', 'ExtensionController@quill_editor');
Route::get('/ext-component-drag-drop', 'ExtensionController@drag_drop');
Route::get('/ext-component-tour', 'ExtensionController@tour');
Route::get('/ext-component-clipboard', 'ExtensionController@clipboard');
Route::get('/ext-component-plyr', 'ExtensionController@plyr');
Route::get('/ext-component-context-menu', 'ExtensionController@context_menu');
Route::get('/ext-component-swiper', 'ExtensionController@swiper');
Route::get('/ext-component-i18n', 'ExtensionController@i18n');

// acess controller
Route::get('/access-control', 'AccessController@index');
Route::get('/access-control/{roles}', 'AccessController@roles');
Route::get('/modern-admin', 'AccessController@home')->middleware('role:Admin');

// Auth::routes();

Route::post('/login/validate', 'Auth\LoginController@validate_api');

// locale Route
Route::get('lang/{locale}',[LanguageController::class,'swap']);
