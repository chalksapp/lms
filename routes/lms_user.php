<?php

Route::group([
    'namespace' => 'Auth',
], function () {
    // Authentication Routes...
    Route::get('login', 'LoginController@showLoginForm')->name('login_page');
    Route::post('login', 'LoginController@login')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::post('register', 'RegisterController@register')->name('register');
});

Route::group([
    'middleware' => [
        'auth:lms_user',
    ],
], function () {
    Route::get('/dashboard', 'LmsUserAuthController@dashboard')->name('dashboard');
    Route::get('/courses', 'CourseController@index')->name('courses');
    Route::get('all-courses', 'CourseController@getAllCourses')->name('get_all_courses');
    Route::get('course/{course}', 'CourseController@courseDetailsPage')->name('course_details');
    Route::get('lessons/{course}', 'CourseController@showLessons')->name('lessons');
    //Tests
    Route::get('/tests', 'TestController@index')->name('common-tests');
});
Route::get('/complete-registration/{encrypted_string}', 'LmsUserAuthController@completeRegistration')->name('complete-registration');

