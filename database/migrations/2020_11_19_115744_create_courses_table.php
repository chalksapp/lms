<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('course_name');
            $table->longText('course_desc');
            $table->date('course_start_date');
            $table->integer('course_fees');
            $table->text('course_level');
            $table->integer('course_duration');
            $table->text('course_period');
            $table->text('course_schedule')->nullable();
            $table->text('course_image')->nullable();
            $table->integer('course_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
