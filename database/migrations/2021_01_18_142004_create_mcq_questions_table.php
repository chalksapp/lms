<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMcqQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mcq_questions', function (Blueprint $table) {
            $table->id();
            $table->string('mcq_ques_title');
            $table->longText('mcq_ques_text');
            $table->string('mcq_option1');
            $table->string('mcq_option2');
            $table->string('mcq_option3');
            $table->string('mcq_option4');
            $table->integer('mcq_correct_ans');
            $table->integer('mcq_full_marks');
            $table->integer('mcq_negative_marks');
            $table->integer('mcq_ques_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mcq_questions');
    }
}
