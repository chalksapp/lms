<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDescQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('desc_questions', function (Blueprint $table) {
            $table->id();
            $table->longText('desc_ques_title');
            $table->longText('desc_ques_text');
            $table->longText('desc_answer_text');
            $table->integer('desc_full_marks');
            $table->integer('desc_negative_marks');
            $table->integer('desc_ques_status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('desc_questions');
    }
}
