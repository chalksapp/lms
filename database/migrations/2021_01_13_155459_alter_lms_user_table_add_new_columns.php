<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterLmsUserTableAddNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lms_users', function (Blueprint $table) {
            $table->renameColumn('user_name', 'name');
            $table->renameColumn('user_email', 'email');
            $table->string('phone', 15)->nullable()->after('user_email');
            $table->string('password')->nullable()->after('phone');
            $table->rememberToken()->nullable()->after('password');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('lms_users', 'password')) {
            Schema::table('lms_users', function (Blueprint $table) {
                $table->renameColumn('name', 'user_name');
                $table->renameColumn('email', 'user_email');
                $table->dropColumn('password');
                $table->dropColumn('phone');
                $table->dropColumn('remember_token');
                $table->dropColumn('deleted_at');
            });
        }
    }
}
