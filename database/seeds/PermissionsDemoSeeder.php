<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
class PermissionsDemoSeeder extends Seeder
{
    /**
     * Create the initial roles and permissions.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions for courses
        Permission::create(['name' => 'create courses']);
        Permission::create(['name' => 'list courses']);
        Permission::create(['name' => 'view courses']);
        Permission::create(['name' => 'edit courses']);
        Permission::create(['name' => 'delete courses']);

        // create permissions for videos
        Permission::create(['name' => 'create videos']);
        Permission::create(['name' => 'list videos']);
        Permission::create(['name' => 'view videos']);
        Permission::create(['name' => 'edit videos']);
        Permission::create(['name' => 'delete videos']);

        // create permissions for users
        Permission::create(['name' => 'create users']);
        Permission::create(['name' => 'list users']);
        Permission::create(['name' => 'view user']);
        Permission::create(['name' => 'edit user']);
        Permission::create(['name' => 'delete user']);

        // create permissions for tests
        Permission::create(['name' => 'create tests']);
        Permission::create(['name' => 'list tests']);
        Permission::create(['name' => 'view test']);
        Permission::create(['name' => 'edit test']);
        Permission::create(['name' => 'delete test']);

        // create permissions for class
        Permission::create(['name' => 'create classes']);
        Permission::create(['name' => 'list classes']);
        Permission::create(['name' => 'view class']);
        Permission::create(['name' => 'edit class']);
        Permission::create(['name' => 'delete class']);

        // create permissions for MCQ
        Permission::create(['name' => 'create mcq']);
        Permission::create(['name' => 'list mcq']);
        Permission::create(['name' => 'view mcq']);
        Permission::create(['name' => 'edit mcq']);
        Permission::create(['name' => 'delete mcq']);

        // create permissions for Desc
        Permission::create(['name' => 'create desc']);
        Permission::create(['name' => 'list desc']);
        Permission::create(['name' => 'view desc']);
        Permission::create(['name' => 'edit desc']);
        Permission::create(['name' => 'delete desc']);

        // create permissions for Roles
        Permission::create(['name' => 'create roles']);
        Permission::create(['name' => 'list roles']);
        Permission::create(['name' => 'view roles']);
        Permission::create(['name' => 'edit roles']);
        Permission::create(['name' => 'delete roles']);

        // create permissions for Admin Users
        Permission::create(['name' => 'create admin users']);
        Permission::create(['name' => 'list admin users']);
        Permission::create(['name' => 'view admin users']);
        Permission::create(['name' => 'edit admin users']);
        Permission::create(['name' => 'delete admin users']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'Super Admin']);
        $role1->givePermissionTo('create courses');
        $role1->givePermissionTo('list courses');
        $role1->givePermissionTo('view courses');
        $role1->givePermissionTo('edit courses');
        $role1->givePermissionTo('delete courses');
        $role1->givePermissionTo('create tests');
        $role1->givePermissionTo('list tests');
        $role1->givePermissionTo('view test');
        $role1->givePermissionTo('edit test');
        $role1->givePermissionTo('delete test');
        $role1->givePermissionTo('create videos');
        $role1->givePermissionTo('list videos');
        $role1->givePermissionTo('view videos');
        $role1->givePermissionTo('edit videos');
        $role1->givePermissionTo('delete videos');
        $role1->givePermissionTo('create users');
        $role1->givePermissionTo('list users');
        $role1->givePermissionTo('view user');
        $role1->givePermissionTo('edit user');
        $role1->givePermissionTo('delete user');
        $role1->givePermissionTo('create mcq');
        $role1->givePermissionTo('list mcq');
        $role1->givePermissionTo('view mcq');
        $role1->givePermissionTo('edit mcq');
        $role1->givePermissionTo('delete mcq');
        $role1->givePermissionTo('create desc');
        $role1->givePermissionTo('list desc');
        $role1->givePermissionTo('view desc');
        $role1->givePermissionTo('edit desc');
        $role1->givePermissionTo('delete desc');
        $role1->givePermissionTo('create classes');
        $role1->givePermissionTo('list classes');
        $role1->givePermissionTo('view class');
        $role1->givePermissionTo('edit class');
        $role1->givePermissionTo('delete class');
        $role1->givePermissionTo('create roles');
        $role1->givePermissionTo('list roles');
        $role1->givePermissionTo('edit roles');
        $role1->givePermissionTo('view roles');
        $role1->givePermissionTo('delete roles');
        $role1->givePermissionTo('create admin users');
        $role1->givePermissionTo('list admin users');
        $role1->givePermissionTo('edit admin users');
        $role1->givePermissionTo('delete admin users');
        $role1->givePermissionTo('view admin users');
        // get super admin and assign all permissions
        $user = \App\User::where('id',1)->first();
        $user->assignRole($role1);
    }
}
