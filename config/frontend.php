<?php
return [
    'PAGINATION_LIMIT' => env('FE_PAGINATION_LIMIT', 6),
    'CURRENCY' => env('FE_CURRENCY', '₹'),
];