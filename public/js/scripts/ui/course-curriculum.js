/*=========================================================================================
    File Name: course-curriculum.js
    Description: Course Curriculum View
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function() {
    // Draggable Cards
    dragula([document.getElementById('card-drag-area')]).on('drop', function(el) {
         var order_array  = [];
         $(".curriculum-card").each(function() {
            var content_id   = $(this).attr('data-content-id');
            if(order_array.indexOf(content_id) == -1){
               order_array.push(content_id);
            }
         });
         //calling ajax request to save the curriculum order
         $.ajax({
            type: 'POST',
            data:{
              _token : $('meta[name="csrf-token"]').attr('content'),
              course_id:this_course_id,
              order_array:JSON.stringify(order_array)
            },
            dataType:'JSON',
            url: site_url +'/save-curriculum-order',
            success: function(data){
              toastr.success(data.message, 'Success!', { "timeOut": 0 });
            },
            error: function(error){
                var errors      = error.responseJSON;
                var errorsHtml  = '';
                $.each(errors.errors,function (k,v) {
                    errorsHtml += '<li>'+ v + '</li>';
                });
                toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
            }
         });
    });
    //remove this tile from curriculum click function
    $('.remove-from-curriculum').click(function(){
        var this_curriculum_id = $(this).attr('data-curriculum-id');
        $.ajax({
            type: 'POST',
            data:{
              _token : $('meta[name="csrf-token"]').attr('content'),
              course_id:this_course_id,
              this_curriculum_id:this_curriculum_id
            },
            dataType:'JSON',
            url: site_url +'/delete-curriculum',
            success: function(data){
              toastr.success(data.message, 'Success!', { "timeOut": 0 });
              location.reload();
            },
            error: function(error){
                var errors      = error.responseJSON;
                var errorsHtml  = '';
                $.each(errors.errors,function (k,v) {
                    errorsHtml += '<li>'+ v + '</li>';
                });
                toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
            }
         });
    })
});
