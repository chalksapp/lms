/*=========================================================================================
    File Name: add_role_view.js
    Description: Add Test Contents
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
  //eneble select2 globally
  function enableSelect2(){
      // Basic Select2 select
      $("#select_permissions").select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        placeholder: "Click here to select a permission",
        dropdownAutoWidth: true,
        width: '100%'
      });
      $('.select2-search__field').attr('placeholder','Click here');
      $('.select2-search__field').css('width','100%');
  }
  //select all checkbox onclick function
    $("#select_all_check").click(function(){
        if($("#select_all_check").is(':checked') ){
            $("#select_permissions > option").attr("selected","selected");
            $("#select_permissions").trigger("change");
        }else{
            $("#select_permissions > option").removeAttr("selected");
             $("#select_permissions").trigger("change");
         }
    });
  enableSelect2();
