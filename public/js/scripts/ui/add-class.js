/*=========================================================================================
    File Name: add-class.js
    Description: List View
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function() {
    // Basic Select2 select
    $("#class_select_users").select2({
      // the following code is used to disable x-scrollbar when click in select input and
      // take 100% width in responsive also
      placeholder: "Click here to select a user",
      dropdownAutoWidth: true,
      width: '100%'
    });
    $('.select2-search__field').attr('placeholder','Click here');
    $('.select2-search__field').css('width','100%');
});
