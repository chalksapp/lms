/*=========================================================================================
    File Name: add-admin-user-view.js
    Description: Add Admin User Contents
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
  //eneble select2 globally
  function enableSelect2(){
      // Basic Select2 select
      $("#select_roles").select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        placeholder: "Click here to select a role",
        dropdownAutoWidth: true,
        width: '100%'
      });
      $('.select2-search__field').attr('placeholder','Click here');
      $('.select2-search__field').css('width','100%');
  }
  //select all checkbox onclick function
    $("#select_all_check").click(function(){
        if($("#select_all_check").is(':checked') ){
            $("#select_roles > option").attr("selected","selected");
            $("#select_roles").trigger("change");
        }else{
            $("#select_roles > option").removeAttr("selected");
             $("#select_roles").trigger("change");
         }
    });
  enableSelect2();
