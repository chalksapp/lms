/*=========================================================================================
    File Name: data-list-view.js
    Description: List View
    ----------------------------------------------------------------------------------------
    Item Name: Vuexy  - Vuejs, HTML & Laravel Admin Dashboard Template
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/

$(document).ready(function() {
  "use strict"
  // init list view datatable
  var dataListView = $(".data-list-view").DataTable({
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: { selectRow: true }
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 4,
    buttons: [
      {
        text: "<i class='feather icon-plus'></i> Add New",
        action: function() {
          $(this).removeClass("btn-secondary");
          location.href = site_url+'/course';
          $("#hid_course_id").val('');
          $("#course_data_form")[0].reset();
          $("#schedule-label").html('Choose File');
          $("#image-label").html('Choose File');
          // Input, Select, Textarea validations except submit button
          enableFormValidation(0);
        },
        className: "btn-outline-primary"
      }
    ],
    initComplete: function(settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  });

  dataListView.on('draw.dt', function(){
    setTimeout(function(){
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });

  // init thumb view datatable
  var dataThumbView = $(".data-thumb-view").DataTable({
    responsive: false,
    columnDefs: [
      {
        orderable: true,
        targets: 0,
        checkboxes: { selectRow: true }
      }
    ],
    dom:
      '<"top"<"actions action-btns"B><"action-filters"lf>><"clear">rt<"bottom"<"actions">p>',
    oLanguage: {
      sLengthMenu: "_MENU_",
      sSearch: ""
    },
    aLengthMenu: [[4, 10, 15, 20], [4, 10, 15, 20]],
    select: {
      style: "multi"
    },
    order: [[1, "asc"]],
    bInfo: false,
    pageLength: 4,
    buttons: [
      {
        text: "<i class='feather icon-plus'></i> Add New",
        action: function() {
          $(this).removeClass("btn-secondary")
          location.href = site_url+'/course';
          // Input, Select, Textarea validations except submit button
          $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
        },
        className: "btn-outline-primary"
      }
    ],
    initComplete: function(settings, json) {
      $(".dt-buttons .btn").removeClass("btn-secondary")
    }
  })

  dataThumbView.on('draw.dt', function(){
    setTimeout(function(){
      if (navigator.userAgent.indexOf("Mac OS X") != -1) {
        $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
      }
    }, 50);
  });

  // To append actions dropdown before add new button
  var actionDropdown = $(".actions-dropodown")
  actionDropdown.insertBefore($(".top .actions .dt-buttons"))


  // Scrollbar
  if ($(".data-items").length > 0) {
    new PerfectScrollbar(".data-items", { wheelPropagation: false })
  }

  // Close sidebar
  $(".hide-data-sidebar, .cancel-data-btn, .overlay-bg").on("click", function() {
    $(".add-new-data").removeClass("show")
    $(".overlay-bg").removeClass("show")
  })
  //course name on click function
  $('.course_name_click').on("click",function(e){
      e.stopPropagation();
      var this_id = $(this).attr('data-id');
      location.href = site_url+'/course/'+this_id;
      //window.location.href = '';
  });
  // On Edit
  $('.action-edit').on("click",function(e){
      e.stopPropagation();
      var courseValueString = $(this).attr('data-value');
      var courseValues      = courseValueString.split(",");
      $("#hid_course_id").val(courseValues[0]);
      location.href = site_url+'/course/'+courseValues[0];
  });
  //on view click button
  $('.action-view').on("click",function(e){
    e.stopPropagation();
    var courseValueString = $(this).attr('data-value');
    var courseValues      = parseCourseData(courseValueString);
    $("#view_course_name").val(atob(courseValues[1]));
    $("#view_course_desc").val(atob(courseValues[2]));
    $("#view_course_date").val(courseValues[3]);
    $("#view_course_fees").val(atob(courseValues[4]));
    $("#view_course_level").val(courseValues[5]);
    $("#view_course_duration").val(atob(courseValues[6]));
    $("#view_course_period").val(courseValues[7]);
    $("#view_course_schedule").html(atob(courseValues[8]));
    $("#view_course_schedule").attr('href','course_files/'+courseValues[0]+'/'+atob(courseValues[8]));
    $("#view_course_image").attr('src','course_files/'+courseValues[0]+'/'+atob(courseValues[9]));
    $("#view_course_modal").modal('show');
  });
  //parse course data
  function parseCourseData(data){
      return data.split(",");
  }
  // On Delete
  $('.action-delete').on("click", function(e){
    var course_value_id = $(this).attr('data-value');
    e.stopPropagation();
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn btn-danger ml-1',
      buttonsStyling: false,
    }).then(function (result) {
      if (result.value) {
        $.ajax({
          type: 'POST',
          dataType:'json',
          data:{
              _token : $('meta[name="csrf-token"]').attr('content'),
              course_id:course_value_id
          },
          url: 'delete-course-data',
          success: function(data){
            toastr.success(data.message, 'Success!', { "timeOut": 0 });
            setTimeout(function(){
              location.reload();
            },500);
          },
          error: function(error){
              var errors      = error.responseJSON;
              var errorsHtml  = '';
              $.each(errors.errors,function (k,v) {
                  errorsHtml += '<li>'+ v + '</li>';
              });
              toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
          }
        });
      }
      else{
        location.reload();
      }
    })
  });

  // dropzone init
  Dropzone.options.dataListUpload = {
    complete: function(files) {
      var _this = this
      // checks files in class dropzone and remove that files
      $(".hide-data-sidebar, .cancel-data-btn, .actions .dt-buttons").on(
        "click",
        function() {
          $(".dropzone")[0].dropzone.files.forEach(function(file) {
            file.previewElement.remove()
          })
          $(".dropzone").removeClass("dz-started")
        }
      )
    }
  }
  Dropzone.options.dataListUpload.complete()

  // mac chrome checkbox fix
  if (navigator.userAgent.indexOf("Mac OS X") != -1) {
    $(".dt-checkboxes-cell input, .dt-checkboxes").addClass("mac-checkbox")
  }

  //save course data ajax call
  function ajaxRequestCourseSave($form,event){
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
          }
      });
      $.ajax({
        type: 'POST',
        data:new FormData(jQuery('#course_data_form')[0]),
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        url: 'save-course-data',
        success: function(data){
          $('#loader_btn').css('display','none');
          toastr.success(data.message, 'Success!', { "timeOut": 0 });
          $(".add-new-data").removeClass('show').addClass('hide');
          $(".overlay-bg").removeClass('show').addClass('hide');
          setTimeout(function(){
            location.reload();
          },1000);
        },
        error: function(error){
            var errors      = error.responseJSON;
            var errorsHtml  = '';
            $.each(errors.errors,function (k,v) {
                errorsHtml += '<li>'+ v + '</li>';
            });
            toastr.error(errorsHtml, 'Error!', { "timeOut": 0 });
            $('#loader_btn').css('display','none');
            $('#save_course_btn').removeAttr('disabled');
        }
      });
  }
  //validation for checking file inputs
  function checkFilesValidation(){
      var selected_schedule_file = $('#hid_schedule_file').val();
      var selected_image_file    = $('#hid_image_file').val();
      if(!selected_schedule_file || !selected_image_file){
          if(!selected_schedule_file){
              $("#schedule_form_group").removeClass('error').addClass('error');
              $("#error_schedule_ul").remove();
              $("#schedule_controls").append('<div class="help-block" id="error_schedule_ul"><ul role="alert"><li>Course Schedule pdf is required</li></ul></div>');
          }
          if(!selected_image_file){
              $("#image_form_group").removeClass('error').addClass('error');
              $("#error_image_ul").remove();
              $("#image_controls").append('<div class="help-block" id="error_image_ul"><ul role="alert"><li>Course Image is required</li></ul></div>');
          }
          return false;
      }
      if(selected_schedule_file){
          $("#schedule_form_group").removeClass('error');
          $("#error_schedule_ul").remove();
      }
      if(selected_image_file){
          $("#image_form_group").removeClass('error');
          $("#error_image_ul").remove();
      }
  }
  //schedule file input onchange function
  $(document).on('change','#course_schedule',function(){
      if($(this)[0].files.length > 0){
          $('#hid_schedule_file').val($(this)[0].files[0].name);
          $("#schedule-label").html($(this)[0].files[0].name);
      }
      checkFilesValidation();
  });
  //image file input onchange function
  $(document).on('change','#course_image',function(){
    if($(this)[0].files.length > 0){
        $('#hid_image_file').val($(this)[0].files[0].name);
        $("#image-label").html($(this)[0].files[0].name);
    }
    checkFilesValidation();
});
  //enable form validation
  function enableFormValidation(edit){
      $("#course_data_form").find($("input,select,textarea")).not("[type=submit]").jqBootstrapValidation({
        preventSubmit: true,
          submitSuccess: function($form, event) {
            event.preventDefault();
            checkFilesValidation();
            $('#loader_btn').css('display','block');
            $('#save_course_btn').attr('disabled',true);
            ajaxRequestCourseSave($form,event);
          },
          filter: function() {
              return $(this).is(":visible");
          }
      });
  }
})
